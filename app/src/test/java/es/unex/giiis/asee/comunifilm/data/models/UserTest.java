package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserTest {



    private User prueba =new User();
    private User prueba2 =new User(1,"nombreprueba","contrasenaprueba","emailprueba");

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void setId() {
        prueba.setId(2);
        assertEquals(prueba.getId(),2);
    }

    @Test
    public void getId() {
        assertEquals(prueba2.getId(),1);
    }

    @Test
    public void getName() {
        assertEquals(prueba2.getName(),"nombreprueba");
    }

    @Test
    public void getPassword() {
        assertEquals(prueba2.getPassword(),"contrasenaprueba");
    }

    @Test
    public void getEmail() {
        assertEquals(prueba2.getEmail(),"emailprueba");
    }

    @Test
    public void setName() {
        prueba.setName("Pruebanombre");
        assertEquals(prueba.getName(),"Pruebanombre");
    }

    @Test
    public void setPassword() {
        prueba.setPassword("pruebacontra");
        assertEquals(prueba.getPassword(),"pruebacontra");
    }

    @Test
    public void setEmail() {
        prueba.setEmail("pruebaemail");
        assertEquals(prueba.getEmail(),"pruebaemail");
    }

    @Test
    public void isVotado() {
        assertEquals(prueba2.isVotado(),false);
    }

    @Test
    public void setVotado() {
        prueba.setVotado(true);
        assertEquals(prueba.isVotado(),true);
    }
}