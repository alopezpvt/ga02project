package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ListOfListTest {

    private ListOfList listOfList = new ListOfList(1,"Películas de miedo", "Alberto");
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void setIdList() {
        listOfList.setIdList(2);
        assertEquals(listOfList.getIdList(),2);
    }

    @Test
    public void setNameList() {
        listOfList.setNameList("Películas de risa");
        assertEquals(listOfList.getNameList(),"Películas de risa");
    }

    @Test
    public void getIdList() {
        assertEquals(listOfList.getIdList(),1);
    }

    @Test
    public void getNameList() {
        assertEquals(listOfList.getNameList(),"Películas de miedo");

    }

    @Test
    public void getNameuser() {
        assertEquals(listOfList.getNameuser(),"Alberto");

    }

    @Test
    public void setNameuser() {
        listOfList.setNameList("Luis");
        assertEquals(listOfList.getNameList(),"Luis");

    }
}