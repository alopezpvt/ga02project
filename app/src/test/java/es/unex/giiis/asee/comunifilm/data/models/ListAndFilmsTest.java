package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ListAndFilmsTest {

    private ListAndFilms listAndFilms = new ListAndFilms(1, 2, "Shrek");

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getIdList() {
        assertEquals(listAndFilms.getIdList(),1);

    }

    @Test
    public void setIdList() {
        listAndFilms.setIdList(3);
        assertEquals(listAndFilms.getIdList(),3);

    }

    @Test
    public void getIdFilm() {
        assertEquals(listAndFilms.getIdFilm(),2);

    }

    @Test
    public void setIdFilm() {
        listAndFilms.setIdFilm(4);
        assertEquals(listAndFilms.getIdFilm(),4);

    }

    @Test
    public void getNameFilm() {
        assertEquals(listAndFilms.getNameFilm(),"Shrek");

    }

    @Test
    public void setNameFilm() {
        listAndFilms.setNameFilm("Ali G");
        assertEquals(listAndFilms.getNameFilm(),"Ali G");

    }
}