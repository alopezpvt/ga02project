package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WishTest {

    private Wish prueba =new Wish("Autor", "NombreTitulo","Poster",5.0, 6, "01-01-2021", "Descripcion");
    private Wish prueba2 =new Wish("AutorPrueba", "NombreTituloPrueba","PosterPrueba",1.0, 9, "01-01-2022", "DescripcionPrueba");
    @Before
    public void setUp() throws Exception {
        prueba.setId(0);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getPosterPath() {
        assertEquals(prueba2.getPosterPath(),"PosterPrueba");
    }

    @Test
    public void setPosterPath() {
        prueba.setPosterPath("poster");
        assertEquals(prueba.getPosterPath(),"poster");
    }

    @Test
    public void getFilmName() {
        assertEquals(prueba2.getFilmName(),"NombreTituloPrueba");
    }

    @Test
    public void setFilmName() {
        prueba.setFilmName("titulo");
        assertEquals(prueba.getFilmName(),"titulo");
    }

    @Test
    public void getId() {
        assertEquals(prueba2.getId(),0);
    }

    @Test
    public void setId() {
        prueba.setId(2);
        assertEquals(prueba.getId(),2);
    }

    @Test
    public void getAutorWish() {
        assertEquals(prueba2.getAutorWish(),"AutorPrueba");
    }

    @Test
    public void setAutorWish() {
        prueba.setAutorWish("autorp");
        assertEquals(prueba.getAutorWish(),"autorp");
    }

    @Test
    public void getVoteCount() {
        assertEquals(java.util.Optional.ofNullable(prueba2.getVoteCount()),java.util.Optional.ofNullable(9));

    }

    @Test
    public void setVoteCount() {
        prueba.setVoteCount(2);
        assertEquals(java.util.Optional.ofNullable(prueba.getVoteCount()),java.util.Optional.ofNullable(2));
    }

    @Test
    public void getVoteAverage() {
        assertEquals(java.util.Optional.ofNullable(prueba2.getVoteAverage()),java.util.Optional.ofNullable(1.0));
    }

    @Test
    public void setVoteAverage() {
        prueba.setVoteAverage(10.0);
        assertEquals(java.util.Optional.ofNullable(prueba.getVoteAverage()),java.util.Optional.ofNullable(10.0));
    }

    @Test
    public void getReleaseDate() {
        assertEquals(prueba2.getReleaseDate(),"01-01-2022");
    }

    @Test
    public void setReleaseDate() {
        prueba.setReleaseDate("12-03-2020");
        assertEquals(prueba.getReleaseDate(),"12-03-2020");
    }

    @Test
    public void getOverview() {
        assertEquals(prueba2.getOverview(),"DescripcionPrueba");
    }

    @Test
    public void setOverview() {
        prueba.setOverview("Descripcionpru");
        assertEquals(prueba.getOverview(),"Descripcionpru");
    }
}