package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GenresTest {
    private Genres gs;
    private Genres aux;
    private List<Genre> list;

    @Before
    public void setUp() throws Exception {
        list = new ArrayList<Genre>();

        Genre g1 = new Genre();
        g1.setId(1);
        g1.setName("Action");

        Genre g2 = new Genre();
        g2.setId(2);
        g2.setName("Comedy");


        list.add(g1);
        list.add(g2);

        gs = new Genres(list);
        aux = new Genres();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getListGenres() {
        assertEquals(gs.getListGenres(), list);
    }

    @Test
    public void setListGenres() {
        aux.setListGenres(list);
        assertEquals(aux.getListGenres(), list);
    }

    @Test
    public void clear() {
        aux.clear();
        assertNotEquals(aux.getListGenres(), list);
    }
}