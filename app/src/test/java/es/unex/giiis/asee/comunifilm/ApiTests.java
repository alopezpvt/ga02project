package es.unex.giiis.asee.comunifilm;

import android.content.Context;

import org.junit.Test;

import java.util.ArrayList;

import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.Genre;
import es.unex.giiis.asee.comunifilm.data.models.Genres;
import es.unex.giiis.asee.comunifilm.data.network.FilmsGenresNetworkLoaderRuneable;
import es.unex.giiis.asee.comunifilm.data.network.FilmsSearchNetworkLoaderRuneable;
import es.unex.giiis.asee.comunifilm.data.network.OnFilmsLoadedListener;
import es.unex.giiis.asee.comunifilm.data.network.TopFilmsNetworkLoaderRuneable;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ApiTests {
    @Test
    public void getGenres_isCorrect() {
        new FilmsGenresNetworkLoaderRuneable(listGenre -> {
            assertNotNull(listGenre);
            assertFalse(listGenre.getListGenres().isEmpty());


            Genre g = listGenre.getListGenres().get(0);
            assertTrue(g.getName() != null && g.getName() != "");
        });



    }


    @Test
    public void search_isCorrect() {
        Context context = null;
        new FilmsSearchNetworkLoaderRuneable(new OnFilmsLoadedListener() {
            @Override
            public void OnFilmsLoaded(ArrayList<Film> listFilms) {
                assertNotNull(listFilms);
                assertFalse(listFilms.isEmpty());

                assertTrue(listFilms.get(0).getTitle().equals("Shrek"));
            }
        }, "Shrek", context);



    }


    @Test
    public void getTops_isCorrect() {
        Context context = null;
        new TopFilmsNetworkLoaderRuneable(new OnFilmsLoadedListener() {
            @Override
            public void OnFilmsLoaded(ArrayList<Film> listFilms) {
                assertNotNull(listFilms);
                assertFalse(listFilms.isEmpty());

                assertTrue(listFilms.get(0) != null);
            }
        }, context);

    }





}