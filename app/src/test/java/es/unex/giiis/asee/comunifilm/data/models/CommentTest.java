package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CommentTest {
    private Comment c1;
    private Comment c2;
    private Comment aux;

    @Before
    public void setUp() throws Exception {
        c1 = new Comment(1,12,"Dummy", "Pocahontas", 30, "Esto es una prueba", 0, "www.poster.com");
        c2 = new Comment(3,"User", "Fast and Furious", 54, "Comentario", 3, "www.cartel.es");
        aux = new Comment(1,"", "", 1, "", 0, "");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getPosterPath() {
        assertEquals(c1.getPosterPath(), "www.poster.com");
        assertEquals(c2.getPosterPath(), "www.cartel.es");
    }

    @Test
    public void setPosterPath() {
        aux.setPosterPath("notAurl");
        assertEquals(aux.getPosterPath(), "notAurl");
    }

    @Test
    public void getFilmName() {
        assertEquals(c1.getFilmName(), "Pocahontas");
        assertEquals(c2.getFilmName(), "Fast and Furious");
    }

    @Test
    public void setFilmName() {
        aux.setFilmName("aFilm");
        assertEquals(aux.getFilmName(), "aFilm");
    }

    @Test
    public void getFilmID() {
        assertEquals(c1.getFilmID(), 30);
        assertEquals(c2.getFilmID(), 54);
    }

    @Test
    public void setFilmID() {
        aux.setFilmID(4);
        assertEquals(aux.getFilmID(), 4);
    }

    @Test
    public void getId() {
        assertEquals(c1.getId(), 1);
    }

    @Test
    public void setId() {
        aux.setId(7);
        assertEquals(aux.getId(), 7);
    }

    @Test
    public void getIdUser() {
        assertEquals(c1.getIdUser(), 12);
        assertEquals(c2.getIdUser(), 3);
    }

    @Test
    public void setIdUser() {
        aux.setIdUser(14);
        assertEquals(aux.getIdUser(), 14);
    }


    @Test
    public void getAutorComment() {
        assertEquals(c1.getAutorComment(), "Dummy");
        assertEquals(c2.getAutorComment(), "User");
    }

    @Test
    public void setAutorComment() {
        aux.setAutorComment("test");
        assertEquals(aux.getAutorComment(), "test");
    }

    @Test
    public void getText() {
        assertEquals(c1.getText(), "Esto es una prueba");
        assertEquals(c2.getText(), "Comentario");
    }

    @Test
    public void setText() {
        aux.setText("this is a test");
        assertEquals(aux.getText(), "this is a test");
    }

    @Test
    public void getLikes() {
        assertEquals(c1.getLikes(), 0);
        assertEquals(c2.getLikes(), 3);
    }

    @Test
    public void setLikes() {
        aux.setLikes(8);
        assertEquals(aux.getLikes(), 8);
    }
}