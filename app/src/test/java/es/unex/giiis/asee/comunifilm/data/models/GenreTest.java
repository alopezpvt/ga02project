package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class GenreTest {
    Genre g;
    Genre aux;

    @Before
    public void setUp() throws Exception {
        g = new Genre();
        g.setId(5);
        g.setName("Action");
        aux = new Genre();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getId() {
        assertEquals(g.getId(), 5);
    }

    @Test
    public void setId() {
        aux.setId(2);
        assertEquals(aux.getId(), 2);
    }

    @Test
    public void getName() {
        assertEquals(g.getName(), "Action");
    }

    @Test
    public void setName() {
        aux.setName("Test");
        assertEquals(aux.getName(), "Test");
    }
}