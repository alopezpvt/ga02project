package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FavTest {

    private Fav prueba =new Fav(0, "autofav","titulo", "poster");
    private Fav prueba2 =new Fav(1, "autofav","titulo", "poster");
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getPosterPath() {
        assertEquals(prueba2.getPosterPath(),"poster");
    }

    @Test
    public void setPosterPath() {
        prueba.setPosterPath("posterpath");
        assertEquals(prueba.getPosterPath(),"posterpath");
    }

    @Test
    public void getFilmName() {
        assertEquals(prueba2.getFilmName(),"titulo");
    }

    @Test
    public void setFilmName() {
        prueba.setFilmName("nombre");
        assertEquals(prueba.getFilmName(),"nombre");
    }

    @Test
    public void getId() {
        assertEquals(prueba2.getId(),1);
    }

    @Test
    public void setId() {
        prueba.setId(1);
        assertEquals(prueba.getId(),1);
    }

    @Test
    public void getAutorFav() {
        assertEquals(prueba2.getAutorFav(),"autofav");
    }

    @Test
    public void setAutorFav() {
        prueba.setAutorFav("autorfav");
        assertEquals(prueba.getAutorFav(),"autorfav");
    }
}