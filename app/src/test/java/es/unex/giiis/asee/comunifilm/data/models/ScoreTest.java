package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ScoreTest {

    private Score prueba =new Score(0, "autor", "titulo", "poster", 1);
    private Score prueba2 =new Score(1,"autorprueba", "tituloprueba", "posterprueba", 2);

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getPosterPath() {
        assertEquals(prueba2.getPosterPath(),"posterprueba");
    }

    @Test
    public void setPosterPath() {
        prueba.setPosterPath("porter");
        assertEquals(prueba.getPosterPath(),"porter");
    }

    @Test
    public void getFilmName() {
        assertEquals(prueba2.getFilmName(),"tituloprueba");
    }

    @Test
    public void setFilmName() {
        prueba.setFilmName("Pruebanombre2");
        assertEquals(prueba.getFilmName(),"Pruebanombre2");
    }

    @Test
    public void getPoints() {
        assertEquals(prueba2.getPoints(),2);
    }

    @Test
    public void setPoints() {
        prueba.setPoints(5);
        assertEquals(prueba.getPoints(),5);
    }

    @Test
    public void getId() {
        assertEquals(prueba2.getId(),1);
    }

    @Test
    public void setId() {
        prueba.setId(4);
        assertEquals(prueba.getId(),4);
    }

    @Test
    public void getAutorScore() {
        assertEquals(prueba2.getAutorScore(),"autorprueba");
    }

    @Test
    public void setAutorScore() {
        prueba.setAutorScore("autor");
        assertEquals(prueba.getAutorScore(),"autor");
    }
}