package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class RecomendTest {

    private Recomend prueba =new Recomend(0, "prueba","prueba", "prueba");
    private Recomend prueba2 =new Recomend(1, "autorrecomend","filmname", "poster");

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getPosterPath() {
        assertEquals(prueba2.getPosterPath(),"poster");
    }

    @Test
    public void setPosterPath() {
        prueba.setPosterPath("Pruebaposter");
        assertEquals(prueba.getPosterPath(),"Pruebaposter");
    }

    @Test
    public void getFilmName() {
        assertEquals(prueba2.getFilmName(),"filmname");
    }

    @Test
    public void setFilmName() {
        prueba.setFilmName("Pruebanombre");
        assertEquals(prueba.getFilmName(),"Pruebanombre");
    }

    @Test
    public void getId() {
        assertEquals(prueba2.getId(),1);
    }

    @Test
    public void setId() {
        prueba.setId(0);
        assertEquals(prueba.getId(),0);
    }

    @Test
    public void getAutorRecomend() {
        assertEquals(prueba2.getAutorRecomend(),"autorrecomend");
    }

    @Test
    public void setAutorRecomend() {
        prueba.setAutorRecomend("autor");
        assertEquals(prueba.getAutorRecomend(),"autor");
    }
}