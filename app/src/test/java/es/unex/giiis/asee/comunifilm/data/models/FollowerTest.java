package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FollowerTest {

    private Follower follower = new Follower(1, 0, "Alberto", "Álvaro");

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getIdFollower() {
        assertEquals(follower.getIdFollower(), 1);

    }

    @Test
    public void setIdFollower() {
        follower.setIdFollower(3);
        assertEquals(follower.getIdFollower(), 3);
    }

    @Test
    public void getIdFollowed() {
        assertEquals(follower.getIdFollowed(), 0);
    }

    @Test
    public void setIdFollowed() {
        follower.setIdFollowed(4);
        assertEquals(follower.getIdFollowed(), 4);
    }

    @Test
    public void getNameFollower() {
        assertEquals(follower.getNameFollower(), "Alberto");
    }

    @Test
    public void setNameFollower() {
        follower.setNameFollower("Antonio");
        assertEquals(follower.getNameFollower(), "Antonio");
    }

    @Test
    public void getNameFollowed() {
        assertEquals(follower.getNameFollowed(), "Álvaro");
    }

    @Test
    public void setNameFollowed() {
        follower.setNameFollowed("Luis");
        assertEquals(follower.getNameFollowed(), "Luis");
    }
}