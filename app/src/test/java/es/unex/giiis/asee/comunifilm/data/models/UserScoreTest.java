package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class UserScoreTest {

    private UserScore uScore1;
    private UserScore uScore2;
    private UserScore uScore3;
    private UserScore uScore4;


    @Test
    public void testGetPoints() {

        assertEquals(uScore1.getPoints(),4);
        assertNotEquals(uScore2.getPoints(),3);
        assertEquals(uScore3.getPoints(),2);
        assertNotEquals(uScore4.getPoints(),6);
    }

    @Test
    public void testSetPoints() {
        uScore1.setPoints(1);
        uScore2.setPoints(2);
        uScore3.setPoints(3);
        uScore4.setPoints(4);
        assertEquals(uScore1.getPoints(),1);
        assertNotEquals(uScore2.getPoints(),3);
        assertEquals(uScore3.getPoints(),3);
        assertNotEquals(uScore4.getPoints(),6);
    }

    @Test
    public void testGetId() {

        assertEquals(uScore1.getId(), 1);
        assertEquals(uScore2.getId(), 2);
        assertEquals(uScore3.getId(), 3);
        assertEquals(uScore4.getId(), 4);
    }

    @Test
    public void testSetId() {

        uScore1.setId(4);
        uScore2.setId(23);
        uScore3.setId(325);
        uScore4.setId(52);
        assertNotEquals(uScore1.getId(), 1);
        assertNotEquals(uScore2.getId(), 2);
        assertNotEquals(uScore3.getId(), 3);
        assertNotEquals(uScore4.getId(), 4);
    }

    @Test
    public void testGetAutorScore() {
        assertEquals(uScore1.getAutorScore(), "Test");
        assertNotEquals(uScore1.getAutorScore(), "Dummy1");
        assertEquals(uScore2.getAutorScore(), "Dummy1");
        assertNotEquals(uScore2.getAutorScore(), "Dummy2");
        assertNotEquals(uScore3.getAutorScore(), "Dummy3");
        assertEquals(uScore4.getAutorScore(), "Dummy3");

    }

    @Test
    public void testSetAutorScore() {

        uScore1.setAutorScore("Corzo");
        uScore3.setAutorScore("Alvaro");
        uScore4.setAutorScore("Alberto");
        uScore2.setAutorScore("Cachorro");
        assertEquals(uScore1.getAutorScore(), "Corzo");
        assertNotEquals(uScore1.getAutorScore(), "Peri");
        assertEquals(uScore2.getAutorScore(), "Cachorro");
        assertNotEquals(uScore2.getAutorScore(), "Alvaro");
        assertNotEquals(uScore3.getAutorScore(), "Alberto");
        assertEquals(uScore4.getAutorScore(), "Alberto");

    }

    @Test
    public void testGetUserScored() {
        assertEquals(uScore1.getUserScored(), "Dummy1");
        assertNotEquals(uScore2.getUserScored(), "Dummy1");
        assertEquals(uScore3.getUserScored(), "Dummy3");
        assertNotEquals(uScore3.getUserScored(), "Corzo");
        assertEquals(uScore4.getUserScored(), "Test");


    }

    @Test
    public void testSetUserScored() {
        uScore2.setUserScored("Corzo");
        uScore4.setUserScored("Alvaro");
        uScore1.setUserScored("Alberto");
        uScore3.setUserScored("Cachorro");

        assertEquals(uScore1.getUserScored(), "Alberto");
        assertNotEquals(uScore2.getUserScored(), "Dummy1");
        assertEquals(uScore3.getUserScored(), "Cachorro");
        assertNotEquals(uScore3.getUserScored(), "Corzo");
        assertEquals(uScore4.getUserScored(), "Alvaro");
    }

    @Before
    public void initTest() {
        uScore1 = new UserScore(1,"Test", "Dummy1", 4);
        uScore2 = new UserScore(2,"Dummy1", "Dummy2", 5);
        uScore3 = new UserScore(3,"Dummy2", "Dummy3", 2);
        uScore4 = new UserScore(4,"Dummy3", "Test", 1);
    }
}