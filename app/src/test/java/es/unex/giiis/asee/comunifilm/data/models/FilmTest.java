package es.unex.giiis.asee.comunifilm.data.models;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FilmTest {

    Film film1;
    Film film2;
    Film film3;
    Film film4;

    @Before
    public void setUp() throws Exception {
        film1 = new Film(45.8, 50, true, "null", 1, false, "null",
                "spanish", "Treyarch", "Call of the Dead", 8.2, "good",
                "10-10-2020", 115, "action");

        film2 = new Film(96.0, 15, true, "null", 2, true, "null",
                "english", "Richtofen", "Der Riese", 2.7, "Wunderbar",
                "01-01-2000", 18883, "thriller");

        film3 = new Film(57.9, 27, false, "null", 3, false, "null",
                "english", "Samantha", "Tag der Toten", 6.1, "Fetch me their souls",
                "19-10-1964", 2, "romance");

        film4 = new Film(786.1, 8, false, "null", 4, true, "null",
                "spanish", "Group935", "Shangri-La", 8.4, "Ascension protocol",
                "09-03-1941", 6, "science-fiction");

    }

    @Test
    public void getIdUser() {
        assertEquals(film1.getIdUser(), 115);
        assertNotEquals(film2.getId(), 115);
        assertEquals(film3.getIdUser(), 2);
        assertNotEquals(film4.getIdUser(), 234);
    }

    @Test
    public void setIdUser() {
        film1.setIdUser(1);
        film2.setIdUser(2);
        film3.setIdUser(3);
        film4.setIdUser(4);

        assertEquals(film1.getIdUser(), 1);
        assertNotEquals(film2.getId(), 1);
        assertEquals(film3.getIdUser(), 3);
        assertNotEquals(film4.getIdUser(), 8);
    }

    @Test
    public void getPopularity() {
        assertEquals(java.util.Optional.ofNullable(film1.getPopularity()), java.util.Optional.ofNullable(45.8));
        assertNotEquals(film2.getPopularity(), 115.935);
        assertEquals(java.util.Optional.ofNullable(film3.getPopularity()), java.util.Optional.ofNullable(57.9));
        assertNotEquals(film4.getPopularity(), 234.06);
    }

    @Test
    public void setPopularity() {
        film1.setPopularity(1.0);
        film2.setPopularity(2.0);
        film3.setPopularity(3.0);
        film4.setPopularity(4.0);

        assertEquals(java.util.Optional.ofNullable(film1.getPopularity()), java.util.Optional.ofNullable(1.0));
        assertNotEquals(film2.getPopularity(), 115.935);
        assertEquals(java.util.Optional.ofNullable(film3.getPopularity()), java.util.Optional.ofNullable(3.0));
        assertNotEquals(film4.getPopularity(), 234.06);
    }

    @Test
    public void getVoteCount() {
        assertEquals(java.util.Optional.ofNullable(film1.getVoteCount()), java.util.Optional.ofNullable(50));
        assertNotEquals(java.util.Optional.ofNullable(film2.getVoteCount()), 50);
        assertEquals(java.util.Optional.ofNullable(film3.getVoteCount()), java.util.Optional.ofNullable(27));
        assertNotEquals(java.util.Optional.ofNullable(film4.getVoteCount()), 50);
    }

    @Test
    public void setVoteCount() {
        film1.setVoteCount(1);
        film2.setVoteCount(2);
        film3.setVoteCount(3);
        film4.setVoteCount(4);

        assertEquals(java.util.Optional.ofNullable(film1.getVoteCount()), java.util.Optional.ofNullable(1));
        assertNotEquals(java.util.Optional.ofNullable(film2.getVoteCount()), 50);
        assertEquals(java.util.Optional.ofNullable(film3.getVoteCount()), java.util.Optional.ofNullable(3));
        assertNotEquals(java.util.Optional.ofNullable(film4.getVoteCount()), 50);
    }

    @Test
    public void getVideo() {
        assertEquals(film1.getVideo(), true);
        assertNotEquals(film2.getVideo(), false);
        assertEquals(film3.getVideo(), false);
        assertNotEquals(film4.getVideo(), true);
    }

    @Test
    public void setVideo() {
        film1.setVideo(false);
        film2.setVideo(false);
        film3.setVideo(true);
        film4.setVideo(true);

        assertEquals(film1.getVideo(), false);
        assertNotEquals(film2.getVideo(), true);
        assertEquals(film3.getVideo(), true);
        assertNotEquals(film4.getVideo(), false);
    }

    @Test
    public void getPosterPath() {
        assertEquals(film1.getPosterPath(), "null");
        assertNotEquals(film2.getPosterPath(), "Bring me 115");
        assertEquals(film3.getPosterPath(), "null");
        assertNotEquals(film4.getPosterPath(), "");
    }

    @Test
    public void setPosterPath() {
        film1.setPosterPath("Lullaby");
        film2.setPosterPath("of a");
        film3.setPosterPath("dead");
        film4.setPosterPath("man");

        assertEquals(film1.getPosterPath(), "Lullaby");
        assertNotEquals(film2.getPosterPath(), "Bring me 115");
        assertEquals(film3.getPosterPath(), "dead");
        assertNotEquals(film4.getPosterPath(), "");
    }

    @Test
    public void getId() {
        assertEquals(film1.getId(), 1);
        assertNotEquals(film2.getId(),1);
        assertEquals(film3.getId(),3);
        assertNotEquals(film4.getId(),9);
    }

    @Test
    public void setId() {
        film1.setId(4);
        film2.setId(3);
        film3.setId(2);
        film4.setId(1);

        assertEquals(film1.getId(), 4);
        assertNotEquals(film2.getId(),1);
        assertEquals(film3.getId(),2);
        assertNotEquals(film4.getId(),4);
    }

    @Test
    public void getAdult() {
        assertEquals(film1.getAdult(), false);
        assertNotEquals(film2.getAdult(), false);
        assertEquals(film3.getAdult(), false);
        assertNotEquals(film4.getAdult(), false);
    }

    @Test
    public void setAdult() {
        film1.setAdult(true);
        film2.setAdult(false);
        film3.setAdult(true);
        film4.setAdult(false);

        assertEquals(film1.getAdult(), true);
        assertNotEquals(film2.getAdult(), true);
        assertEquals(film3.getAdult(), true);
        assertNotEquals(film4.getAdult(), true);
    }

    @Test
    public void getBackdropPath() {
        assertEquals(film1.getBackdropPath(), "null");
        assertNotEquals(film2.getBackdropPath(), "Bring me 115");
        assertEquals(film3.getBackdropPath(), "null");
        assertNotEquals(film4.getBackdropPath(), "");
    }

    @Test
    public void setBackdropPath() {
        film1.setBackdropPath("Where");
        film2.setBackdropPath("are");
        film3.setBackdropPath("we");
        film4.setBackdropPath("going");

        assertEquals(film1.getBackdropPath(), "Where");
        assertNotEquals(film2.getBackdropPath(), "Bring me 115");
        assertEquals(film3.getBackdropPath(), "we");
        assertNotEquals(film4.getBackdropPath(), "");
    }

    @Test
    public void getOriginalLanguage() {
        assertEquals(film1.getOriginalLanguage(), "spanish");
        assertNotEquals(film2.getOriginalLanguage(), "Bring me 115");
        assertEquals(film3.getOriginalLanguage(), "english");
        assertNotEquals(film4.getOriginalLanguage(), "");
    }

    @Test
    public void setOriginalLanguage() {
        film1.setOriginalLanguage("Indian");
        film2.setOriginalLanguage("Italian");
        film3.setOriginalLanguage("Portuguese");
        film4.setOriginalLanguage("Castuo");

        assertEquals(film1.getOriginalLanguage(), "Indian");
        assertNotEquals(film2.getOriginalLanguage(), "Bring me 115");
        assertEquals(film3.getOriginalLanguage(), "Portuguese");
        assertNotEquals(film4.getOriginalLanguage(), "");
    }

    @Test
    public void getOriginalTitle() {
        assertEquals(film1.getOriginalTitle(), "Treyarch");
        assertNotEquals(film2.getOriginalTitle(), "MAXIS");
        assertEquals(film3.getOriginalTitle(), "Samantha");
        assertNotEquals(film4.getOriginalTitle(), "ASCENSION");
    }

    @Test
    public void setOriginalTitle() {
        film1.setOriginalTitle("Marlton");
        film2.setOriginalTitle("Misty");
        film3.setOriginalTitle("Russman");
        film4.setOriginalTitle("Stuhlinger");

        assertEquals(film1.getOriginalTitle(), "Marlton");
        assertNotEquals(film2.getOriginalTitle(), "MAXIS");
        assertEquals(film3.getOriginalTitle(), "Russman");
        assertNotEquals(film4.getOriginalTitle(), "ASCENSION");
    }

    @Test
    public void getTitle() {
        assertEquals(film1.getTitle(), "Call of the Dead");
        assertNotEquals(film2.getTitle(), "MAXIS");
        assertEquals(film3.getTitle(), "Tag der Toten");
        assertNotEquals(film4.getTitle(), "ASCENSION");
    }

    @Test
    public void setTitle() {
        film1.setTitle("Nacht der Untoten");
        film2.setTitle("Verruckt");
        film3.setTitle("Shi No Numa");
        film4.setTitle("The Giant");

        assertEquals(film1.getTitle(), "Nacht der Untoten");
        assertNotEquals(film2.getTitle(), "MAXIS");
        assertEquals(film3.getTitle(), "Shi No Numa");
        assertNotEquals(film4.getTitle(), "ASCENSION");
    }

    @Test
    public void getVoteAverage() {
        assertEquals(java.util.Optional.ofNullable(film1.getVoteAverage()), java.util.Optional.ofNullable(8.2));
        assertNotEquals(film2.getVoteAverage(), 7.7);
        assertEquals(java.util.Optional.ofNullable(film3.getVoteAverage()), java.util.Optional.ofNullable(6.1));
        assertNotEquals(film4.getVoteAverage(), 0.0);
    }

    @Test
    public void setVoteAverage() {
        film1.setVoteAverage(1.0);
        film2.setVoteAverage(2.0);
        film3.setVoteAverage(3.0);
        film4.setVoteAverage(4.0);

        assertEquals(java.util.Optional.ofNullable(film1.getVoteAverage()), java.util.Optional.ofNullable(1.0));
        assertNotEquals(film2.getVoteAverage(), 7.7);
        assertEquals(java.util.Optional.ofNullable(film3.getVoteAverage()), java.util.Optional.ofNullable(3.0));
        assertNotEquals(film4.getVoteAverage(), 0.0);
    }

    @Test
    public void getOverview() {
        assertEquals(film1.getOverview(),"good");
        assertNotEquals(film2.getOverview(), "bad wolf");
        assertEquals(film3.getOverview(),"Fetch me their souls");
        assertNotEquals(film4.getOverview(), "one more time");
    }

    @Test
    public void setOverview() {
        film1.setOverview("meh");
        film2.setOverview("pff");
        film3.setOverview("bleh");
        film4.setOverview("ssldfusldf");

        assertEquals(film1.getOverview(),"meh");
        assertNotEquals(film2.getOverview(), "bad wolf");
        assertEquals(film3.getOverview(),"bleh");
        assertNotEquals(film4.getOverview(), "one more time");
    }

    @Test
    public void getReleaseDate() {
        assertEquals(film1.getReleaseDate(), "10-10-2020");
        assertNotEquals(film2.getReleaseDate(), "10-10-2020");
        assertEquals(film3.getReleaseDate(), "19-10-1964");
        assertNotEquals(film4.getReleaseDate(), "10-10-2020");
    }

    @Test
    public void setReleaseDate() {
        film1.setReleaseDate("01-01-1990");
        film2.setReleaseDate("01-01-1991");
        film3.setReleaseDate("01-01-1992");
        film4.setReleaseDate("01-01-1993");

        assertEquals(film1.getReleaseDate(), "01-01-1990");
        assertNotEquals(film2.getReleaseDate(), "10-10-2020");
        assertEquals(film3.getReleaseDate(), "01-01-1992");
        assertNotEquals(film4.getReleaseDate(), "10-10-2020");
    }

    @Test
    public void getType() {
        assertEquals(film1.getType(), "action");
        assertNotEquals(film2.getType(),"2d");
        assertEquals(film3.getType(), "romance");
        assertNotEquals(film4.getType(), "musical");
    }

    @Test
    public void setType() {
        film1.setType("Teddy");
        film2.setType("is the");
        film3.setType("biggest");
        film4.setType("liar");

        assertEquals(film1.getType(), "Teddy");
        assertNotEquals(film2.getType(),"2d");
        assertEquals(film3.getType(), "biggest");
        assertNotEquals(film4.getType(), "musical");
    }
}