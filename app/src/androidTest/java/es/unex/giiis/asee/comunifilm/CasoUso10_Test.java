package es.unex.giiis.asee.comunifilm;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CasoUso10_Test {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void casoUso10_Test() {
        ViewInteraction materialButton = onView(
                allOf(withId(R.id.bRegister), withText("¡Regístrate!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                2),
                        isDisplayed()));
        materialButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.eTUserName),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                7),
                        isDisplayed()));
        appCompatEditText.perform(replaceText("Corzo"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.eTPassword1),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                4),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("password"), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.eTPassword2),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                2),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("password"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.eTPassword2), withText("password"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                2),
                        isDisplayed()));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.eTEmail),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText5.perform(replaceText("dcorzogo@alumnos.unex.es"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.eTEmail), withText("dcorzogo@alumnos.unex.es"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText6.perform(pressImeActionButton());

        ViewInteraction materialButton2 = onView(
                allOf(withId(R.id.bFinishRegistration), withText("¡Regístrate ahora!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                9),
                        isDisplayed()));
        materialButton2.perform(click());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.eTSearchFilm),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.searchFilmFragment),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText7.perform(replaceText("d"), closeSoftKeyboard());

        ViewInteraction materialButton3 = onView(
                allOf(withId(R.id.bSearch), withText("Buscar"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.searchFilmFragment),
                                        0),
                                3),
                        isDisplayed()));
        materialButton3.perform(click());

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Open navigation drawer"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction appCompatImageView = onView(
                allOf(withId(R.id.AvatarHeaderView), withContentDescription("Navigation header"),
                        childAtPosition(
                                allOf(withId(R.id.headerLayout),
                                        childAtPosition(
                                                withId(R.id.navigation_header_container),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageView.perform(click());

        ViewInteraction materialButton4 = onView(
                allOf(withId(R.id.bBorrarUsuario), withText("Borrar"),
                        childAtPosition(
                                childAtPosition(
                                        withClassName(is("android.widget.FrameLayout")),
                                        0),
                                0),
                        isDisplayed()));
        materialButton4.perform(click());

        ViewInteraction materialButton5 = onView(
                allOf(withId(android.R.id.button1), withText("Yes"),
                        childAtPosition(
                                allOf(withClassName(is("com.android.internal.widget.ButtonBarLayout")),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                3)),
                                3),
                        isDisplayed()));
        materialButton5.perform(click());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
