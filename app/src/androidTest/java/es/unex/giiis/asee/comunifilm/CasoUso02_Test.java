package es.unex.giiis.asee.comunifilm;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CasoUso02_Test {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void casoUso02_Test() throws InterruptedException {
        ViewInteraction materialButton = onView(
                allOf(withId(R.id.bRegister), withText("¡Regístrate!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        1),
                                2),
                        isDisplayed()));
        materialButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.eTUserName),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                7),
                        isDisplayed()));
        appCompatEditText.perform(replaceText("aaaa"), closeSoftKeyboard());

        //pressBack();

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.eTPassword1),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                4),
                        isDisplayed()));
        appCompatEditText2.perform(replaceText("12345678"), closeSoftKeyboard());

        //pressBack();

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.eTPassword2),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                2),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("12345678"), closeSoftKeyboard());

        //pressBack();

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.eTEmail),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText4.perform(replaceText("lms@o.com"), closeSoftKeyboard());

        //pressBack();

        ViewInteraction materialButton2 = onView(
                allOf(withId(R.id.bFinishRegistration), withText("¡Regístrate ahora!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                9),
                        isDisplayed()));
        materialButton2.perform(click());

        Thread.sleep(1000);

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.FilmList),
                        childAtPosition(
                                withId(R.id.swipeRefreshLayout),
                                0)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        Thread.sleep(500);

        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.bComentar),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.scrollView),
                                        0),
                                17)));
        appCompatImageButton.perform(scrollTo(), click());

        Thread.sleep(500);

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.eTComment),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.custom),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText5.perform(replaceText("test"), closeSoftKeyboard());

        Thread.sleep(500);

        ViewInteraction materialButton3 = onView(
                allOf(withId(android.R.id.button1), withText("Comentar"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)));
        materialButton3.perform(scrollTo(), click());

        Thread.sleep(500);

        ViewInteraction textView = onView(
                allOf(withId(R.id.tVComment), withText("test"),
                        withParent(withParent(withId(R.id.recyclerComment))),
                        isDisplayed()));
        textView.check(matches(withText("test")));

        Thread.sleep(500);

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Desplazarse hacia arriba"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("com.google.android.material.appbar.AppBarLayout")),
                                                0)),
                                1),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        Thread.sleep(500);

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.FilmList),
                        childAtPosition(
                                withId(R.id.swipeRefreshLayout),
                                0)));
        recyclerView2.perform(actionOnItemAtPosition(1, click()));

        Thread.sleep(500);

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withId(R.id.bComentar),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.scrollView),
                                        0),
                                17)));
        appCompatImageButton3.perform(scrollTo(), click());

        Thread.sleep(500);

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.eTComment),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.custom),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText6.perform(replaceText("test2"), closeSoftKeyboard());

        Thread.sleep(500);

        ViewInteraction materialButton4 = onView(
                allOf(withId(android.R.id.button1), withText("Comentar"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.buttonPanel),
                                        0),
                                3)));
        materialButton4.perform(scrollTo(), click());

        Thread.sleep(500);

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.tVComment), withText("test2"),
                        withParent(withParent(withId(R.id.recyclerComment))),
                        isDisplayed()));
        textView2.check(matches(withText("test2")));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
