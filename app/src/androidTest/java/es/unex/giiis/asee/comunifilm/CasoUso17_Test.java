package es.unex.giiis.asee.comunifilm;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CasoUso17_Test {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void casoUso17_Test() {
        ViewInteraction materialButton = onView(
                allOf(withId(R.id.bRegister), withText("¡Regístrate!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        1),
                                2),
                        isDisplayed()));
        materialButton.perform(click());

        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.eTUserName),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                7),
                        isDisplayed()));
        appCompatEditText.perform(replaceText("aaaa"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.eTUserName), withText("aaaa"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                7),
                        isDisplayed()));
        appCompatEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.eTPassword1),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                4),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("12345678"), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.eTPassword1), withText("12345678"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                4),
                        isDisplayed()));
        appCompatEditText4.perform(pressImeActionButton());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.eTPassword2),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                2),
                        isDisplayed()));
        appCompatEditText5.perform(replaceText("12345678"), closeSoftKeyboard());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.eTPassword2), withText("12345678"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                2),
                        isDisplayed()));
        appCompatEditText6.perform(pressImeActionButton());

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.eTEmail),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText7.perform(replaceText("lm@o.com"), closeSoftKeyboard());

        pressBack();

        ViewInteraction materialButton2 = onView(
                allOf(withId(R.id.bFinishRegistration), withText("¡Regístrate ahora!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.registerOrLoggerFragment),
                                        0),
                                9),
                        isDisplayed()));
        materialButton2.perform(click());

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.FilmList),
                        childAtPosition(
                                withId(R.id.swipeRefreshLayout),
                                0)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction appCompatImageButton = onView(
                allOf(withId(R.id.bCompartir),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.scrollView),
                                        0),
                                18)));
        appCompatImageButton.perform(scrollTo(), click());

        ViewInteraction scrollView = onView(
                allOf(IsInstanceOf.<View>instanceOf(android.widget.ScrollView.class),
                        withParent(allOf(withId(android.R.id.content),
                                withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class)))),
                        isDisplayed()));
        scrollView.check(matches(isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
