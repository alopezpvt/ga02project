package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.Wish;

public class ListAllWishFragmentViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;
    private LiveData<List<Wish>> mAllwish;


    public ListAllWishFragmentViewModel(RepositoryFilms mRepositoryFilms) {

        this.mRepositoryFilms = mRepositoryFilms;
        mAllwish = mRepositoryFilms.getCurrentdeseadas();
    }

    public LiveData<List<Wish>> getallwish() {
        return mRepositoryFilms.getCurrentdeseadas();
    }
}