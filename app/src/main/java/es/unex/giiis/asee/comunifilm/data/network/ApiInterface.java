package es.unex.giiis.asee.comunifilm.data.network;


import es.unex.giiis.asee.comunifilm.data.models.Genres;
import es.unex.giiis.asee.comunifilm.data.models.ResultSearchFilm;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    public static final String BASE_URL = "https://api.themoviedb.org";
    public static final String API_KEY = "5e401542b701d60f6992434b292a6b7c";
    public static final String LANGUAGE = "en-ES";

    @GET("/3/search/movie")
    Call<ResultSearchFilm> searchFilm(

            @Query("api_key") String apiKey,
            @Query("languaje") String languaje,
            @Query("query") String film


    );

    @GET("/3/movie/popular")
    Call<ResultSearchFilm> popularFilm(
            @Query("api_key") String apiKey,
            @Query("languaje") String languaje

    );

    @GET("/3/movie/top_rated")
    Call<ResultSearchFilm> topFilms(
            @Query("api_key") String apiKey,
            @Query("languaje") String languaje

    );

    @GET("/3/genre/movie/list")
    Call<Genres> genres(
            @Query("api_key") String apiKey,
            @Query("languaje") String languaje

    );
}
