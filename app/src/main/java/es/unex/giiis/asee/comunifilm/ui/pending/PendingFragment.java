package es.unex.giiis.asee.comunifilm.ui.pending;


import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.HideKeyboard;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterWish;
import es.unex.giiis.asee.comunifilm.data.models.Wish;



public class PendingFragment extends Fragment {

    private PendingViewModel mViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list_all_films_wish, container, false);
        HideKeyboard.hideKeyboard(getActivity());

        RecyclerView recyclerView = v.findViewById(R.id.recyclerRWish);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        AdapterWish adapterWish = new AdapterWish(new ArrayList<Wish>());
        recyclerView.setAdapter(adapterWish);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factorypending).get(PendingViewModel.class);

        mViewModel.getdeseadas().observe(getActivity(), deseadas -> AppExecutors.getInstance().mainThread().execute(() -> {
            adapterWish.swap((ArrayList<Wish>) deseadas);
        }));


        return v;
    }
}