package es.unex.giiis.asee.comunifilm.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Wish;

public class AdapterWish extends RecyclerView.Adapter<AdapterWish.ViewHolderWish> {

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    ArrayList<Wish> listWish;
    Context context;
    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public AdapterWish(ArrayList<Wish> listWish) {
        this.listWish = listWish;
    }

    @NonNull
    @Override
    public ViewHolderWish onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wish_list, parent, false);
        context = parent.getContext();
        return new AdapterWish.ViewHolderWish(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderWish holder, int position) {
        holder.setWish(listWish.get(position), context);
    }

    @Override
    public int getItemCount() {
        return listWish.size();
    }

    public void swap(ArrayList<Wish> listWish) {
        this.listWish = listWish;
        notifyDataSetChanged();

    }


    public class ViewHolderWish extends RecyclerView.ViewHolder {

        TextView title;
        ImageView posterFilm;


        public ViewHolderWish(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);


            title = itemView.findViewById(R.id.tVTitlewish);
            posterFilm = itemView.findViewById(R.id.imgPosterFilmWish);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int pos = getAdapterPosition();
                        if (pos != RecyclerView.NO_POSITION) {
                            listener.onItemClick(pos);
                        }
                    }
                }
            });

        }

        public void setWish(Wish wish, Context context) {


            title.setText(wish.getFilmName());
            Log.i("adapter: ", "contexto" + context.toString());

            if (wish.getPosterPath() != null)
                Glide.with(context).load(wish.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(posterFilm);


        }
    }

}
