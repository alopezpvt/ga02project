package es.unex.giiis.asee.comunifilm.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;

import java.util.ArrayList;
import java.util.List;

import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.HideKeyboard;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterSearchFilms;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.Genre;
import es.unex.giiis.asee.comunifilm.data.models.Genres;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;
import es.unex.giiis.asee.comunifilm.data.network.FilmsGenresNetworkLoaderRuneable;
import es.unex.giiis.asee.comunifilm.data.network.OnGenresLoadedListener;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.SearchFilmFragmentViewModel;

import static android.content.Context.MODE_PRIVATE;


public class SearchFilmFragment extends Fragment {

    private final ArrayList<Film> listFilm = new ArrayList<Film>();
    private String commentOrRecommend;
    private ListOfList listOfList;
    private ChipGroup cgCategories;
    private Genres genres;

    private AdapterSearchFilms mAdapter;
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private String userName;
    private SharedPreferences settings;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            commentOrRecommend = getArguments().getString("CommentOrRecommend");
            listOfList = (ListOfList) getArguments().getSerializable("listOfList");

            Log.i("Personal: ", "commentOrRecomend: " + commentOrRecommend);

            if (listOfList != null)
                Log.i("Personal: ", "And idList: " + listOfList.getIdList());
        }

    }

    private void showLoading() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setVisibility(View.INVISIBLE);
    }

    private void showReposDataView() {
        mProgressBar.setVisibility(View.GONE);
        mSwipeRefreshLayout.setRefreshing(false);
        mRecyclerView.setVisibility(View.VISIBLE);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search_film, container, false);
        HideKeyboard.hideKeyboard(getActivity());


        EditText eTSearchFilm = v.findViewById(R.id.eTSearchFilm);

        Button bSearch = v.findViewById(R.id.bSearch);

        cgCategories = v.findViewById(R.id.cgCategories);

        mRecyclerView = v.findViewById(R.id.FilmList);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mAdapter = new AdapterSearchFilms(new ArrayList<Film>());
        mRecyclerView.setAdapter(mAdapter);
        mProgressBar = v.findViewById(R.id.progressBar);


        RecyclerView recyclerView = v.findViewById(R.id.recyclerSearchFilm);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        AdapterSearchFilms adapterSearchFilms = new AdapterSearchFilms(new ArrayList<Film>());
        recyclerView.setAdapter(adapterSearchFilms);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        SearchFilmFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factorysearch).get(SearchFilmFragmentViewModel.class);
        settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
        userName = settings.getString("userName", "none");
        mViewModel.setuser(userName);

        settingGenres(inflater);

        mViewModel.populares();
        mViewModel.getbusqueda().observe(getViewLifecycleOwner(), (Observer<List<Film>>) new Observer<List<Film>>() {
            @Override
            public void onChanged(List<Film> busqueda) {
                Log.i("Listaprincipal: ", "numero: " + busqueda.size());
                mProgressBar.setVisibility(View.GONE);
                mSwipeRefreshLayout.setRefreshing(false);

                AppExecutors.getInstance().mainThread().execute(() -> {
                    mAdapter.swap((ArrayList<Film>) busqueda);
                });

                mRecyclerView.setAdapter(mAdapter);


                cgCategories.setOnCheckedChangeListener(new ChipGroup.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(ChipGroup group, int checkedId) {
                        Chip selectedChip = (Chip) group.findViewById(checkedId);
                        boolean exit = false;
                        List<Genre> g = genres.getListGenres();

                        Genre aux = null;
                        for (int i = 0; i < g.size() && !exit; i++) {
                            aux = g.get(i);
                            if (aux.getName() == selectedChip.getText()) {
                                exit = true;

                            }
                        }

                        ArrayList<Film> filmByGenre = new ArrayList<>();

                        if (exit) {

                            for (Film f : busqueda) {
                                Log.i("genero", "Generoaux: " + f.toString());
                                Log.i("genero", "Generoaux: " + aux.getId());
                                Log.i("genero", "Generos: " + f.getGenreIds());
                                if (f.getGenreIds()!=null&&f.getGenreIds().equals(aux.getId())) {
                                    filmByGenre.add(f);
                                }

                            }

                            AppExecutors.getInstance().mainThread().execute(() -> {
                                mAdapter.swap((ArrayList<Film>) filmByGenre);
                            });

                        }else {
                            AppExecutors.getInstance().mainThread().execute(() -> {
                                mAdapter.swap((ArrayList<Film>) busqueda);
                            });
                        }

                    }
                });


                mAdapter.setOnItemClickListener(pos -> {

                    Log.i("Click: ", "clicking: " + busqueda.get(pos).getTitle());

                    Bundle bundle = new Bundle();
                    Film popular = busqueda.get(pos);
                    bundle.putSerializable("film", popular);

                    SearchFilmFragment.this.commentOrRecommendSelector(bundle, v);
                });


                // Show the repo list or the loading screen based on whether the repos data exists and is loaded
                if (busqueda != null && busqueda.size() != 0)
                    SearchFilmFragment.this.showReposDataView();
                else {
                    SearchFilmFragment.this.showLoading();
                }

            }
        });

        bSearch.setOnClickListener(view -> {
            if (!eTSearchFilm.getText().toString().isEmpty()) {
                mViewModel.settitle(eTSearchFilm.getText().toString());
                eTSearchFilm.setText("");
                HideKeyboard.hideKeyboard(getActivity());

            }
        });

        mSwipeRefreshLayout = v.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(() -> mViewModel.onRefresh());

        //getActivity().onBackPressed();
        return v;
    }


    private void commentOrRecommendSelector(Bundle bundle, View v) {


        if (commentOrRecommend.equals("see")) {
            DetailsFragment detailsFragment = new DetailsFragment();

            detailsFragment.setArguments(bundle);
            Navigation.findNavController(v).navigate(R.id.action_nav_home_to_detailsFragment, bundle);

        }

        if (commentOrRecommend.equals("addList")) {
            bundle.putSerializable("listOfList", listOfList);
            Navigation.findNavController(v).navigate(R.id.action_searchFilmFragment_to_addPersonalListFragment, bundle);

        }
    }


    private void settingGenres(LayoutInflater inflater) {
        AppExecutors.getInstance().networkIO().execute(new FilmsGenresNetworkLoaderRuneable(listGenre -> {
            genres = listGenre;

            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {

                    Chip chip;
                    for (Genre genre : genres.getListGenres()) {
                        chip = (Chip) inflater.inflate(R.layout.chip_choice, cgCategories, false);
                        chip.setText(genre.getName());
                        if(genre.getId()==28||genre.getId()==10751||genre.getId()==36||genre.getId()==878||genre.getId()==10752)
                        cgCategories.addView(chip);
                    }
                }
            });
        }));
    }

}