package es.unex.giiis.asee.comunifilm.ui.friends;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import es.unex.giiis.asee.comunifilm.HideKeyboard;
import es.unex.giiis.asee.comunifilm.R;

public class friendsFragment extends Fragment {


    public static friendsFragment newInstance() {
        return new friendsFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.friends_fragment, container, false);
        HideKeyboard.hideKeyboard(getActivity());

        Button bBuscarUsuario = v.findViewById(R.id.bBuscarUsuario);
        Bundle bundle = new Bundle();

        bBuscarUsuario.setOnClickListener(v1 -> {
            bundle.putString("CommentOrRecommend", "searchUser");
            Navigation.findNavController(v1).navigate(R.id.action_nav_friends_to_searchUserFragment, bundle);
        });

        Button bSiguiendo = v.findViewById(R.id.bSiguiendo);

        bSiguiendo.setOnClickListener(v12 -> {
            bundle.putString("CommentOrRecommend", "listUserFollowing");
            Navigation.findNavController(v12).navigate(R.id.action_nav_friends_to_searchUserFragment, bundle);
        });

        Button bSeguidores = v.findViewById(R.id.bSeguidores);

        bSeguidores.setOnClickListener(v13 -> {
            bundle.putString("CommentOrRecommend", "listUserFollowed");
            Navigation.findNavController(v13).navigate(R.id.action_nav_friends_to_searchUserFragment, bundle);
        });

        return v;
    }

}