package es.unex.giiis.asee.comunifilm.fragments.factory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.DetailsFragmentViewModel;

public class DetailsViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final RepositoryUsers mRepositoryUsers;
    private final RepositoryFilms mrepositoryFilms;

    public DetailsViewModelFactory(RepositoryUsers mRepositoryUsers, RepositoryFilms mrepositoryFilms) {
        this.mRepositoryUsers = mRepositoryUsers;
        this.mrepositoryFilms = mrepositoryFilms;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new DetailsFragmentViewModel(mRepositoryUsers,mrepositoryFilms);
    }
}
