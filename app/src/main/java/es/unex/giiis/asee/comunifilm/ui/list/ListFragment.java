package es.unex.giiis.asee.comunifilm.ui.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import es.unex.giiis.asee.comunifilm.HideKeyboard;
import es.unex.giiis.asee.comunifilm.R;

public class ListFragment extends Fragment {



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_list, container, false);
        HideKeyboard.hideKeyboard(getActivity());


        Button bListaPuntuaciones= v.findViewById(R.id.bListaPuntuaciones);

        bListaPuntuaciones.setOnClickListener(v1 -> Navigation.findNavController(v1).navigate(R.id.action_nav_list_to_listAllFilmsScoreFragment));
        Button bFavFilm = v.findViewById(R.id.bFavFilm);

        bFavFilm.setOnClickListener(v12 -> Navigation.findNavController(v12).navigate(R.id.action_nav_list_to_listAllFilmsFavFragment));

        Button bTop= v.findViewById(R.id.bTop);

        bTop.setOnClickListener(v13 -> Navigation.findNavController(v13).navigate(R.id.action_nav_list_to_topFragment));

        Button bOwnList = v.findViewById(R.id.bOwnList);

        bOwnList.setOnClickListener(v14 -> Navigation.findNavController(v14).navigate(R.id.action_nav_list_to_listOfListFragment));


        Button bScore= v.findViewById(R.id.bScoredUser);

        bScore.setOnClickListener(v15 -> Navigation.findNavController(v15).navigate(R.id.action_nav_list_to_listAllUserScoreFragment));

        Button bRecomendedFilm = v.findViewById(R.id.bRecomendedFilm);

        bRecomendedFilm.setOnClickListener(v16 -> Navigation.findNavController(v16).navigate(R.id.action_nav_list_to_listAllFilmsRecomendedFragment));


        return v;
    }
}