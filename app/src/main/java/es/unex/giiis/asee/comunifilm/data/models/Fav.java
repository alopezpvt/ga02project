package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "FAV")
public class Fav {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String autorFav;
    private String filmName;
    private String posterPath;


    public Fav(long id, String autorFav, String filmName, String posterPath) {
        this.id = id;
        this.autorFav = autorFav;
        this.filmName = filmName;
        this.posterPath = posterPath;
    }

    @Ignore
    public Fav(String autorFav, String filmName, String posterPath) {
        this.id = id;
        this.autorFav = autorFav;
        this.filmName = filmName;
        this.posterPath = posterPath;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAutorFav() {
        return autorFav;
    }

    public void setAutorFav(String autorFav) {
        this.autorFav = autorFav;
    }


    @Override
    public String toString() {
        return "autorFav{" +
                "id=" + id +
                ", autorFav='" + autorFav + '\'' +
                ", filmName='" + filmName + '\'' +
                ", posterPath='" + posterPath + '\'' +
                '}';
    }
}
