package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "WISH")
public class Wish {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String autorWish;
    private String filmName;
    private String posterPath;
    private Double voteAverage;
    private Integer voteCount;
    private String releaseDate;
    private String overview;

    public Wish(long id, String autorWish, String filmName, String posterPath) {
        this.id = id;
        this.autorWish = autorWish;
        this.filmName = filmName;
        this.posterPath = posterPath;
    }

    @Ignore
    public Wish(String autorWish, String filmName, String posterPath, Double voteAverage, Integer voteCount, String releaseDate, String overview) {
        this.id = id;
        this.autorWish = autorWish;
        this.filmName = filmName;
        this.posterPath = posterPath;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
        this.releaseDate = releaseDate;
        this.overview = overview;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAutorWish() {
        return autorWish;
    }

    public void setAutorWish(String autorWish) {
        this.autorWish = autorWish;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }


    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }


    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    @Override
    public String toString() {
        return "Wish{" +
                "id=" + id +
                ", autorWish='" + autorWish + '\'' +
                ", filmName='" + filmName + '\'' +
                ", posterPath='" + posterPath + '\'' +
                '}';
    }
}
