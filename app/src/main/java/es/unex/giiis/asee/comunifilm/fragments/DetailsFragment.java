package es.unex.giiis.asee.comunifilm.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterComments;
import es.unex.giiis.asee.comunifilm.data.models.Comment;
import es.unex.giiis.asee.comunifilm.data.models.Fav;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.Recomend;
import es.unex.giiis.asee.comunifilm.data.models.Score;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.data.models.Wish;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.DetailsFragmentViewModel;


import static android.content.Context.MODE_PRIVATE;

public class DetailsFragment extends Fragment {

    public static final int COMMENT_DIALOG = 1;


    private Film film;
    private int votos;
    private long iduser;
    private String userName;
    private SharedPreferences settings;
    private ArrayList<Comment> listComments;
    private RecyclerView recycler;
    private RecyclerView.LayoutManager layoutManager;
    private AdapterComments adapterComments;
    private DetailsFragmentViewModel mViewModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            film = (Film) getArguments().getSerializable("film");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_details, container, false);

        settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
        userName = settings.getString("userName", "none");
        iduser = settings.getLong("userID", -1);

        recycler = v.findViewById(R.id.recyclerComment);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        listComments = new ArrayList<Comment>();

        adapterComments = new AdapterComments(listComments);
        recycler.setAdapter(adapterComments);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factorydetails).get(DetailsFragmentViewModel.class);

        mViewModel.settitlefilm(film.getTitle());
        mViewModel.setidfilm(film.getId());


        ImageButton bCambiarFecha = v.findViewById(R.id.bCambiarFecha);
        ImageButton bFavorito = v.findViewById(R.id.bFavorito);
        ImageButton b1pDetails = v.findViewById(R.id.b1pDetails);
        ImageButton b2pDetails = v.findViewById(R.id.b2pDetails);
        ImageButton b3pDetails = v.findViewById(R.id.b3pDetails);
        ImageButton b4pDetails = v.findViewById(R.id.b4pDetails);
        ImageButton b5pDetails = v.findViewById(R.id.b5pDetails);
        ImageButton bRecomendar = v.findViewById(R.id.bRecomendar);
        ImageButton bDeseadas = v.findViewById(R.id.bDeseadas);
        ImageButton bComentar = v.findViewById(R.id.bComentar);
        ImageButton bBorrarPeli = v.findViewById(R.id.bBorrarPeli);
        ImageButton bCompartir = v.findViewById(R.id.bCompartir);

        ImageView imgPosterFilm = v.findViewById(R.id.imgDetailsPoster);
        if (film.getPosterPath() != null)
            Glide.with(getContext()).load(film.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(imgPosterFilm);

        TextView title = v.findViewById(R.id.tVTitleDetails);
        TextView tVdescriptionDetails = v.findViewById(R.id.tVdescriptionDetails);
        TextView tVpuntuacionDetails = v.findViewById(R.id.tVpuntuacionDetails);
        TextView tVvotosDetails = v.findViewById(R.id.tVvotosDetails);
        TextView tVdateDetails = v.findViewById(R.id.tVdateDetails);
        title.setText(film.getTitle());
        tVdescriptionDetails.setText(film.getOverview());
        tVpuntuacionDetails.setText(String.valueOf(film.getVoteAverage() / 2));
        tVdateDetails.setText(film.getReleaseDate());

        getScoreByAutor(tVvotosDetails);

        getCommetList();


        if (settings.getString("userName", "none").equals("none")) {
            AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Debes estar registrado para interactuar", Toast.LENGTH_SHORT).show());

        } else {


            recommendButton(bRecomendar);

            scoreButton(b1pDetails, tVvotosDetails, 1);
            scoreButton(b2pDetails, tVvotosDetails, 2);
            scoreButton(b3pDetails, tVvotosDetails, 3);
            scoreButton(b4pDetails, tVvotosDetails, 4);
            scoreButton(b5pDetails, tVvotosDetails, 5);

            favouriteButton(bFavorito);

            wishButton(bDeseadas);

            commentButton(bComentar);

            changeDateButton(bCambiarFecha);

            shareButton(bCompartir);

            deleteFilmButton(bBorrarPeli, v);


        }
        return v;
    }


    private void getScoreByAutor(TextView tVvotosDetails) {

        AppExecutors.getInstance().networkIO().execute(() -> {


            if (film.getPuntuacion() == 0)
                votos = film.getVoteCount();
            else votos = film.getVoteCount() + 1;

            AppExecutors.getInstance().mainThread().execute(() -> {
                tVvotosDetails.setText(String.valueOf(votos));
            });
        });
    }

    private void getCommetList() {

        mViewModel.getcomentarios().observe(getActivity(), comentarios -> AppExecutors.getInstance().mainThread().execute(() -> {
            AdapterComments adapterComments = new AdapterComments((ArrayList<Comment>) comentarios);
            recycler.setAdapter(adapterComments);

        }));
    }

    private void recommendButton(ImageButton bRecomendar) {

        bRecomendar.setOnClickListener(v -> {
            Recomend recomend;
            recomend = new Recomend(userName, film.getTitle(), film.getPosterPath());

            if (film.getRecomendada()) {
                film.setRecomendada(false);

                Toast.makeText(getContext(), "Has eleminado esta Pelicula de Recomendadas", Toast.LENGTH_SHORT).show();
                AppExecutors.getInstance().diskIO().execute(() -> {
                    mViewModel.updatefilm(film);
                    mViewModel.eleminarrecomendada(recomend);
                });

            } else {
                film.setRecomendada(true);

                Toast.makeText(getContext(), "Has añadido a recomendadas esta Pelicula", Toast.LENGTH_SHORT).show();
                AppExecutors.getInstance().diskIO().execute(() -> {
                    mViewModel.updatefilm(film);
                    mViewModel.insertarrecomendada(recomend);
                });

            }

        });
    }

    private void scoreButton(ImageButton puntuar, TextView tVvotosDetails, int point) {

        puntuar.setOnClickListener(v -> {


            Score score = new Score(userName, film.getTitle(), film.getPosterPath(), 1);

            if (film.getPuntuacion() != 0) {
                film.setPuntuacion(point);

                score.setPoints(point);
                Toast.makeText(getContext(), "Has actualizado la puntuacion de esta pelicula con " + point, Toast.LENGTH_SHORT).show();
                AppExecutors.getInstance().diskIO().execute(() -> {
                    mViewModel.updatefilm(film);
                    mViewModel.eleminarpuntuacion(score);
                    mViewModel.insertarpuntuacion(score);
                });

            } else {
                film.setPuntuacion(point);
                score.setPoints(point);
                Toast.makeText(getContext(), "Has puntuado esta pelicula con " + point, Toast.LENGTH_SHORT).show();
                tVvotosDetails.setText(String.valueOf(votos + 1));
                AppExecutors.getInstance().diskIO().execute(() -> {
                    mViewModel.updatefilm(film);
                    mViewModel.insertarpuntuacion(score);
                });

            }

        });
    }

    private void favouriteButton(ImageButton bFavorito) {
        bFavorito.setOnClickListener(v -> {

            Fav fav = new Fav(userName, film.getTitle(), film.getPosterPath());

            if (film.getFavoritos()) {
                film.setFavoritos(false);

                Toast.makeText(getContext(), "Has eleminado esta Pelicula de favoritos", Toast.LENGTH_SHORT).show();
                AppExecutors.getInstance().diskIO().execute(() -> {
                    mViewModel.updatefilm(film);
                    mViewModel.eleminarfavorito(fav);
                });

            } else {
                film.setFavoritos(true);

                Toast.makeText(getContext(), "Has añadido a favoritos esta Pelicula", Toast.LENGTH_SHORT).show();
                AppExecutors.getInstance().diskIO().execute(() -> {

                    mViewModel.updatefilm(film);
                    mViewModel.insertarfavorito(fav);
                });

            }
        });
    }

    private void wishButton(ImageButton bDeseadas) {
        bDeseadas.setOnClickListener(v -> {
            Wish wish = new Wish(userName, film.getTitle(), film.getPosterPath(), film.getVoteAverage(), film.getVoteCount(), film.getReleaseDate(), film.getOverview());

            if (film.getDeseadas()) {
                film.setDeseadas(false);

                Toast.makeText(getContext(), "Has eleminado esta Pelicula de Pendientes", Toast.LENGTH_SHORT).show();

                AppExecutors.getInstance().diskIO().execute(() -> {

                    mViewModel.updatefilm(film);
                    mViewModel.eleminardeseada(wish);
                });

            } else {
                film.setDeseadas(true);

                Toast.makeText(getContext(), "Has añadido a Pendientes esta Pelicula", Toast.LENGTH_SHORT).show();

                AppExecutors.getInstance().diskIO().execute(() -> {
                    mViewModel.updatefilm(film);
                    mViewModel.insertardeseada(wish);
                });

            }

        });
    }

    private void commentButton(ImageButton bComentar) {
        bComentar.setOnClickListener(v -> AppExecutors.getInstance().diskIO().execute(() -> {

            User user = mViewModel.getuser(settings.getLong("userID", 0));
            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {
                    DialogFragment dialogFragment = new WriteCommentFragment();
                    dialogFragment.setTargetFragment(DetailsFragment.this, COMMENT_DIALOG);

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("film", film);
                    bundle.putSerializable("user", user);

                    dialogFragment.setArguments(bundle);
                    dialogFragment.show(getParentFragmentManager(), "Comentar");
                }
            });

        }));
    }

    private void changeDateButton(ImageButton bCambiarFecha) {
        bCambiarFecha.setOnClickListener(v -> {

            Bundle bundle = new Bundle();
            bundle.putSerializable("film", film);
            if (film.getIdUser() == iduser) {

                Navigation.findNavController(v).navigate(R.id.confirmReleaseDateFragment, bundle);
            } else {
                AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "No eres el creador de esta pelicula", Toast.LENGTH_SHORT).show());
            }
        });
    }

    private void shareButton(ImageButton bCompartir) {
        bCompartir.setOnClickListener(v -> {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "¡Echa un vistazo a esta película! https://www.themoviedb.org/movie/" + film.getId());
            sendIntent.setType("text/plain");

            Intent shareIntent = Intent.createChooser(sendIntent, null);
            startActivity(shareIntent);

        });
    }

    private void deleteFilmButton(ImageButton bBorrarPeli, View v) {
        bBorrarPeli.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("¿Estás seguro de querer borrar la película?");

            builder.setPositiveButton("Yes", (dialogInterface, i) -> AppExecutors.getInstance().diskIO().execute(() -> {
                boolean deletting;
                if (film.getIdUser() == settings.getLong("userID", 0)) {
                    mViewModel.borrarpelicula(film.getId(), film.getTitle());
                    deletting = true;

                } else {
                    deletting = false;
                }

                AppExecutors.getInstance().mainThread().execute(() -> {
                    if (deletting) {
                        Toast.makeText(getContext(), "Has borrado la película", Toast.LENGTH_SHORT).show();
                        Navigation.findNavController(v).navigate(R.id.action_detailsFragment_to_nav_home);

                    } else
                        Toast.makeText(getContext(), "Esta película no la has creado tu", Toast.LENGTH_SHORT).show();

                });
            }));

            builder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.cancel());
            builder.create().show();

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        switch (requestCode) {
            case COMMENT_DIALOG:
                if (resultCode == Activity.RESULT_OK) {

                    AppExecutors.getInstance().mainThread().execute(() -> {

                        Comment comment = (Comment) data.getSerializableExtra("comment");
                        Log.i("com", "comentario añadido " + comment.toString());
                        listComments.add(comment);
                        adapterComments.swap(listComments);
                        recycler.setAdapter(adapterComments);
                    });

                } else if (resultCode == Activity.RESULT_CANCELED) {
                    Log.i("com", "comentario no añadido ");
                }
        }
    }


}