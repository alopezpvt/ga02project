package es.unex.giiis.asee.comunifilm.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.Score;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ScoreDAO {

    @Insert(onConflict = REPLACE)
    public long insert(Score score);

    @Query("SELECT * FROM SCORE WHERE id IN (:id)")
    public Score getScore(long id);

    @Query("SELECT * FROM SCORE")
    public List<Score> getAllScore();

    @Query("SELECT * FROM SCORE")
    LiveData<List<Score>> getAllScorelive();

    @Query("SELECT * FROM SCORE WHERE filmName IN (:filmName) ")
    public List<Score> getAllScorebyfilmname(String filmName);

    @Query("SELECT * FROM SCORE WHERE autorScore IN (:autorScore) ")
    public List<Score> getAllScorebyautor(String autorScore);

    @Query("SELECT * FROM SCORE WHERE autorScore IN (:autorScore) ")
    LiveData<List<Score>> getAllScorebyautorlive(String autorScore);

    @Query("SELECT * FROM SCORE WHERE autorScore IN (:autorScore) AND filmName IN (:title) ")
    public Score isScorebyautor(String autorScore, String title);

    @Query("SELECT * FROM SCORE WHERE autorScore IN (:autorScore) AND filmName IN (:title) ")
    LiveData<List<Score>> isScorebyautorlive(String autorScore, String title);

    @Query("DELETE FROM SCORE WHERE autorScore IN (:autorScore) AND filmName IN (:title)")
    abstract void deletebyquery(String autorScore, String title);

    @Query("DELETE FROM SCORE WHERE filmName IN (:title)")
    abstract void deletebyfilmname(String title);

    @Query("DELETE FROM SCORE WHERE autorScore IN (:autorScore) AND filmName IN (:title)")
    abstract void deletebyfilmnameanduser(String autorScore, String title);

    @Update
    public int update(Score Score);

}
