package es.unex.giiis.asee.comunifilm;

import android.content.Context;

import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.data.roomdb.ComufilmDatabaseServer;
import es.unex.giiis.asee.comunifilm.data.network.ApiDatabase;
import es.unex.giiis.asee.comunifilm.data.roomdb.ComufilmDatabaseUser;
import es.unex.giiis.asee.comunifilm.fragments.factory.AddPersonalListModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.AddUserTofollowModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.ConfirmReleaseDateViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.CreateFilmViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.DeleteFilmOfPersonalListViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.DetailsViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.ListAllFilmsFavViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.ListAllFilmsRecomendViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.ListAllFilmsScoreViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.ListAllUserScoreViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.ListAllWishViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.ListofListViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.LogginUserViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.PersonalListOfFilmsModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.RegisterUserViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.SearchUserViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.UnfollowUserViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.UserScoreViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.WriteCommentViewModelFactory;
import es.unex.giiis.asee.comunifilm.ui.pending.PendingViewModelFactory;
import es.unex.giiis.asee.comunifilm.ui.profile.ProfileViewModelFactory;
import es.unex.giiis.asee.comunifilm.fragments.factory.SearchFilmViewModelFactory;

public class AppContainer {

    private ComufilmDatabaseServer database;
    private ComufilmDatabaseUser databaseuser;
    private ApiDatabase networkDataSource;
    public RepositoryFilms repositoryFilms;
    public RepositoryUsers repositoryUsers;
    public SearchFilmViewModelFactory factorysearch;
    public DetailsViewModelFactory factorydetails;
    public ProfileViewModelFactory factoryprofile;
    public PendingViewModelFactory factorypending;
    public WriteCommentViewModelFactory factorywritecomment;
    public RegisterUserViewModelFactory factoryregisteruser;
    public LogginUserViewModelFactory factorylogginuser;
    public UserScoreViewModelFactory factoryuserscore;
    public UnfollowUserViewModelFactory factoryunfollowuser;
    public SearchUserViewModelFactory factorysearchuser;
    public ListAllUserScoreViewModelFactory factoryliastalluserscore;
    public ListAllWishViewModelFactory factoryliastallwish;
    public ListAllFilmsScoreViewModelFactory factoryliastallfilmsscore;
    public ListAllFilmsRecomendViewModelFactory factoryliastallfilmsrecomend;
    public ListAllFilmsFavViewModelFactory factoryliastallfilmsfav;
    public DeleteFilmOfPersonalListViewModelFactory factorydeletefilmofpersonal;
    public CreateFilmViewModelFactory factorycreatefilm;
    public ConfirmReleaseDateViewModelFactory factoryconfirmdate;
    public ListofListViewModelFactory factorylistoflist;
    public AddPersonalListModelFactory factoryaddpersonal;
    public AddUserTofollowModelFactory factoryadduserfollow;
    public PersonalListOfFilmsModelFactory factorypersonallist;


    public AppContainer(Context context) {
        database = ComufilmDatabaseServer.getInstance(context);
        databaseuser = ComufilmDatabaseUser.getInstance(context);
        networkDataSource = ApiDatabase.getInstance();
        repositoryFilms = RepositoryFilms.getInstance(database.getFilmDAO(), database.getCommentDAO(), databaseuser.getFavDAO(), database.getRecomendDAO(), databaseuser.getWishDAO(), database.getScoreDAO(), databaseuser.getListOfListDAO(), databaseuser.getListAndFilmsDAO(), networkDataSource);
        repositoryUsers = RepositoryUsers.getInstance(database.getUserDAO(), database.getUserScoreDAO(), database.getFollowerDAO());
        factorysearch = new SearchFilmViewModelFactory(repositoryFilms);
        factorydetails = new DetailsViewModelFactory(repositoryUsers, repositoryFilms);
        factoryprofile = new ProfileViewModelFactory(repositoryUsers, repositoryFilms);
        factorypending = new PendingViewModelFactory(repositoryFilms);
        factorywritecomment = new WriteCommentViewModelFactory(repositoryFilms);
        factoryregisteruser = new RegisterUserViewModelFactory(repositoryUsers);
        factorylogginuser = new LogginUserViewModelFactory(repositoryUsers);
        factoryuserscore = new UserScoreViewModelFactory(repositoryUsers);
        factoryunfollowuser = new UnfollowUserViewModelFactory(repositoryUsers);
        factorysearchuser = new SearchUserViewModelFactory(repositoryUsers);
        factoryliastalluserscore = new ListAllUserScoreViewModelFactory(repositoryUsers);
        factoryliastallwish = new ListAllWishViewModelFactory(repositoryFilms);
        factoryliastallfilmsscore = new ListAllFilmsScoreViewModelFactory(repositoryFilms);
        factoryliastallfilmsrecomend = new ListAllFilmsRecomendViewModelFactory(repositoryFilms);
        factoryliastallfilmsfav = new ListAllFilmsFavViewModelFactory(repositoryFilms);
        factorydeletefilmofpersonal = new DeleteFilmOfPersonalListViewModelFactory(repositoryFilms);
        factorycreatefilm = new CreateFilmViewModelFactory(repositoryFilms);
        factoryconfirmdate = new ConfirmReleaseDateViewModelFactory(repositoryFilms);
        factorylistoflist = new ListofListViewModelFactory(repositoryFilms);
        factoryaddpersonal = new AddPersonalListModelFactory(repositoryFilms);
        factoryadduserfollow = new AddUserTofollowModelFactory(repositoryUsers);
        factorypersonallist = new PersonalListOfFilmsModelFactory(repositoryFilms);
    }
}
