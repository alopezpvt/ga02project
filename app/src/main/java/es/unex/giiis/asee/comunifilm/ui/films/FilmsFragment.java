package es.unex.giiis.asee.comunifilm.ui.films;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.HideKeyboard;
import es.unex.giiis.asee.comunifilm.data.network.FilmsOwnLoaderRuneable;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterSearchFilms;
import es.unex.giiis.asee.comunifilm.fragments.DetailsFragment;
import es.unex.giiis.asee.comunifilm.data.models.Film;

import static android.content.Context.MODE_PRIVATE;


public class FilmsFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_my_films, container, false);
        HideKeyboard.hideKeyboard(getActivity());

        SharedPreferences settings = getContext().getSharedPreferences("user", MODE_PRIVATE);
        if (settings.getString("userName", "none").equals("none")) {

            AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Si no estas registrado tus acciones estan limitadas", Toast.LENGTH_SHORT).show());

        }

        RecyclerView recyclerView = v.findViewById(R.id.recyclerMyFilms);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        AdapterSearchFilms adapterSearchFilms = new AdapterSearchFilms(new ArrayList<Film>());
        recyclerView.setAdapter(adapterSearchFilms);

        AppExecutors.getInstance().networkIO().execute(new FilmsOwnLoaderRuneable(listFilms -> {
            adapterSearchFilms.swap(listFilms);

            adapterSearchFilms.setOnItemClickListener(pos -> {

                Log.i("Click: ", "clicking: " + listFilms.get(pos).getTitle());

                Bundle bundle = new Bundle();
                Film film = listFilms.get(pos);
                bundle.putSerializable("film", film);

                DetailsFragment detailsFragment = new DetailsFragment();

                detailsFragment.setArguments(bundle);
                Navigation.findNavController(v).navigate(R.id.action_nav_films_to_detailsFragment, bundle);
            });
            recyclerView.setAdapter(adapterSearchFilms);

        }, settings.getString("userName", "Ninguno"), getContext()));

        FloatingActionButton bAddFilm = v.findViewById(R.id.bAddFilm);

        bAddFilm.setOnClickListener(view -> Navigation.findNavController(v).navigate(R.id.action_nav_films_to_listMyFilmsFragment));

        return v;
    }
}