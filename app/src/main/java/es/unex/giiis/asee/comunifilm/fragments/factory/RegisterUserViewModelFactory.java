package es.unex.giiis.asee.comunifilm.fragments.factory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.RegisterUserFragmentViewModel;


public class RegisterUserViewModelFactory extends ViewModelProvider.NewInstanceFactory {


    private final RepositoryUsers mrepositoryUsers;

    public RegisterUserViewModelFactory(RepositoryUsers mrepositoryUsers) {

        this.mrepositoryUsers = mrepositoryUsers;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new RegisterUserFragmentViewModel(mrepositoryUsers);
    }
}
