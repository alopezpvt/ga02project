package es.unex.giiis.asee.comunifilm.fragments.viewmodels;


import androidx.lifecycle.ViewModel;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.ListAndFilms;


public class AddPersonalListViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;

    public AddPersonalListViewModel(RepositoryFilms mRepositoryFilms) {
        this.mRepositoryFilms = mRepositoryFilms;
    }

    public void insertarlistAndFilm(ListAndFilms listAndFilm) {
        mRepositoryFilms.insertarlistAndFilm(listAndFilm);
    }

}