package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.ViewModel;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;

public class UnfollowUserFragmentViewModel extends ViewModel {


    private final RepositoryUsers mRepositoryUsers;


    public UnfollowUserFragmentViewModel(RepositoryUsers mRepositoryUsers) {
        this.mRepositoryUsers = mRepositoryUsers;
    }

    public void eleminarfollower(long idFollower, long id) {
        mRepositoryUsers.eleminarfollower(idFollower, id);
    }
}