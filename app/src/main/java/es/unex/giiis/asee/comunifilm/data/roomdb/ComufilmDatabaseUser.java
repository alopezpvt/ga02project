package es.unex.giiis.asee.comunifilm.data.roomdb;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import es.unex.giiis.asee.comunifilm.data.models.Fav;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.ListAndFilms;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;
import es.unex.giiis.asee.comunifilm.data.models.Wish;

@Database(entities = {Fav.class, ListOfList.class, ListAndFilms.class, Film.class, Wish.class}, version = 1, exportSchema = false)

public abstract class ComufilmDatabaseUser extends RoomDatabase {
    private static ComufilmDatabaseUser instance;

    public synchronized static ComufilmDatabaseUser getInstance(Context context) {
        if (instance == null)
            instance = Room.databaseBuilder(context, ComufilmDatabaseUser.class, "ComunifilmUser.db").build();
        return instance;
    }

    public abstract FavDAO getFavDAO();

    public abstract ListOfListDAO getListOfListDAO();

    public abstract ListAndFilmsDAO getListAndFilmsDAO();

    public abstract FilmDAO getFilmDAO();

    public abstract WishDAO getWishDAO();


}
