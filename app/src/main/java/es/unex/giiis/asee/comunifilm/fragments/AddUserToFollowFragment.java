package es.unex.giiis.asee.comunifilm.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Follower;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.AddUserToFollowViewModel;

import static android.content.Context.MODE_PRIVATE;


public class AddUserToFollowFragment extends Fragment {


    private User userToFollow;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            userToFollow = (User) getArguments().getSerializable("user");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_user_to_follow, container, false);

        TextView tvUserName = v.findViewById(R.id.tvUserName);
        tvUserName.setText(userToFollow.getName());
        Button bNoAddUser = v.findViewById(R.id.bNoAddUser);
        Button bAddUser = v.findViewById(R.id.bAddUser);
        Bundle bundle = new Bundle();


        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        AddUserToFollowViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryadduserfollow).get(AddUserToFollowViewModel.class);


        bAddUser.setOnClickListener(v1 -> AppExecutors.getInstance().networkIO().execute(() -> {

            SharedPreferences settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
            long idFollower = settings.getLong("userID", -1);
            String nameFollower = settings.getString("userName", "null");

            Follower follower = new Follower(idFollower, userToFollow.getId(), nameFollower, userToFollow.getName());
            mViewModel.insertarfollower(follower);


            AppExecutors.getInstance().mainThread().execute(() -> {
                Navigation.findNavController(v1).navigate(R.id.action_addUserToFollowFragment_to_nav_friends);
                Toast.makeText(getContext(), "Siguiendo usuario!", Toast.LENGTH_SHORT).show();

            });
        }));


        bNoAddUser.setOnClickListener(v12 -> {
            bundle.putString("CommentOrRecommend", "searchUser");
            AppExecutors.getInstance().mainThread().execute(() -> Navigation.findNavController(v12).navigate(R.id.action_addUserToFollowFragment_to_searchUserFragment, bundle));
        });


        return v;
    }
}