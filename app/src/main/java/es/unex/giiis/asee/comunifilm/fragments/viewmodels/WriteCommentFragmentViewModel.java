package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.ViewModel;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.Comment;


public class WriteCommentFragmentViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;

    public WriteCommentFragmentViewModel(RepositoryFilms mRepositoryFilms) {

        this.mRepositoryFilms = mRepositoryFilms;
    }

    public void insertarcomentario(Comment comment) {
        mRepositoryFilms.insertarcomentario(comment);
    }


}