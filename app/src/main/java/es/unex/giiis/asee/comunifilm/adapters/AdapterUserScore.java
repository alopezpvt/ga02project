package es.unex.giiis.asee.comunifilm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.UserScore;

public class AdapterUserScore extends RecyclerView.Adapter<AdapterUserScore.ViewHolderUserScore> {
    ArrayList<UserScore> listsucore;
    Context context;

    public AdapterUserScore(ArrayList<UserScore> listsucore) {
        this.listsucore = listsucore;
    }

    @NonNull
    @Override
    public AdapterUserScore.ViewHolderUserScore onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.score_user_list, null, false);
        context = parent.getContext();
        return new AdapterUserScore.ViewHolderUserScore(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterUserScore.ViewHolderUserScore holder, int position) {
        holder.setUserScore(listsucore.get(position));
    }

    @Override
    public int getItemCount() {

        return listsucore.size();
    }

    public class ViewHolderUserScore extends RecyclerView.ViewHolder {

        TextView userscored;
        TextView points;
        TextView username;

        public ViewHolderUserScore(@NonNull View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.tVUserScore2);
            userscored = itemView.findViewById(R.id.tVUserPunctScore2);
            points = itemView.findViewById(R.id.tVPointsScore2);
        }

        public void setUserScore(UserScore userScore) {
            username.setText(userScore.getAutorScore());
            userscored.setText(userScore.getUserScored());
            points.setText(Integer.toString(userScore.getPoints()));

        }
    }

}
