package es.unex.giiis.asee.comunifilm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Comment;


public class AdapterComments extends RecyclerView.Adapter<AdapterComments.ViewHolderComments> {
    ArrayList<Comment> listComments;
    Context context;

    public AdapterComments(ArrayList<Comment> listComments) {
        this.listComments = listComments;
    }

    @NonNull
    @Override
    public ViewHolderComments onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_list_item, null, false);
        context = parent.getContext();
        return new ViewHolderComments(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderComments holder, int position) {
        holder.setComments(listComments.get(position));
    }

    public void swap(ArrayList<Comment> listComments) {
        this.listComments = listComments;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listComments.size();
    }

    public class ViewHolderComments extends RecyclerView.ViewHolder {

        ImageView posterFilm;
        TextView title;
        TextView commentText;

        public ViewHolderComments(@NonNull View itemView) {
            super(itemView);
            commentText = itemView.findViewById(R.id.tVComment);
            title = itemView.findViewById(R.id.tVTitleFilm);
            posterFilm = itemView.findViewById(R.id.imgFilm);
        }

        public void setComments(Comment comment) {

            commentText.setText(comment.getText());
            title.setText(comment.getAutorComment());

            if (comment.getPosterPath() != null)
                Glide.with(context).load(comment.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(posterFilm);

        }
    }
}
