package es.unex.giiis.asee.comunifilm.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterScore;
import es.unex.giiis.asee.comunifilm.data.models.Score;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.ListAllFilmsScoreFragmentViewModel;


public class ListAllFilmsScoreFragment extends Fragment {


    private ArrayList<Score> listScore;
    private RecyclerView recycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list_all_films_score, container, false);

        recycler = v.findViewById(R.id.recyclerScore);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        listScore = new ArrayList<Score>();

        AdapterScore adapterScore = new AdapterScore(listScore);
        recycler.setAdapter(adapterScore);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        ListAllFilmsScoreFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryliastallfilmsscore).get(ListAllFilmsScoreFragmentViewModel.class);

        mViewModel.getallfilmsscore().observe(getActivity(), listapuntuaciones -> {
            Log.i("Listapuntuacion: ", "numero: " + listapuntuaciones.size());
            AdapterScore adapterScore1 = new AdapterScore((ArrayList<Score>) listapuntuaciones);
            recycler.setAdapter(adapterScore1);

        });

        return v;
    }
}

