package es.unex.giiis.asee.comunifilm.data;

import android.util.Log;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.data.models.Comment;
import es.unex.giiis.asee.comunifilm.data.models.Fav;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.ListAndFilms;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;
import es.unex.giiis.asee.comunifilm.data.models.Recomend;
import es.unex.giiis.asee.comunifilm.data.models.Score;
import es.unex.giiis.asee.comunifilm.data.models.Wish;
import es.unex.giiis.asee.comunifilm.data.network.ApiDatabase;
import es.unex.giiis.asee.comunifilm.data.roomdb.CommentDAO;
import es.unex.giiis.asee.comunifilm.data.roomdb.FavDAO;
import es.unex.giiis.asee.comunifilm.data.roomdb.FilmDAO;
import es.unex.giiis.asee.comunifilm.data.roomdb.ListAndFilmsDAO;
import es.unex.giiis.asee.comunifilm.data.roomdb.ListOfListDAO;
import es.unex.giiis.asee.comunifilm.data.roomdb.RecomendDAO;
import es.unex.giiis.asee.comunifilm.data.roomdb.ScoreDAO;
import es.unex.giiis.asee.comunifilm.data.roomdb.WishDAO;

public class RepositoryFilms {
    private static final String LOG_TAG = RepositoryFilms.class.getSimpleName();

    // For Singleton instantiation
    private static RepositoryFilms sInstance;
    private final CommentDAO mCommentDAO;
    private LiveData<List<Comment>> mAllComment;
    private final FavDAO mFavDAO;
    private LiveData<List<Fav>> mAllFav;
    private final RecomendDAO mRecomendDAO;
    private MutableLiveData<List<Recomend>> mAllRecomend = new MutableLiveData<List<Recomend>>();
    private final ScoreDAO mScoreDAO;
    private MutableLiveData<List<Score>> mAllScore = new MutableLiveData<List<Score>>();
    private final WishDAO mWishDAO;
    private LiveData<List<Wish>> mAllWish;
    private final FilmDAO mFilmDAO;
    private final ListOfListDAO mListOfListDAO;
    private MutableLiveData<List<ListOfList>> mAllListOfList = new MutableLiveData<List<ListOfList>>();
    private final ListAndFilmsDAO mListAndFilmsDAO;
    private LiveData<List<ListAndFilms>> mListAndFilmsFav;

    private final ApiDatabase mApiDatabase;
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final MutableLiveData<String> titleFilterLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> userFilterLiveData = new MutableLiveData<>();
    private final MutableLiveData<Long> idFilterLiveData = new MutableLiveData<>();
    private final MutableLiveData<String> titlefilmFilterLiveData = new MutableLiveData<>();
    private final MutableLiveData<Long> idlista = new MutableLiveData<>();
    private final Map<String, Long> lastUpdateTimeMillisMap = new HashMap<>();
    private static final long MIN_TIME_FROM_LAST_FETCH_MILLIS = 30000;


    private RepositoryFilms(CommentDAO mCommentDAO, FavDAO mFavDAO, RecomendDAO mRecomendDAO, ScoreDAO mScoreDAO, WishDAO mWishDAO
            , FilmDAO filmdao, ListOfListDAO mListOfListDAO, ListAndFilmsDAO mListAndFilmsDAO, ApiDatabase apidatabase) {
        this.mCommentDAO = mCommentDAO;
        mAllComment = mCommentDAO.getAllCommentslive();
        this.mFavDAO = mFavDAO;
        mAllFav = mFavDAO.getAllFavlive();
        this.mRecomendDAO = mRecomendDAO;
        this.mScoreDAO = mScoreDAO;
        this.mWishDAO = mWishDAO;
        mAllWish = mWishDAO.getAllWishlive();
        this.mListOfListDAO = mListOfListDAO;
        this.mListAndFilmsDAO = mListAndFilmsDAO;
        mListAndFilmsFav = mListAndFilmsDAO.getListAndFilmslive();
        this.mFilmDAO = filmdao;

        mApiDatabase = apidatabase;
        // LiveData that fetches repos from network
        LiveData<Film[]> networkData = mApiDatabase.getCurrentBusqueda();
        titleFilterLiveData.setValue("popular");

        networkData.observeForever(new Observer<Film[]>() {
            @Override
            public void onChanged(Film[] newReposFromNetwork) {
                mExecutors.diskIO().execute(() -> {
                    // Insert our new repos into local database
                    mFilmDAO.bulkInsert(Arrays.asList(newReposFromNetwork));
                    Log.d(LOG_TAG, "New values inserted in Room");

                    mFilmDAO.genero(10752,"%u%");
                    mFilmDAO.genero(878,"%o%");
                    mFilmDAO.genero(36,"%i%");
                    mFilmDAO.genero(10751,"%e%");
                    mFilmDAO.genero(28,"%a%");
                });
            }
        });

    }

    public synchronized static RepositoryFilms getInstance(FilmDAO Filmdao, CommentDAO mCommentDAO, FavDAO mFavDAO
            , RecomendDAO mRecomendDAO, WishDAO mWishDAO, ScoreDAO mScoreDAO, ListOfListDAO mListOfListDAO, ListAndFilmsDAO mListAndFilmsDAO, ApiDatabase nds) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new RepositoryFilms(mCommentDAO, mFavDAO, mRecomendDAO, mScoreDAO, mWishDAO, Filmdao, mListOfListDAO, mListAndFilmsDAO, nds);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void settitle(final String title) {
        titleFilterLiveData.setValue("%" + title + "%");
        AppExecutors.getInstance().diskIO().execute(() -> {
            if (isFetchNeeded(title)) {
                doFetchbusqueda(title);
            }
        });
    }

    public void reinicio() {
        AppExecutors.getInstance().mainThread().execute(() -> {
            titleFilterLiveData.setValue("popular");
            userFilterLiveData.setValue("");
            idFilterLiveData.setValue((long) 0);
            idlista.setValue((long) 0);
            titlefilmFilterLiveData.setValue("");
        });
    }

    public void settitlefilm(final String titlefilm) {
        AppExecutors.getInstance().mainThread().execute(() -> {
            titlefilmFilterLiveData.setValue(titlefilm);
        });
    }

    public void setuser(final String user) {
        AppExecutors.getInstance().mainThread().execute(() -> {
            userFilterLiveData.setValue(user);
        });
    }

    public void setidfilm(final long id) {
        AppExecutors.getInstance().mainThread().execute(() -> {
            idFilterLiveData.setValue(id);
        });
    }

    public void setidlista(final long id) {
        AppExecutors.getInstance().mainThread().execute(() -> {
            idlista.setValue(id);
        });
    }

    public void doFetchbusqueda(String title) {
        Log.d(LOG_TAG, "Fetching Repos from Github");
        AppExecutors.getInstance().diskIO().execute(() -> {

            mApiDatabase.fetchbusqueda(title, userFilterLiveData.getValue());
            lastUpdateTimeMillisMap.put(title, System.currentTimeMillis());
        });
    }

    public void doFetchpopular() {
        Log.d(LOG_TAG, "Fetching Repos from Github");
        AppExecutors.getInstance().diskIO().execute(() -> {
            mApiDatabase.fetchpopulares(userFilterLiveData.getValue());
        });
    }

    /**
     * Database related operations
     **/

    public LiveData<List<Film>> getCurrentBusqueda() {

        return Transformations.switchMap(titleFilterLiveData, title -> {
            if (!title.equals("popular")) {
                return mFilmDAO.getReposBytitle(title);
            } else {
                return mFilmDAO.getAllPopularsLive(title);
            }
        });
    }

    public LiveData<List<ListAndFilms>> getCurrentList() {
        return Transformations.switchMap(idlista, title -> mListAndFilmsDAO.getAllFilmsPerListlive(title));
    }


    public LiveData<List<Score>> getCurrentpuntuacionusuario() {

        return Transformations.switchMap(userFilterLiveData,
                user -> Transformations.switchMap(titlefilmFilterLiveData,
                        title -> mScoreDAO.isScorebyautorlive(user, title)));
    }

    public LiveData<List<Comment>> getCurrentcomentarios() {
        return Transformations.switchMap(idFilterLiveData,
                id -> mCommentDAO.getCommentsByFilmIDlive(id));
    }


    public LiveData<List<Recomend>> getCurrentrecomendados() {

        return Transformations.switchMap(userFilterLiveData,
                user -> Transformations.switchMap(titlefilmFilterLiveData,
                        new Function<String, LiveData<List<Recomend>>>() {
                            @Override
                            public LiveData<List<Recomend>> apply(String title) {
                                return mRecomendDAO.isRecomendbyautorlive(user, title);
                            }
                        }));
    }

    public LiveData<List<Fav>> getCurrentfavoritos() {

        return Transformations.switchMap(userFilterLiveData,
                user -> mFavDAO.getAllFavbyautor(user));
    }

    public LiveData<List<Wish>> getCurrentdeseadas() {

        return Transformations.switchMap(userFilterLiveData,
                user -> mWishDAO.getAllWishbyautorlive(user));

    }

    public LiveData<List<Score>> getallfilmsscore() {

        AppExecutors.getInstance().diskIO().execute(() -> {
            ArrayList<Score> listapuntuaciones = (ArrayList<Score>) mScoreDAO.getAllScore();
            mAllScore.postValue(listapuntuaciones);
        });
        return mAllScore;
    }

    public LiveData<List<Recomend>> getallfilmsrecomend() {
        AppExecutors.getInstance().diskIO().execute(() -> {
            ArrayList<Recomend> listarecomendadas = (ArrayList<Recomend>) mRecomendDAO.getAllRecomend();
            mAllRecomend.postValue(listarecomendadas);
        });
        return mAllRecomend;
    }

    public LiveData<List<ListOfList>> getListOfListAllfilter() {
        return Transformations.switchMap(userFilterLiveData,
                user -> mListOfListDAO.getListOfListBynameuser(user));
    }

    public void insertarrecomendada(Recomend recomend) {
        mRecomendDAO.insert(recomend);
    }

    public void eleminarrecomendada(Recomend recomend) {
        mRecomendDAO.deletebyfilmname(recomend.getFilmName());
    }

    public void insertarfavorito(Fav fav) {
        mFavDAO.insert(fav);
    }

    public void eleminarfavorito(Fav fav) {
        mFavDAO.deletebyquery(fav.getAutorFav(), fav.getFilmName());
    }

    public void insertardeseada(Wish wish) {
        mWishDAO.insert(wish);
    }

    public void eleminardeseada(Wish wish) {
        mWishDAO.deletebyquery(wish.getAutorWish(), wish.getFilmName());
    }

    public void insertarpuntuacion(Score puntuacion) {
        mScoreDAO.insert(puntuacion);
    }

    public void eleminarpuntuacion(Score puntuacion) {
        mScoreDAO.deletebyfilmnameanduser(puntuacion.getAutorScore(), puntuacion.getFilmName());
    }

    public void insertarcomentario(Comment comment) {
        mCommentDAO.insert(comment);
    }

    public void insertarfilm(Film film) {
        mFilmDAO.insert(film);
    }

    public void updatefilm(Film film) {
        mFilmDAO.update(film);
    }

    public Film getfilm(long filmid) {
        return mFilmDAO.getFilm(filmid);
    }


    public void insertarListOfList(ListOfList listOfList) {
        mListOfListDAO.insert(listOfList);
    }

    public void insertarlistAndFilm(ListAndFilms listAndFilm) {
        mListAndFilmsDAO.insert(listAndFilm);
    }


    public void deletefilmofpersonallist(long idList, long id) {
        mListAndFilmsDAO.deleteFilmOfList(idList, id);
    }


    public void eleminarpelicula(long idfilm, String titulo) {
        mFilmDAO.deleteFilm((int) idfilm);
        mScoreDAO.deletebyfilmname(titulo);
        mCommentDAO.deletebyfilmname(titulo);
        mRecomendDAO.deletebyfilmname(titulo);
        mWishDAO.deletebyfilmtitle(titulo);
        mFavDAO.deletebyfilmname(titulo);
        mListAndFilmsDAO.deleteFilmOfListbyidfilm(idfilm);

    }

    /**
     * Checks if we have to update the repos data.
     *
     * @return Whether a fetch is needed
     */
    private boolean isFetchNeeded(String title) {
        Long lastFetchTimeMillis = lastUpdateTimeMillisMap.get(title);
        lastFetchTimeMillis = lastFetchTimeMillis == null ? 0L : lastFetchTimeMillis;
        long timeFromLastFetch = System.currentTimeMillis() - lastFetchTimeMillis;
        return timeFromLastFetch > MIN_TIME_FROM_LAST_FETCH_MILLIS || mFilmDAO.getNumberFilmbytitle(title) == 0;
    }


}