package es.unex.giiis.asee.comunifilm.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MainActivity;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.LogginUserFragmentViewModel;


import static android.content.Context.MODE_PRIVATE;

public class LogginUserFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_loggin_user, container, false);
        EditText eTLogginUserName = v.findViewById(R.id.eTLogginUserName);
        EditText eTLogginPassword = v.findViewById(R.id.eTLogginPassword);
        Button bJoinSession = v.findViewById(R.id.bJoinSession);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        LogginUserFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factorylogginuser).get(LogginUserFragmentViewModel.class);

        bJoinSession.setOnClickListener(v1 -> {
            String userName = eTLogginUserName.getText().toString();
            String password = eTLogginPassword.getText().toString();

            if (userName != null && !userName.trim().isEmpty() && password != null && !password.trim().isEmpty()) {

                AppExecutors.getInstance().diskIO().execute(() -> {

                    Boolean existe = false;
                    User user = null;
                    List<User> todos = mViewModel.getallusers().getValue();
                    for (User usuariobd : todos) {
                        if (usuariobd.getName().equals(userName)) {
                            existe = true;
                            user = usuariobd;
                        }
                    }

                    if (existe == false) {

                        AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "User not exist", Toast.LENGTH_SHORT).show());

                    } else if (user.getPassword().equals(password)) {

                        mViewModel.setuser(user.getName());
                        SharedPreferences settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();

                        editor.putLong("userID", user.getId());
                        editor.putString("userName", user.getName());
                        editor.putString("password", user.getPassword());
                        editor.putString("email", user.getEmail());
                        editor.commit();

                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                        getActivity().finish();

                    } else {

                        AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "User or password are incorrect", Toast.LENGTH_SHORT).show());
                    }
                });
            } else {

                AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Please, complete all fields", Toast.LENGTH_SHORT).show());
            }
        });

        return v;
    }
}