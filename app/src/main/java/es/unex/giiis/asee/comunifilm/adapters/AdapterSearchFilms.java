package es.unex.giiis.asee.comunifilm.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Film;


public class AdapterSearchFilms extends RecyclerView.Adapter<AdapterSearchFilms.ViewHolderSearchFilms> {

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    private ArrayList<Film> listFilms;
    private Context context;
    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public AdapterSearchFilms(ArrayList<Film> listFilms) {
        this.listFilms = listFilms;
    }

    @NonNull
    @Override
    public ViewHolderSearchFilms onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_film_list, parent, false);
        context = parent.getContext();
        return new ViewHolderSearchFilms(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderSearchFilms holder, int position) {
        holder.setFilms(listFilms.get(position), context);

    }

    @Override
    public int getItemCount() {
        return listFilms.size();
    }

    public ArrayList<Film> getListFilms() {
        return listFilms;
    }

    public void swap(ArrayList<Film> list) {
        listFilms = list;
        notifyDataSetChanged();
    }

    public class ViewHolderSearchFilms extends RecyclerView.ViewHolder {

        ImageView imgPosterFilm;
        TextView tVtitle;

        public ViewHolderSearchFilms(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);
            imgPosterFilm = itemView.findViewById(R.id.imgPosterFilm);
            tVtitle = itemView.findViewById(R.id.tVTitle);
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        listener.onItemClick(pos);
                    }
                }
            });
        }

        public void setFilms(Film film, Context context) {
            tVtitle.setText(film.getTitle());
            Log.i("adapter: ", "contexto" + context.toString());

            if (film.getPosterPath() != null)
                Glide.with(context).load(film.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(imgPosterFilm);

        }
    }

}
