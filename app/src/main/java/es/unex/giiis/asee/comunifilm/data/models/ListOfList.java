package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity(tableName = "LISTOFLIST")
public class ListOfList implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private long idList;
    private String nameList;
    private String nameuser;

    public ListOfList(long idList, String nameList, String nameuser) {
        this.idList = idList;
        this.nameList = nameList;
        this.nameuser = nameuser;
    }

    @Ignore
    public ListOfList(String nameList, String nameuser) {
        this.nameList = nameList;
        this.nameuser = nameuser;
    }

    public void setIdList(long idList) {
        this.idList = idList;
    }


    public void setNameList(String nameList) {
        this.nameList = nameList;
    }

    public long getIdList() {
        return idList;
    }

    public String getNameList() {
        return nameList;
    }

    public String getNameuser() {
        return nameuser;
    }

    public void setNameuser(String nameuser) {
        this.nameuser = nameuser;
    }
}
