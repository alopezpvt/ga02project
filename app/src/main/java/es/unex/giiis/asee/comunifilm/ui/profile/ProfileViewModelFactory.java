package es.unex.giiis.asee.comunifilm.ui.profile;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;

public class ProfileViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final RepositoryUsers mRepositoryUsers;
    private final RepositoryFilms mrepositoryFilms;

    public ProfileViewModelFactory(RepositoryUsers mRepositoryUsers, RepositoryFilms mrepositoryFilms) {
        this.mRepositoryUsers = mRepositoryUsers;
        this.mrepositoryFilms = mrepositoryFilms;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ProfileFragmentViewModel(mRepositoryUsers, mrepositoryFilms);
    }
}
