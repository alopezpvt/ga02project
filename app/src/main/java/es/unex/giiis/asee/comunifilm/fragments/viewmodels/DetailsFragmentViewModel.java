package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.data.models.Comment;
import es.unex.giiis.asee.comunifilm.data.models.Fav;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.Recomend;
import es.unex.giiis.asee.comunifilm.data.models.Score;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.data.models.Wish;

public class DetailsFragmentViewModel extends ViewModel {

    private final RepositoryUsers mRepositoryUsers;
    private final RepositoryFilms mRepositoryFilms;
    private final LiveData<List<Comment>> mAllComment;
    private final LiveData<List<Fav>> mAllFav;
    private final LiveData<List<Recomend>> mAllRecomend;
    private final LiveData<List<Score>> mAllScore;
    private final LiveData<List<Wish>> mAllWish;


    public DetailsFragmentViewModel(RepositoryUsers mRepositoryUsers, RepositoryFilms mRepositoryFilms) {
        this.mRepositoryUsers = mRepositoryUsers;
        this.mRepositoryFilms = mRepositoryFilms;
        mAllComment = mRepositoryFilms.getCurrentcomentarios();
        mAllFav = mRepositoryFilms.getCurrentfavoritos();
        mAllRecomend = mRepositoryFilms.getCurrentrecomendados();
        mAllScore = mRepositoryFilms.getCurrentpuntuacionusuario();
        mAllWish = mRepositoryFilms.getCurrentdeseadas();

    }

    public void settitlefilm(String titlefilm) {

        mRepositoryFilms.settitlefilm(titlefilm);
    }

    public void setidfilm(long id) {

        mRepositoryFilms.setidfilm(id);
    }


    public void insertarrecomendada(Recomend recomend) {
        mRepositoryFilms.insertarrecomendada(recomend);
    }

    public void eleminarrecomendada(Recomend recomend) {
        mRepositoryFilms.eleminarrecomendada(recomend);
    }

    public void insertarfavorito(Fav favorito) {
        mRepositoryFilms.insertarfavorito(favorito);
    }

    public void eleminarfavorito(Fav favorito) {
        mRepositoryFilms.eleminarfavorito(favorito);
    }

    public void insertardeseada(Wish wish) {
        mRepositoryFilms.insertardeseada(wish);
    }

    public void eleminardeseada(Wish wish) {
        mRepositoryFilms.eleminardeseada(wish);
    }

    public void insertarpuntuacion(Score puntuacion) {
        mRepositoryFilms.insertarpuntuacion(puntuacion);
    }

    public void eleminarpuntuacion(Score puntuacion) {
        mRepositoryFilms.eleminarpuntuacion(puntuacion);
    }

    public void updatefilm(Film film) {
        mRepositoryFilms.updatefilm(film);
    }

    public User getuser(long userid) {
        return mRepositoryUsers.getusuario(userid);
    }

    public void borrarpelicula(long idfilm, String titulo) {
        mRepositoryFilms.eleminarpelicula(idfilm, titulo);
    }

    public LiveData<List<Score>> getpuntuacionusuario() {
        return mAllScore;
    }

    public LiveData<List<Comment>> getcomentarios() {
        return mAllComment;
    }

    public LiveData<List<Recomend>> getrecomendadas() {
        return mAllRecomend;
    }

    public LiveData<List<Fav>> getfavoritos() {
        return mAllFav;
    }

    public LiveData<List<Wish>> getdeseadas() {
        return mAllWish;
    }

}