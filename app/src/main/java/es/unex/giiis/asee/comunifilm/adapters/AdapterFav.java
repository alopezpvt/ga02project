package es.unex.giiis.asee.comunifilm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Fav;


public class AdapterFav extends RecyclerView.Adapter<AdapterFav.ViewHolderFav> {

    ArrayList<Fav> listFav;
    Context context;

    public AdapterFav(ArrayList<Fav> listFav) {
        this.listFav = listFav;
    }

    @NonNull
    @Override
    public ViewHolderFav onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fav_list, null, false);
        context = parent.getContext();
        return new ViewHolderFav(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderFav holder, int position) {
        holder.setFav(listFav.get(position));
    }

    @Override
    public int getItemCount() {
        return listFav.size();
    }

    public class ViewHolderFav extends RecyclerView.ViewHolder {

        TextView title;
        ImageView posterFilm;


        public ViewHolderFav(@NonNull View itemView) {
            super(itemView);


            title = itemView.findViewById(R.id.tVTitleFav);
            posterFilm = itemView.findViewById(R.id.imgPosterFilmFav);

        }

        public void setFav(Fav fav) {


            title.setText(fav.getFilmName());

            if (fav.getPosterPath() != null)
                Glide.with(context).load(fav.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(posterFilm);


        }
    }
}
