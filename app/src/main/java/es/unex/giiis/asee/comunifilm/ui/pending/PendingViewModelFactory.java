package es.unex.giiis.asee.comunifilm.ui.pending;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;

public class PendingViewModelFactory extends ViewModelProvider.NewInstanceFactory {


    private final RepositoryFilms mrepositoryFilms;

    public PendingViewModelFactory(RepositoryFilms mrepositoryFilms) {
        this.mrepositoryFilms = mrepositoryFilms;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new PendingViewModel(mrepositoryFilms);
    }
}
