package es.unex.giiis.asee.comunifilm.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.UnfollowUserFragmentViewModel;

import static android.content.Context.MODE_PRIVATE;


public class UnFollowUserFragment extends Fragment {

    private User user;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            user = (User) getArguments().getSerializable("user");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_un_follow_user, container, false);

        TextView tvUserToUnfollow = v.findViewById(R.id.tvUserToUnfollow);
        tvUserToUnfollow.setText(user.getName());

        Button bSiUnfollow = v.findViewById(R.id.bSiUnfollow);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        UnfollowUserFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryunfollowuser).get(UnfollowUserFragmentViewModel.class);

        bSiUnfollow.setOnClickListener(v1 -> AppExecutors.getInstance().networkIO().execute(() -> {
            SharedPreferences settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
            long idFollower = settings.getLong("userID", -1);

            mViewModel.eleminarfollower(idFollower, user.getId());
            AppExecutors.getInstance().mainThread().execute(() -> {
                Bundle bundle = new Bundle();
                bundle.putString("CommentOrRecommend", "listUserFollowing");
                Navigation.findNavController(v1).navigate(R.id.action_unFollowUserFragment_to_searchUserFragment, bundle);
                Toast.makeText(getContext(), "Se ha dejado de seguir al usuario.", Toast.LENGTH_SHORT).show();
            });

        }));

        Button bNoUnfollow = v.findViewById(R.id.bNoUnfollow);

        bNoUnfollow.setOnClickListener(v12 -> AppExecutors.getInstance().mainThread().execute(() -> {
            Bundle bundle = new Bundle();
            bundle.putString("CommentOrRecommend", "listUserFollowing");
            Navigation.findNavController(v12).navigate(R.id.action_unFollowUserFragment_to_searchUserFragment, bundle);
        }));

        return v;
    }
}