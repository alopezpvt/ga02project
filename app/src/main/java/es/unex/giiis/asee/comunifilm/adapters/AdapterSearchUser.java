package es.unex.giiis.asee.comunifilm.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.User;

public class AdapterSearchUser extends RecyclerView.Adapter<AdapterSearchUser.ViewHolderSearchUser> {

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    private ArrayList<User> listUser;
    private Context context;
    private AdapterSearchUser.OnItemClickListener listener;

    public void setOnItemClickListener(AdapterSearchUser.OnItemClickListener listener) {
        this.listener = listener;
    }

    public AdapterSearchUser(ArrayList<User> listUser) {
        this.listUser = listUser;
    }

    @NonNull
    @Override
    public AdapterSearchUser.ViewHolderSearchUser onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_user_list, parent, false);
        context = parent.getContext();
        return new AdapterSearchUser.ViewHolderSearchUser(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterSearchUser.ViewHolderSearchUser holder, int position) {
        holder.setUser(listUser.get(position), context);
    }

    @Override
    public int getItemCount() {
        return listUser.size();
    }

    public void swap(ArrayList<User> list) {
        this.listUser = list;
        notifyDataSetChanged();
    }

    public class ViewHolderSearchUser extends RecyclerView.ViewHolder {

        TextView tVuser;

        public ViewHolderSearchUser(@NonNull View itemView, AdapterSearchUser.OnItemClickListener listener) {
            super(itemView);
            tVuser = itemView.findViewById(R.id.tVUser);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        int pos = getAdapterPosition();
                        if (pos != RecyclerView.NO_POSITION) {
                            listener.onItemClick(pos);
                        }
                    }
                }
            });
        }

        public void setUser(User user, Context context) {
            tVuser.setText(user.getName());
            Log.i("adapter: ", "contexto" + context.toString());
        }
    }

}
