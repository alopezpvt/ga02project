package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import java.io.Serializable;

@Entity(tableName = "USERS")
public class User implements Serializable {


    @PrimaryKey(autoGenerate = true)
    private long id;
    @ColumnInfo(name = "userName")

    private String name;
    private String password;
    private String email;
    private boolean votado;

    public User(long id, String name, String password, String email) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.email = email;
        this.votado = false;
    }

    @Ignore
    public User() {

    }


    @Ignore
    public User(String name, String password, String email) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.votado = false;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isVotado() {
        return votado;
    }

    public void setVotado(boolean votado) {
        this.votado = votado;
    }
}

