package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "RECOMEND")
public class Recomend {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String autorRecomend;
    private String filmName;
    private String posterPath;


    public Recomend(long id, String autorRecomend, String filmName, String posterPath) {
        this.id = id;

        this.autorRecomend = autorRecomend;
        this.filmName = filmName;
        this.posterPath = posterPath;
    }

    @Ignore
    public Recomend(String autorRecomend, String filmName, String posterPath) {
        this.id = id;
        this.autorRecomend = autorRecomend;
        this.filmName = filmName;
        this.posterPath = posterPath;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAutorRecomend() {
        return autorRecomend;
    }

    public void setAutorRecomend(String autorRecomend) {
        this.autorRecomend = autorRecomend;
    }


    @Override
    public String toString() {
        return "Recomend{" +
                "id=" + id +
                ", autorRecomend='" + autorRecomend + '\'' +
                ", filmName='" + filmName + '\'' +
                ", posterPath='" + posterPath + '\'' +
                '}';
    }
}
