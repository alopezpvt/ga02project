package es.unex.giiis.asee.comunifilm.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterUserScore;
import es.unex.giiis.asee.comunifilm.data.models.UserScore;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.ListAllUserScoreFragmentViewModel;

public class ListAllUserScoreFragment extends Fragment{

    private ArrayList<UserScore> listUScore;
    private RecyclerView recycler;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_list_all_user_score, container, false);

        recycler = v.findViewById(R.id.recyclerRecomend);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        listUScore = new ArrayList<UserScore>();

        AdapterUserScore adapterUScore = new AdapterUserScore(listUScore);
        recycler.setAdapter(adapterUScore);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        ListAllUserScoreFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryliastalluserscore).get(ListAllUserScoreFragmentViewModel.class);

                mViewModel.getalluserscore().observe(getActivity(), busqueda -> {
                    Log.i("Listausuario: ", "numero: " + busqueda.size());
                    AdapterUserScore adapterUScore1 = new AdapterUserScore((ArrayList<UserScore>) busqueda);
                    recycler.setAdapter(adapterUScore1);

                });

        return v;
    }
}
