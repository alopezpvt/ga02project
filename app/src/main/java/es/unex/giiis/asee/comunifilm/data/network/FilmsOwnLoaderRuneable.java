package es.unex.giiis.asee.comunifilm.data.network;

import android.content.Context;
import android.util.Log;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.data.roomdb.ComufilmDatabaseServer;

public class FilmsOwnLoaderRuneable implements Runnable {

    private final OnFilmsLoadedListener onFilmsLoadedListener;
    private String userName;
    private Context context;
    private ArrayList<Film> listFilm;

    public FilmsOwnLoaderRuneable(OnFilmsLoadedListener onFilmsLoadedListener, String username, Context context) {
        this.onFilmsLoadedListener = onFilmsLoadedListener;
        this.userName = username;
        this.context = context;

        listFilm = new ArrayList<Film>();
    }

    @Override
    public void run() {
        listFilm.clear();

        ComufilmDatabaseServer dbServer = ComufilmDatabaseServer.getInstance(context);
        User user = dbServer.getUserDAO().getExactUserbyName(userName);
        Log.i("MisPelis: ", "username: " + userName);
        ArrayList<Film> listAux = (ArrayList<Film>) dbServer.getFilmDAO().getFilmsbyUser((int) user.getId());
        Log.i("MisPelis: ", "username: " + listAux.size());
        for (int i = 0; i < listAux.size(); i++) {
            listFilm.add(listAux.get(i));
            Log.i("MisPelis: ", "film: " + listAux.get(i).getTitle());
            Log.i("MisPelis: ", "path poster: " + listAux.get(i).getPosterPath());
        }

        AppExecutors.getInstance().mainThread().execute(new Runnable() {
            @Override
            public void run() {
                onFilmsLoadedListener.OnFilmsLoaded(listFilm);
            }
        });

    }
}
