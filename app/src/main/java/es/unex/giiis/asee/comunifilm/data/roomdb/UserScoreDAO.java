package es.unex.giiis.asee.comunifilm.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.UserScore;

import static androidx.room.OnConflictStrategy.REPLACE;


@Dao
public interface UserScoreDAO {

    @Insert(onConflict = REPLACE)
    public long insert(UserScore uscore);

    @Query("SELECT * FROM USERSCORE WHERE id IN (:id)")
    public UserScore getScore(long id);

    @Query("SELECT * FROM USERSCORE")
    public List<UserScore> getAllScore();

    @Query("SELECT * FROM USERSCORE")
    LiveData<List<UserScore>> getAllScorelive();

    @Query("SELECT * FROM USERSCORE WHERE userScored IN (:name) ")
    public List<UserScore> getAllScoresbyUserScored(String name);

    @Query("SELECT * FROM USERSCORE WHERE autorScore IN (:autorScore) ")
    public List<UserScore> getAllScorebyautor(String autorScore);

    @Query("SELECT * FROM USERSCORE WHERE userScored IN (:userScored) AND autorScore IN (:autorScore)")
    public UserScore getScoreByAutorAndScored(String autorScore, String userScored);

    @Query("DELETE FROM USERSCORE WHERE userScored IN (:userScored) AND autorScore IN (:autorScore)")
    public void deleteByAutorAndScored(String autorScore, String userScored);

    @Update
    public int update(UserScore uscore);
}
