package es.unex.giiis.asee.comunifilm.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.Wish;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface WishDAO {

    @Insert(onConflict = REPLACE)
    public long insert(Wish wish);

    @Query("SELECT * FROM WISH WHERE id IN (:id)")
    public Wish getWish(long id);

    @Query("SELECT * FROM WISH")
    public List<Wish> getAllWish();

    @Query("SELECT * FROM WISH")
    LiveData<List<Wish>> getAllWishlive();

    @Query("SELECT * FROM WISH WHERE autorWish IN (:autorWish) ")
    public List<Wish> getAllWishbyautor(String autorWish);

    @Query("SELECT * FROM WISH WHERE autorWish IN (:autorWish) ")
    LiveData<List<Wish>> getAllWishbyautorlive(String autorWish);

    @Query("SELECT * FROM WISH WHERE autorWish IN (:autorWish) AND filmName IN (:title) ")
    public Wish isWishbyautor(String autorWish, String title);

    @Query("SELECT * FROM WISH WHERE autorWish IN (:autorWish) AND filmName IN (:title) ")
    LiveData<List<Wish>> isWishbyautorlive(String autorWish, String title);

    @Query("DELETE FROM WISH WHERE autorWish IN (:autorWish) AND filmName IN (:title)")
    abstract void deletebyquery(String autorWish, String title);

    @Query("DELETE FROM WISH WHERE filmName IN (:title)")
    abstract void deletebyfilmtitle(String title);

    @Update
    public int update(Wish wish);

}
