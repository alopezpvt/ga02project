package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.ViewModel;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.data.models.Follower;

public class AddUserToFollowViewModel extends ViewModel {


    private final RepositoryUsers mRepositoryUsers;

    public AddUserToFollowViewModel(RepositoryUsers mRepositoryUsers) {
        this.mRepositoryUsers = mRepositoryUsers;
    }

    public void insertarfollower(Follower follower) {
        mRepositoryUsers.insertarfollower(follower);
    }

}