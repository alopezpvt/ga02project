package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.Fav;


public class ListAllFilmsFavFragmentViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;
    private LiveData<List<Fav>> mAllfav;


    public ListAllFilmsFavFragmentViewModel(RepositoryFilms mRepositoryFilms) {

        this.mRepositoryFilms = mRepositoryFilms;
        mAllfav = mRepositoryFilms.getCurrentfavoritos();
    }

    public LiveData<List<Fav>> getCurrentfavoritos() {
        return mRepositoryFilms.getCurrentfavoritos();
    }
}