package es.unex.giiis.asee.comunifilm.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.network.TopFilmsNetworkLoaderRuneable;
import es.unex.giiis.asee.comunifilm.adapters.AdapterSearchFilms;
import es.unex.giiis.asee.comunifilm.data.models.Film;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TopFragment#} factory method to
 * create an instance of this fragment.
 */
public class TopFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_top, container, false);


        RecyclerView recyclerView = v.findViewById(R.id.recyclerTop);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        AdapterSearchFilms adapterSearchFilms = new AdapterSearchFilms(new ArrayList<Film>());


        AppExecutors.getInstance().networkIO().execute(new TopFilmsNetworkLoaderRuneable(listFilms -> {
            adapterSearchFilms.swap(listFilms);

            adapterSearchFilms.setOnItemClickListener(pos -> {

                Log.i("Click: ", "clicking: " + listFilms.get(pos).getTitle());
                Bundle bundle = new Bundle();
                Film film = listFilms.get(pos);
                bundle.putSerializable("film", film);

                DetailsFragment detailsFragment = new DetailsFragment();

                Navigation.findNavController(v).navigate(R.id.action_topFragment_to_detailsFragment, bundle);

            });
        }, getContext()));

        recyclerView.setAdapter(adapterSearchFilms);
        return v;
    }
}