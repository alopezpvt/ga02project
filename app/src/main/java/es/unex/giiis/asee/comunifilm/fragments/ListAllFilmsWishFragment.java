package es.unex.giiis.asee.comunifilm.fragments;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterWish;
import es.unex.giiis.asee.comunifilm.data.models.Wish;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.ListAllWishFragmentViewModel;

import static android.content.Context.MODE_PRIVATE;

public class ListAllFilmsWishFragment extends Fragment {

    private ArrayList<Wish> listWish;
    private RecyclerView recycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list_all_films_wish, container, false);

        recycler = v.findViewById(R.id.recyclerRWish);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        listWish = new ArrayList<Wish>();

        AdapterWish adapterWish = new AdapterWish(listWish);
        recycler.setAdapter(adapterWish);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        ListAllWishFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryliastallwish).get(ListAllWishFragmentViewModel.class);

        mViewModel.getallwish().observe(getActivity(), listadeseadas -> {
            Log.i("Listapendientes: ", "numero: " + listadeseadas.size());
            AdapterWish adapterWish1 = new AdapterWish((ArrayList<Wish>) listadeseadas);
            recycler.setAdapter(adapterWish1);

        });


        return v;
    }
}