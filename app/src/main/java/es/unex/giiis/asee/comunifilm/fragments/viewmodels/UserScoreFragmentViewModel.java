package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.data.models.UserScore;

public class UserScoreFragmentViewModel extends ViewModel {


    private final RepositoryUsers mRepositoryUsers;
    private LiveData<List<User>> mAllUser;

    public UserScoreFragmentViewModel(RepositoryUsers mRepositoryUsers) {
        this.mRepositoryUsers = mRepositoryUsers;
        mAllUser = mRepositoryUsers.getallusers();
    }

    public void insertaruserscore(UserScore userScore) {
        mRepositoryUsers.insertaruserscore(userScore);
    }

    public UserScore getScoreByAutorAndScore(String autor, String user) {
        return mRepositoryUsers.getScoreByAutorAndScore(autor, user).getValue();
    }

    public void updateUserScore(UserScore userScore) {
        mRepositoryUsers.updateUserScore(userScore);
    }


    public void update(User user) {
        mRepositoryUsers.update(user);
    }


}