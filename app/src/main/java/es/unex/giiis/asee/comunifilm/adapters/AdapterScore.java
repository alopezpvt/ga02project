package es.unex.giiis.asee.comunifilm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Score;


public class AdapterScore extends RecyclerView.Adapter<AdapterScore.ViewHolderScore> {
    ArrayList<Score> listscore;
    Context context;

    public AdapterScore(ArrayList<Score> listscore) {
        this.listscore = listscore;
    }

    @NonNull
    @Override
    public ViewHolderScore onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.score_list, null, false);
        context = parent.getContext();
        return new ViewHolderScore(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderScore holder, int position) {
        holder.setScore(listscore.get(position));
    }

    @Override
    public int getItemCount() {

        return listscore.size();
    }

    public class ViewHolderScore extends RecyclerView.ViewHolder {
        ImageView posterFilm;
        TextView title;
        TextView points;
        TextView username;

        public ViewHolderScore(@NonNull View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.tVUserScore);
            title = itemView.findViewById(R.id.tVTitleScore);
            posterFilm = itemView.findViewById(R.id.imgTitleFilmScore);
            points = itemView.findViewById(R.id.tVPointsScore);
        }

        public void setScore(Score score) {
            username.setText(score.getAutorScore());
            title.setText(score.getFilmName());
            points.setText(Integer.toString(score.getPoints()));

            if (score.getPosterPath() != null)
                Glide.with(context).load(score.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(posterFilm);

        }
    }
}

