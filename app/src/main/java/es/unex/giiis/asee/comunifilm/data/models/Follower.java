package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.Entity;
import androidx.room.Ignore;

@Entity(tableName = "FOLLOWER", primaryKeys = {"idFollower", "idFollowed"})
public class Follower {

    private long idFollower;
    private long idFollowed;
    private String nameFollower;
    private String nameFollowed;

    public Follower(long idFollower, long idFollowed, String nameFollower, String nameFollowed) {
        this.idFollower = idFollower;
        this.idFollowed = idFollowed;
        this.nameFollower = nameFollower;
        this.nameFollowed = nameFollowed;
    }

    @Ignore
    public Follower(long idFollower, long idFollowed) {
        this.idFollower = idFollower;
        this.idFollowed = idFollowed;
    }

    public long getIdFollower() {
        return idFollower;
    }

    public void setIdFollower(long idFollower) {
        this.idFollower = idFollower;
    }

    public long getIdFollowed() {
        return idFollowed;
    }

    public void setIdFollowed(long idFollowed) {
        this.idFollowed = idFollowed;
    }

    public String getNameFollower() {
        return nameFollower;
    }

    public void setNameFollower(String nameFollower) {
        this.nameFollower = nameFollower;
    }

    public String getNameFollowed() {
        return nameFollowed;
    }

    public void setNameFollowed(String nameFollowed) {
        this.nameFollowed = nameFollowed;
    }
}
