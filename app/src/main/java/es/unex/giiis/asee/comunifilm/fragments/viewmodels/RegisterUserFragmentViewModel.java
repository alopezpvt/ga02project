package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.data.models.User;

public class RegisterUserFragmentViewModel extends ViewModel {


    private final RepositoryUsers mRepositoryUsers;
    private LiveData<List<User>> mAllUser;
    private final LiveData<String> nombreusuario;
    private final LiveData<Integer> idusuario;

    public RegisterUserFragmentViewModel(RepositoryUsers mRepositoryUsers) {

        this.mRepositoryUsers = mRepositoryUsers;
        mAllUser=mRepositoryUsers.getallusers();
        nombreusuario=mRepositoryUsers.getnombreusuario();
        idusuario=mRepositoryUsers.getidusuario();
    }

    public void insertarusuario(User user) {
        mRepositoryUsers.insertarusuario(user);
    }

    public void registrarusuario(User user) {
        mRepositoryUsers.registrarusuario(user);
    }

    public LiveData<String> getnombreusuario() {
        return nombreusuario;
    }
    public LiveData<Integer> obteneridusuario() {
        return idusuario;
    }

    public LiveData<List<User>> getallusers() {
        return mRepositoryUsers.getallusers();
    }








}