package es.unex.giiis.asee.comunifilm.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.HideKeyboard;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterListOfList;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.ListofListViewModel;

import static android.content.Context.MODE_PRIVATE;


public class ListOfListFragment extends Fragment {

    private ArrayList<ListOfList> listListOfList = new ArrayList<ListOfList>();
    private String username;
    private SharedPreferences settings;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list_of_list, container, false);

        SharedPreferences settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
        username = settings.getString("userName", "ninguno");

        RecyclerView recyclerView = v.findViewById(R.id.recyclerList);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        AdapterListOfList adapterListOfList = new AdapterListOfList(listListOfList);
        recyclerView.setAdapter(adapterListOfList);
        Bundle bundle = new Bundle();

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        ListofListViewModel mViewModel = new ViewModelProvider(this, appContainer.factorylistoflist).get(ListofListViewModel.class);


        AppExecutors.getInstance().mainThread().execute(() -> mViewModel.getListOfListAll().observe(getActivity(), listoflist -> {
            Log.i("lista", "lista de listas: " + listoflist.toString());
            adapterListOfList.swap((ArrayList<ListOfList>) listoflist);

            adapterListOfList.setOnItemClickListener(pos -> {
                Log.i("lista: ", "clicking: " + listoflist.get(pos));
                bundle.putSerializable("listOfList", listoflist.get(pos));
                Navigation.findNavController(v).navigate(R.id.action_listOfListFragment_to_personalListOfFilmsFragment, bundle);

            });

            recyclerView.setAdapter(adapterListOfList);
        }));

        Button bSearchList = v.findViewById(R.id.bSearchList);
        EditText eTSearchListFilm = v.findViewById(R.id.eTSearchListFilm);
        bSearchList.setOnClickListener(v1 -> {

            String searchListFilm = eTSearchListFilm.getText().toString().toLowerCase();

            if (searchListFilm != null && !searchListFilm.trim().isEmpty()) {
                AppExecutors.getInstance().mainThread().execute(() -> mViewModel.getListOfListAll().observe(getActivity(), listoflist -> {

                    ArrayList<ListOfList> listAux = new ArrayList<ListOfList>();
                    for (int i = 0; i < listoflist.size(); i++) {
                        if (listoflist.get(i).getNameList().toLowerCase().contains(searchListFilm))
                            listAux.add(listoflist.get(i));
                    }

                    listListOfList.clear();
                    listListOfList = listAux;

                    AppExecutors.getInstance().mainThread().execute(() -> {
                        adapterListOfList.swap(listListOfList);

                        adapterListOfList.setOnItemClickListener(pos -> {
                            Log.i("lista: ", "clicking: " + listListOfList.get(pos));
                            bundle.putSerializable("listOfList", listListOfList.get(pos));
                            Navigation.findNavController(v1).navigate(R.id.action_listOfListFragment_to_personalListOfFilmsFragment, bundle);

                        });


                        recyclerView.setAdapter(adapterListOfList);
                    });


                }));
            } else {

                AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Debes escribir un nombre válido", Toast.LENGTH_SHORT).show());
            }

            eTSearchListFilm.setText("");


            HideKeyboard.hideKeyboard(getActivity());
        });


        Button bOwnList = v.findViewById(R.id.bCreateList);
        EditText nameOfList = v.findViewById(R.id.etFilmToCreate);
        bOwnList.setOnClickListener(v12 -> {
            AppExecutors.getInstance().diskIO().execute(() -> {

                String nameList = nameOfList.getText().toString();
                if (nameList != null && !nameList.trim().isEmpty()) {

                    ListOfList listOfList = new ListOfList(nameList, username);
                    mViewModel.insertarlistofList(listOfList);
                    Log.i("lista", "Creando nueva lista " + listOfList.getNameList());

                } else {
                    AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Debe introducirse un nombre válido.", Toast.LENGTH_SHORT).show());
                }
            });
            nameOfList.setText("");
            HideKeyboard.hideKeyboard(getActivity());
        });


        return v;
    }


}