package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.ListAndFilms;

public class PersonalListOfFilmsViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;
    private final LiveData<List<ListAndFilms>> mListAndFilms;

    public PersonalListOfFilmsViewModel(RepositoryFilms mRepositoryFilms) {
        this.mRepositoryFilms = mRepositoryFilms;
        mListAndFilms = mRepositoryFilms.getCurrentList();
    }

    public void insertarlistAndFilm(ListAndFilms listAndFilm){
        mRepositoryFilms.insertarlistAndFilm(listAndFilm);
    }
    public void setidlista(long id) {
        mRepositoryFilms.setidlista(id);
    }

    public LiveData<List<ListAndFilms>> getbusquedalista() {
        return mListAndFilms;
    }

    public Film getfilm(long id) {
      return mRepositoryFilms.getfilm(id);
    }

}