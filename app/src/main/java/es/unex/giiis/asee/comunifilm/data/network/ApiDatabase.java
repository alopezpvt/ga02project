package es.unex.giiis.asee.comunifilm.data.network;

import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import java.util.ArrayList;
import java.util.List;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.data.models.Film;


public class ApiDatabase {
    private static final String LOG_TAG = ApiDatabase.class.getSimpleName();
    private static ApiDatabase sInstance;

    // LiveData storing the latest downloaded weather forecasts
    private final MutableLiveData<Film[]> mDownloadedBusqueda;


    private ApiDatabase() {

        mDownloadedBusqueda = new MutableLiveData<>();

    }

    public synchronized static ApiDatabase getInstance() {
        if (sInstance == null) {
            sInstance = new ApiDatabase();


        }
        return sInstance;
    }

    public LiveData<Film[]> getCurrentBusqueda() {
        return mDownloadedBusqueda;
    }


    /**
     * Gets the newest repos
     */
    public void fetchbusqueda(String title, String nombreusu) {
        Log.d(LOG_TAG, "Fetch busqueda started");
        // Get gata from network and pass it to LiveData

        AppExecutors.getInstance().networkIO().execute(new FilmsSearchNetworkLoaderRuneable(new OnFilmsLoadedListener() {
            @Override
            public void OnFilmsLoaded(ArrayList<Film> listFilms) {
                mDownloadedBusqueda.postValue(listFilms.toArray(new Film[0]));
            }
        }, title, nombreusu));

    }

    public void fetchpopulares(String popular) {
        Log.i("Popular: ", "Obteniendo lista de populares");
        // Get gata from network and pass it to LiveData
        AppExecutors.getInstance().networkIO().execute(new FilmsPopularNetworkLoaderRuneable(new OnFilmsLoadedListener() {
            @Override
            public void OnFilmsLoaded(ArrayList<Film> listFilms) {
                List<Film> aux = listFilms;
                mDownloadedBusqueda.postValue(aux.toArray(new Film[0]));
            }
        }, popular));

    }

}