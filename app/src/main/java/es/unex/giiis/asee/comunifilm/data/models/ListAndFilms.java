package es.unex.giiis.asee.comunifilm.data.models;


import androidx.room.Entity;

@Entity(tableName = "LISTANDFILMS", primaryKeys = {"idList", "idFilm"})
public class ListAndFilms {


    private long idList;
    private long idFilm;
    private String nameFilm;


    public ListAndFilms(long idList, long idFilm, String nameFilm) {
        this.idList = idList;
        this.idFilm = idFilm;
        this.nameFilm = nameFilm;
    }

    public long getIdList() {
        return idList;
    }

    public void setIdList(long idList) {
        this.idList = idList;
    }

    public long getIdFilm() {
        return idFilm;
    }

    public void setIdFilm(long idFilm) {
        this.idFilm = idFilm;
    }

    public String getNameFilm() {
        return nameFilm;
    }

    public void setNameFilm(String nameFilm) {
        this.nameFilm = nameFilm;
    }

    @Override
    public String toString() {
        return "ListAndFilms{" +
                "idList=" + idList +
                ", idFilm=" + idFilm +
                '}';
    }
}
