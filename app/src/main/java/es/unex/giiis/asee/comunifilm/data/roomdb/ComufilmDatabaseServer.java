package es.unex.giiis.asee.comunifilm.data.roomdb;


import android.content.Context;
import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import es.unex.giiis.asee.comunifilm.data.models.Comment;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.Follower;
import es.unex.giiis.asee.comunifilm.data.models.Recomend;
import es.unex.giiis.asee.comunifilm.data.models.Score;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.data.models.UserScore;

@Database(entities = {Film.class, User.class, Score.class, UserScore.class, Comment.class, Recomend.class, Follower.class}, version = 1, exportSchema = false)

public abstract class ComufilmDatabaseServer extends RoomDatabase {

    private static ComufilmDatabaseServer instance;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(4);

    public synchronized static ComufilmDatabaseServer getInstance(Context context) {
        if (instance == null)
            instance = Room.databaseBuilder(context, ComufilmDatabaseServer.class, "ComunifilmServer.db").addCallback(Datos).build();
        return instance;
    }

    public abstract FilmDAO getFilmDAO();

    public abstract UserDAO getUserDAO();

    public abstract ScoreDAO getScoreDAO();

    public abstract UserScoreDAO getUserScoreDAO();

    public abstract RecomendDAO getRecomendDAO();

    public abstract CommentDAO getCommentDAO();

    public abstract FollowerDAO getFollowerDAO();


    // Creacion de objetos dentro de la base de datos
    private static RoomDatabase.Callback Datos = new RoomDatabase.Callback() {
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);

            databaseWriteExecutor.execute(() -> {

                FilmDAO filmDAO = instance.getFilmDAO();
                UserDAO userDAO = instance.getUserDAO();

                userDAO.insert(new User("Dummy1", "Pass1", "sample@gmail.com"));
                userDAO.insert(new User("Dummy2", "Pass2", "sample2@gmail.com"));
                userDAO.insert(new User("Dummy3", "Pass3", "sample3@gmail.com"));

            });
        }
    };


}
