package es.unex.giiis.asee.comunifilm.data.network;

import android.view.LayoutInflater;
import com.google.android.material.chip.ChipGroup;
import java.io.IOException;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.data.models.Genres;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FilmsGenresNetworkLoaderRuneable implements Runnable {

    private final OnGenresLoadedListener onGenresLoadedListener;
    private static String BASE_URL = "https://api.themoviedb.org";
    private static String API_KEY = "5e401542b701d60f6992434b292a6b7c";
    private String languaje = "en-ES";
    private Genres genres;

    private LayoutInflater inflater;
    private ChipGroup cgCategories;


    public FilmsGenresNetworkLoaderRuneable(OnGenresLoadedListener onGenresLoadedListener) {
        this.onGenresLoadedListener = onGenresLoadedListener;
        this.genres = new Genres();
    }


    @Override
    public void run() {

        genres.clear();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        try {
            genres = apiInterface.genres(ApiInterface.API_KEY, ApiInterface.LANGUAGE).execute().body();

            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {
                    onGenresLoadedListener.OnFilmsLoaded(genres);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
