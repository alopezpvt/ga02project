package es.unex.giiis.asee.comunifilm;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import es.unex.giiis.asee.comunifilm.fragments.RegisterOrLoggerFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        SharedPreferences settings = getSharedPreferences("user", MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        if(settings.getString("userName", "none").equals("none")) {

            RegisterOrLoggerFragment registerOrLoggerFragment = new RegisterOrLoggerFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.registerOrLoggerFragment, registerOrLoggerFragment).commit();
        } else {

            Intent intent = new Intent(this, NavDrawActivity.class);
            startActivity(intent);
            HideKeyboard.hideKeyboard(this);
            finish();
        }
    }
}