package es.unex.giiis.asee.comunifilm.data.roomdb;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.Comment;
import static androidx.room.OnConflictStrategy.REPLACE;


@Dao
public interface CommentDAO {
    @Insert(onConflict = REPLACE)
    public long insert(Comment comment);

    @Query("SELECT * FROM COMMENTS WHERE id IN (:id)")
    public Comment getComment(long id);

    @Query("SELECT * FROM COMMENTS")
    public List<Comment> getAllComments();

    @Query("SELECT * FROM COMMENTS")
    LiveData<List<Comment>> getAllCommentslive();

    @Query("SELECT * FROM COMMENTS WHERE filmID = :id")
    public List<Comment> getCommentsByFilmID(long id);

    @Query("SELECT * FROM COMMENTS WHERE filmID = :id")
    LiveData<List<Comment>> getCommentsByFilmIDlive(long id);

    @Query("DELETE FROM COMMENTS WHERE filmName IN (:title)")
    abstract void deletebyfilmname(String title);

    @Update
    public int update(Comment user);
}
