package es.unex.giiis.asee.comunifilm.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface FilmDAO {

    @Insert(onConflict = REPLACE)
    public long insert(Film film);

    @Insert(onConflict = REPLACE)
    void bulkInsert(List<Film> film);

    @Query("SELECT * FROM FILMS WHERE id IN (:id)")
    public Film getFilm(long id);

    @Query("SELECT * FROM FILMS")
    public List<Film> getAllFilms();

    @Query("SELECT * FROM FILMS")
    public LiveData<List<Film>> getAllFilmsLive();

    @Query("SELECT * FROM FILMS WHERE title like :title")
    LiveData<List<Film>> getReposBytitle(String title);

    @Query("SELECT * FROM FILMS WHERE type like :type ORDER BY popularity DESC")
    LiveData<List<Film>> getAllPopularsLive(String type);

    @Query("SELECT * FROM FILMS WHERE title IN (:title) ")
    public Film getFilmbyTitle(String title);

    @Query("SELECT count(*) FROM FILMS WHERE title = :title")
    int getNumberFilmbytitle(String title);

    @Query("SELECT * FROM FILMS WHERE idUser IN (:idUser) ")
    public List<Film> getFilmsbyUser(int idUser);

    @Query("UPDATE FILMS SET genreIds = :genero  WHERE title like :title ")
    public void genero(int genero,String title);

    @Query("DELETE FROM FILMS WHERE title like :title")
    int deleteReposByUser(String title);

    @Query("DELETE FROM FILMS WHERE type = 'popular'")
    int deletePopular();

    @Query("DELETE FROM FILMS WHERE id IN (:id)")
    abstract public void deleteFilm(int id);

    @Update
    public int update(Film film);

}
