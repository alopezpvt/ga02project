package es.unex.giiis.asee.comunifilm.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterFav;
import es.unex.giiis.asee.comunifilm.data.models.Fav;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.ListAllFilmsFavFragmentViewModel;



public class ListAllFilmsFavFragment extends Fragment {

    private ArrayList<Fav> listFav;
    private RecyclerView recycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list_all_films_fav, container, false);

        recycler = v.findViewById(R.id.recyclerFav);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        listFav = new ArrayList<Fav>();

        AdapterFav adapterFav = new AdapterFav(listFav);
        recycler.setAdapter(adapterFav);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        ListAllFilmsFavFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryliastallfilmsfav).get(ListAllFilmsFavFragmentViewModel.class);

        mViewModel.getCurrentfavoritos().observe(getActivity(), listarecomendadas -> {
            Log.i("Listafav: ", "numero: " + listarecomendadas.size());
            AdapterFav adapterFav1 = new AdapterFav((ArrayList<Fav>) listarecomendadas);
            recycler.setAdapter(adapterFav1);
        });

        return v;
    }
}