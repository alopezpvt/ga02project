package es.unex.giiis.asee.comunifilm.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.fragment.app.Fragment;
import es.unex.giiis.asee.comunifilm.NavDrawActivity;
import es.unex.giiis.asee.comunifilm.R;


public class RegisterOrLoggerFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register_or_logger, container, false);

        Button bRegister = v.findViewById(R.id.bRegister);
        Button bJoin = v.findViewById(R.id.bJoin);
        Button bSkip = v.findViewById(R.id.bSkip);

        bRegister.setOnClickListener(v1 -> {
            RegisterUserFragment registerUserFragment = new RegisterUserFragment();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.registerOrLoggerFragment, registerUserFragment).commit();

        });

        bJoin.setOnClickListener(v12 -> {
            LogginUserFragment logginUserFragment = new LogginUserFragment();
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.registerOrLoggerFragment, logginUserFragment).commit();


        });

        bSkip.setOnClickListener(v13 -> {

            Intent intent = new Intent(getActivity(), NavDrawActivity.class);
            startActivity(intent);

        });

        return v;
    }
}