package es.unex.giiis.asee.comunifilm.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.R;

import static android.content.Context.MODE_PRIVATE;

public class WishFragment extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_wish, container, false);

        SharedPreferences settings = getContext().getSharedPreferences("user", MODE_PRIVATE);
        if (settings.getString("userName", "none").equals("none")) {
            getActivity().onBackPressed();
            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(), "You aren't register", Toast.LENGTH_SHORT).show();
                }
            });

        } else {

            Bundle bundle = new Bundle();
            bundle.putString("CommentOrRecommend", "wish");
            SearchFilmFragment searchFilmFragment = new SearchFilmFragment();
            searchFilmFragment.setArguments(bundle);
            getActivity().getSupportFragmentManager().beginTransaction().add(R.id.searchFilmFragment, searchFilmFragment).commit();
        }
        return v;

    }
}