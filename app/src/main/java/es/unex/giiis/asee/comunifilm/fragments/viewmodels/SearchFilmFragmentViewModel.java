package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.Film;

public class SearchFilmFragmentViewModel extends ViewModel {

    private final RepositoryFilms mRepositoryFilms;
    private final LiveData<List<Film>> mFilms;
    private String mTitle = "";

    public SearchFilmFragmentViewModel(RepositoryFilms repositoryFilms) {
        mRepositoryFilms = repositoryFilms;
        mFilms = mRepositoryFilms.getCurrentBusqueda();
    }
    public void settitle(String title) {
        mTitle = title;
        mRepositoryFilms.settitle(title);
    }
    public void populares() {
        mRepositoryFilms.doFetchpopular();
    }
    public void setuser(String user) {
        mRepositoryFilms.setuser(user);
    }
    public void onRefresh() {
        if(!mTitle.isEmpty())
        mRepositoryFilms.doFetchbusqueda(mTitle);
    }
    public void onRefreshTop() {
        mRepositoryFilms.doFetchbusqueda(mTitle);
    }

    public LiveData<List<Film>> getbusqueda() {
        return mFilms;
    }
}