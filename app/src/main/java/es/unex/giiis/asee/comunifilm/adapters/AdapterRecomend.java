package es.unex.giiis.asee.comunifilm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Recomend;

public class AdapterRecomend extends RecyclerView.Adapter<AdapterRecomend.ViewHolderRecomend> {

    ArrayList<Recomend> listRecomends;
    Context context;

    public AdapterRecomend(ArrayList<Recomend> listRecomends) {
        this.listRecomends = listRecomends;
    }

    @NonNull
    @Override
    public ViewHolderRecomend onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recomend_list, null, false);
        context = parent.getContext();
        return new ViewHolderRecomend(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderRecomend holder, int position) {
        holder.setRecomend(listRecomends.get(position));
    }

    @Override
    public int getItemCount() {
        return listRecomends.size();
    }

    public class ViewHolderRecomend extends RecyclerView.ViewHolder {

        TextView userName;
        TextView title;
        ImageView posterFilm;


        public ViewHolderRecomend(@NonNull View itemView) {
            super(itemView);

            userName = itemView.findViewById(R.id.tVUserRecomend);
            title = itemView.findViewById(R.id.tVTitleFilmRecomended);
            posterFilm = itemView.findViewById(R.id.imgTitleFilmRecomended);

        }

        public void setRecomend(Recomend recomend) {

            userName.setText(recomend.getAutorRecomend());
            title.setText(recomend.getFilmName());

            if (recomend.getPosterPath() != null)
                Glide.with(context).load(recomend.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(posterFilm);


        }
    }
}
