package es.unex.giiis.asee.comunifilm.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.HideKeyboard;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterSearchUser;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.SearchUserFragmentViewModel;

import static android.content.Context.MODE_PRIVATE;

public class SearchUserFragment extends Fragment {

    private String commentOrRecommend = "null";
    private long idFollower;
    private String userName;
    private SharedPreferences settings;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            commentOrRecommend = getArguments().getString("CommentOrRecommend");
        }
        SharedPreferences settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
        idFollower = settings.getLong("userID", -1);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_search_user, container, false);

        EditText eTSearchUser = v.findViewById(R.id.eTSearchUser);

        Button bSearch = v.findViewById(R.id.bSearch2);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        SearchUserFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factorysearchuser).get(SearchUserFragmentViewModel.class);

        mViewModel.getallusers();

        RecyclerView recyclerView = v.findViewById(R.id.recyclerSearchUser);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));


        AdapterSearchUser adapterSearchUser = new AdapterSearchUser(new ArrayList<User>());


        settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
        userName = settings.getString("userName", "none");

        if (commentOrRecommend.equals("listUserFollowing")) {

            mViewModel.getallfollower(userName).observe(getActivity(), listafollower -> {

                ArrayList<User> listuser = new ArrayList<User>();


                AppExecutors.getInstance().diskIO().execute(() -> {
                    for (int i = 0; i < listafollower.size(); i++) {
                        User user = mViewModel.getuser(listafollower.get(i).getIdFollowed());
                        if (user.getId() != idFollower)
                            listuser.add(user);
                    }

                    AppExecutors.getInstance().mainThread().execute(() -> {

                        adapterSearchUser.swap(listuser);
                        adapterSearchUser.setOnItemClickListener(new AdapterSearchUser.OnItemClickListener() {
                            @Override
                            public void onItemClick(int pos) {
                                Bundle bundle = new Bundle();
                                User user = listuser.get(pos);
                                bundle.putSerializable("user", user);
                                Navigation.findNavController(v).navigate(R.id.action_searchUserFragment_to_unFollowUserFragment, bundle);
                            }
                        });
                        recyclerView.setAdapter(adapterSearchUser);
                    });
                });
            });
        }

        if (commentOrRecommend.equals("listUserFollowed")) {

            AppExecutors.getInstance().mainThread().execute(() -> {
                mViewModel.getallfollowed(userName).observe(getActivity(), listafollower -> {

                    ArrayList<User> listuser = new ArrayList<User>();

                    AppExecutors.getInstance().diskIO().execute(() -> {
                        for (int i = 0; i < listafollower.size(); i++) {
                            User user = mViewModel.getuser(listafollower.get(i).getIdFollower());
                            if (user.getId() != idFollower)
                                listuser.add(user);
                        }
                        AppExecutors.getInstance().mainThread().execute(() -> {

                            adapterSearchUser.swap(listuser);
                            recyclerView.setAdapter(adapterSearchUser);
                        });
                    });
                });
            });
        }

        bSearch.setOnClickListener(v1 -> {
            String name = eTSearchUser.getText().toString().toLowerCase();

            AppExecutors.getInstance().networkIO().execute(() -> {

                ArrayList<User> total = (ArrayList<User>) mViewModel.getallusers().getValue();
                ArrayList<User> listuser = new ArrayList<User>();

                for (int i = 0; i < total.size(); i++) {
                    if (total.get(i).getName().toLowerCase().contains(name) && total.get(i).getId() != idFollower) {
                        listuser.add(total.get(i));
                        Log.i("filmUser: ", "user: " + total.get(i).getName());
                    }
                }

                if (commentOrRecommend.equals("searchUser")) {

                    AppExecutors.getInstance().mainThread().execute(() -> {

                        adapterSearchUser.swap(listuser);
                        adapterSearchUser.setOnItemClickListener(pos -> {
                            Bundle bundle = new Bundle();
                            User user = listuser.get(pos);
                            bundle.putSerializable("user", user);
                            Log.i("user", "user: " + user.getName());
                            Navigation.findNavController(v1).navigate(R.id.action_searchUserFragment_to_addUserToFollowFragment, bundle);
                        });
                        recyclerView.setAdapter(adapterSearchUser);

                    });

                }
                if (commentOrRecommend.equals("listUserFollowing")) {

                    AppExecutors.getInstance().mainThread().execute(() -> {
                        mViewModel.getallfollower(userName).observe(getActivity(), listafollower -> {

                            ArrayList<User> listuser1 = new ArrayList<User>();


                            AppExecutors.getInstance().diskIO().execute(() -> {
                                for (int i = 0; i < listafollower.size(); i++) {
                                    User user = mViewModel.getuser(listafollower.get(i).getIdFollowed());
                                    if (user.getId() != idFollower)
                                        listuser1.add(user);
                                }


                                AppExecutors.getInstance().mainThread().execute(() -> {

                                    adapterSearchUser.swap(listuser1);
                                    adapterSearchUser.setOnItemClickListener(new AdapterSearchUser.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(int pos) {
                                            Bundle bundle = new Bundle();
                                            User user = listuser1.get(pos);
                                            bundle.putSerializable("user", user);
                                            Navigation.findNavController(v1).navigate(R.id.action_searchUserFragment_to_unFollowUserFragment, bundle);
                                        }
                                    });
                                    recyclerView.setAdapter(adapterSearchUser);

                                });


                            });
                        });

                    });
                }
                if (commentOrRecommend.equals("listUserFollowed")) {

                    AppExecutors.getInstance().mainThread().execute(() -> {
                        mViewModel.getallfollowed(userName).observe(getActivity(), listafollower -> {

                            ArrayList<User> listuser12 = new ArrayList<User>();

                            AppExecutors.getInstance().diskIO().execute(() -> {
                                for (int i = 0; i < listafollower.size(); i++) {
                                    User user = mViewModel.getuser(listafollower.get(i).getIdFollower());
                                    if (user.getId() != idFollower)
                                        listuser12.add(user);
                                }
                                AppExecutors.getInstance().mainThread().execute(() -> {

                                    adapterSearchUser.swap(listuser12);
                                    recyclerView.setAdapter(adapterSearchUser);
                                });
                            });
                        });
                    });
                }
                if (commentOrRecommend == "null") {
                    AppExecutors.getInstance().mainThread().execute(() -> {

                        adapterSearchUser.swap(listuser);
                        adapterSearchUser.setOnItemClickListener(pos -> {
                            Bundle bundle = new Bundle();
                            User user = listuser.get(pos);
                            bundle.putSerializable("user", user);

                            UserScoreFragment scoreUserFragment = new UserScoreFragment();

                            scoreUserFragment.setArguments(bundle);
                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.searchUserFragment, scoreUserFragment).commit();

                        });
                        recyclerView.setAdapter(adapterSearchUser);
                    });
                }
            });
            eTSearchUser.setText("");
            HideKeyboard.hideKeyboard(getActivity());
        });

        recyclerView.setAdapter(adapterSearchUser);
        return v;
    }
}
