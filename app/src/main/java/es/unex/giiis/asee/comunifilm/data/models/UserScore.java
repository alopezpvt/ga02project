package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "USERSCORE")
public class UserScore {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String autorScore;
    private String userScored;
    private int points;


    public UserScore(long id, String autorScore, String userScored, int points) {
        this.id = id;
        this.autorScore = autorScore;
        this.userScored = userScored;
        this.points = points;

    }

    @Ignore
    public UserScore(String autorScore, String userScored, int points) {
        this.autorScore = autorScore;
        this.userScored = userScored;
        this.points = points;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAutorScore() {
        return autorScore;
    }

    public void setAutorScore(String autorScore) {
        this.autorScore = autorScore;
    }

    public String getUserScored() {
        return userScored;
    }

    public void setUserScored(String userScored) {
        this.userScored = userScored;
    }

    @Override
    public String toString() {
        return "Score{" +
                "id=" + id +
                ", autorScore='" + autorScore + '\'' +
                ", userScored='" + userScored + '\'' +
                ", points='" + points + '\'' +
                '}';
    }
}
