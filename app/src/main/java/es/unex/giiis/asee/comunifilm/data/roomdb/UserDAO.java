package es.unex.giiis.asee.comunifilm.data.roomdb;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.User;
import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface UserDAO {
    @Insert(onConflict = REPLACE)
    public long insert(User user);

    @Query("SELECT * FROM USERS WHERE id IN (:id)")
    public User getUser(long id);

    @Query("SELECT * FROM USERS WHERE id =:id")
    public List<User> getUsers(long id);

    @Query("SELECT * FROM USERS")
    public List<User> getAllUser();


    @Query("SELECT * FROM USERS")
    LiveData<List<User>> getAllUserlive();

    @Query("SELECT id FROM USERS WHERE userName IN (:name)")
    LiveData<Integer> getidbyname(String name);

    @Query("SELECT COUNT(*) FROM USERS WHERE userName IN (:name)")
    int getidbynamenum(String name);

    @Update
    public int update(User user);

    @Query("SELECT * FROM USERS WHERE userName IN (:name)")
    public User existUser(String name);

    @Query("SELECT * FROM USERS WHERE userName LIKE (:name)")
    public List<User> getUserbyName(String name);

    @Query("DELETE FROM USERS WHERE userName IN (:name) AND email IN (:mail)")
    abstract void deleteUser(String name, String mail);

    @Query("SELECT * FROM USERS WHERE userName IN (:name)")
    public User getExactUserbyName(String name);
}
