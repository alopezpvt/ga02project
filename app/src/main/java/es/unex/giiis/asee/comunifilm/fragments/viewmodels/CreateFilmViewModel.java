package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.ViewModel;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.Film;

public class CreateFilmViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;

    public CreateFilmViewModel(RepositoryFilms mRepositoryFilms) {
        this.mRepositoryFilms = mRepositoryFilms;
    }

    public void insertarfilm(Film film) {
        mRepositoryFilms.insertarfilm(film);
    }
}