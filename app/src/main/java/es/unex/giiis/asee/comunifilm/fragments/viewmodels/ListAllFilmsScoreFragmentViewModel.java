package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.Score;

public class ListAllFilmsScoreFragmentViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;
    private LiveData<List<Score>> mAllscore;


    public ListAllFilmsScoreFragmentViewModel(RepositoryFilms mRepositoryFilms) {

        this.mRepositoryFilms = mRepositoryFilms;
        mAllscore = mRepositoryFilms.getallfilmsscore();
    }

    public LiveData<List<Score>> getallfilmsscore() {
        return mRepositoryFilms.getallfilmsscore();
    }
}