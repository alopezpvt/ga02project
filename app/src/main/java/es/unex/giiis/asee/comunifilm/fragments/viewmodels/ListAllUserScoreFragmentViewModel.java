package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.data.models.UserScore;

public class ListAllUserScoreFragmentViewModel extends ViewModel {


    private final RepositoryUsers mRepositoryUsers;
    private LiveData<List<UserScore>> mAllUserScore;


    public ListAllUserScoreFragmentViewModel(RepositoryUsers mRepositoryUsers) {

        this.mRepositoryUsers = mRepositoryUsers;
        mAllUserScore = mRepositoryUsers.getalluserscore();
    }

    public LiveData<List<UserScore>> getalluserscore() {
        return mRepositoryUsers.getalluserscore();
    }
}