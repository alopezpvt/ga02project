package es.unex.giiis.asee.comunifilm.ui.profile;


import androidx.lifecycle.ViewModel;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.data.models.User;


public class ProfileFragmentViewModel extends ViewModel {

    private final RepositoryUsers mRepositoryUsers;
    private final RepositoryFilms mrepositoryFilms;

    public ProfileFragmentViewModel(RepositoryUsers mRepositoryUsers, RepositoryFilms mrepositoryFilms) {
        this.mRepositoryUsers = mRepositoryUsers;
        this.mrepositoryFilms = mrepositoryFilms;
    }

    public void reinicio() {
        mrepositoryFilms.reinicio();
        mRepositoryUsers.reinicio();
    }

    public void eleminarusuario(User user) {
        mRepositoryUsers.eleminarusuario(user);
    }

}