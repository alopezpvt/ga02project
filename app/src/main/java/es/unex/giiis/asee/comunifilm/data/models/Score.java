package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "SCORE")
public class Score {

    @PrimaryKey(autoGenerate = true)
    private long id;
    private String autorScore;
    private String filmName;
    private String posterPath;
    private int points;


    public Score(long id, String autorScore, String filmName, String posterPath, int points) {
        this.id = id;
        this.autorScore = autorScore;
        this.filmName = filmName;
        this.posterPath = posterPath;
        this.points = points;
    }

    @Ignore
    public Score(String autorScore, String filmName, String posterPath, int points) {
        this.id = id;
        this.autorScore = autorScore;
        this.filmName = filmName;
        this.posterPath = posterPath;
        this.points = points;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAutorScore() {
        return autorScore;
    }

    public void setAutorScore(String autorScore) {
        this.autorScore = autorScore;
    }


    @Override
    public String toString() {
        return "Score{" +
                "id=" + id +
                ", autorScore='" + autorScore + '\'' +
                ", filmName='" + filmName + '\'' +
                ", posterPath='" + posterPath + '\'' +
                ", points='" + points + '\'' +
                '}';
    }
}
