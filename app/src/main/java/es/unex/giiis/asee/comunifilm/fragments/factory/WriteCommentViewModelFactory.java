package es.unex.giiis.asee.comunifilm.fragments.factory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.WriteCommentFragmentViewModel;


public class WriteCommentViewModelFactory extends ViewModelProvider.NewInstanceFactory {


    private final RepositoryFilms mrepositoryFilms;

    public WriteCommentViewModelFactory( RepositoryFilms mrepositoryFilms) {

        this.mrepositoryFilms = mrepositoryFilms;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new WriteCommentFragmentViewModel(mrepositoryFilms);
    }
}
