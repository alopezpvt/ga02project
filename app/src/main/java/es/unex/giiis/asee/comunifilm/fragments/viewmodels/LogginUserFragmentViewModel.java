package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.data.models.User;

public class LogginUserFragmentViewModel extends ViewModel {


    private final RepositoryUsers mRepositoryUsers;
    private LiveData<List<User>> mAllUser;


    public LogginUserFragmentViewModel(RepositoryUsers mRepositoryUsers) {

        this.mRepositoryUsers = mRepositoryUsers;
        mAllUser = mRepositoryUsers.getallusers();
    }

    public LiveData<List<User>> getallusers() {
        return mRepositoryUsers.getallusers();
    }

    public void setuser(String nombre) {
        mRepositoryUsers.setuser(nombre);
    }


}