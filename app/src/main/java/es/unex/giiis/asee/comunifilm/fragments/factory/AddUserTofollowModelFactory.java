package es.unex.giiis.asee.comunifilm.fragments.factory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.AddUserToFollowViewModel;

public class AddUserTofollowModelFactory extends ViewModelProvider.NewInstanceFactory {


    private final RepositoryUsers mRepositoryUsers;

    public AddUserTofollowModelFactory(RepositoryUsers mRepositoryUsers) {

        this.mRepositoryUsers = mRepositoryUsers;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new AddUserToFollowViewModel(mRepositoryUsers);
    }
}
