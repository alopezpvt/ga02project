package es.unex.giiis.asee.comunifilm.ui.profile;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MainActivity;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.User;

import static android.content.Context.MODE_PRIVATE;

public class ProfileFragment extends Fragment {



    private ProfileFragmentViewModel mViewModel;
    private User user = new User();
    private String userName;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_fragment, container, false);

        SharedPreferences settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
        userName = settings.getString("userName","Ninguno");
        user.setName(userName);
        user.setEmail(settings.getString("email","Ninguno"));
        user.setPassword(settings.getString("password","Ninguno"));

        TextView tVusername = v.findViewById(R.id.tVNomUsuario);
        TextView tVpassword = v.findViewById(R.id.tVPassUser);
        TextView tVemail = v.findViewById(R.id.tVemail);

        tVusername.setText(user.getName());
        tVpassword.setText(user.getPassword());
        tVemail.setText(user.getEmail());

        Button delete = v.findViewById(R.id.bBorrarUsuario);
        Button desconectar = v.findViewById(R.id.bDesconectar);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryprofile).get(ProfileFragmentViewModel.class);

        delete.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("¿Estás seguro de querer borrar al usuario?");
            builder.setPositiveButton("Yes", (dialogInterface, i) -> AppExecutors.getInstance().diskIO().execute(() -> {

                mViewModel.reinicio();
                User usuarioaborrar=new User(user.getName(),"xxxxxxxx", user.getEmail());
                mViewModel.eleminarusuario(usuarioaborrar);
                SharedPreferences.Editor editor = settings.edit();

                editor.clear();

                editor.commit();

                AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Has borrado el usuario", Toast.LENGTH_SHORT).show());
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }));
            builder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.cancel());

            builder.create().show();
            });


        desconectar.setOnClickListener(view -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("¿Estás seguro de querer cerrar sesion?");
            builder.setPositiveButton("Yes", (dialogInterface, i) -> AppExecutors.getInstance().diskIO().execute(() -> {
                mViewModel.reinicio();
                SharedPreferences.Editor editor = settings.edit();

                editor.clear();

                editor.commit();

                AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Cerrando Sesion", Toast.LENGTH_SHORT).show());
                Intent intent = new Intent(getContext(), MainActivity.class);
                startActivity(intent);
                getActivity().finish();
            }));
            builder.setNegativeButton("No", (dialogInterface, i) -> dialogInterface.cancel());


            builder.create().show();
        });

        return v;
    }

}