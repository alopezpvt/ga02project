package es.unex.giiis.asee.comunifilm.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import com.bumptech.glide.Glide;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.DeleteFilmOfPersonalListViewModel;


public class DeleteFilmOfPersonalListFragment extends Fragment {

    private Film film;
    private ListOfList listOfList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            film = (Film) getArguments().getSerializable("film");
            listOfList = (ListOfList) getArguments().getSerializable("listOfList");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_delete_film_of_personal_list, container, false);

        ImageView imgPoster = v.findViewById(R.id.imgPosterFilmDelete);
        TextView tvTitle = v.findViewById(R.id.tvTitleFilm);
        TextView tvDescription = v.findViewById(R.id.tvDescriptionFilm);
        Button noDelete = v.findViewById(R.id.bBack);
        Button siDelete = v.findViewById(R.id.bsiDelete);
        Bundle bundle = new Bundle();

        if (film.getPosterPath() != null)
            Glide.with(getContext()).load(film.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(imgPoster);

        tvTitle.setText(film.getTitle());
        tvDescription.setText(film.getOverview());
        bundle.putSerializable("listOfList", listOfList);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        DeleteFilmOfPersonalListViewModel mViewModel = new ViewModelProvider(this, appContainer.factorydeletefilmofpersonal).get(DeleteFilmOfPersonalListViewModel.class);


        noDelete.setOnClickListener(v1 -> Navigation.findNavController(v1).navigate(R.id.action_deleteFilmOfPersonalListFragment_to_personalListOfFilmsFragment, bundle));

        siDelete.setOnClickListener(v12 -> AppExecutors.getInstance().networkIO().execute(new Runnable() {
            @Override
            public void run() {
                mViewModel.deletefilmofpersonallist(listOfList.getIdList(), film.getId());
                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        Navigation.findNavController(v12).navigate(R.id.action_deleteFilmOfPersonalListFragment_to_personalListOfFilmsFragment, bundle);
                    }
                });

            }
        }));

        return v;
    }
}