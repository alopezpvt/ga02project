package es.unex.giiis.asee.comunifilm.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;


import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ListOfListDAO {

    @Insert(onConflict = REPLACE)
    public long insert(ListOfList listOfList);

    @Query("SELECT * FROM LISTOFLIST WHERE idList IN (:id)")
    public List<ListOfList> getListOfListById(long id);

    @Query("SELECT * FROM LISTOFLIST WHERE nameuser IN (:nameuser)")
    LiveData<List<ListOfList>> getListOfListBynameuser(String nameuser);

    @Query("SELECT * FROM LISTOFLIST")
    public List<ListOfList> getListOfListAll();

    @Query("SELECT * FROM LISTOFLIST")
    LiveData<List<ListOfList>> getListOfListAlllive();

    @Query("SELECT * FROM LISTOFLIST WHERE nameList IN (:name)")
    boolean exist(String name);
}
