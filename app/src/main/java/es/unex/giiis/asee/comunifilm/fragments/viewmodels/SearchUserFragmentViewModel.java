package es.unex.giiis.asee.comunifilm.fragments.viewmodels;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.data.models.Follower;
import es.unex.giiis.asee.comunifilm.data.models.User;

public class SearchUserFragmentViewModel extends ViewModel {


    private final RepositoryUsers mRepositoryUsers;
    private LiveData<List<Follower>> mallfollower;
    private LiveData<List<Follower>> mallfollowed;


    public SearchUserFragmentViewModel(RepositoryUsers mRepositoryUsers) {
        this.mRepositoryUsers = mRepositoryUsers;
        mallfollower = mRepositoryUsers.getallfollower("");
        mallfollowed = mRepositoryUsers.getallfollower("");
        mRepositoryUsers.getallusers();
    }

    public void eleminarfollower(long idFollower, long id) {
        mRepositoryUsers.eleminarfollower(idFollower, id);
    }

    public LiveData<List<User>> getusers(long iduser) {
        return mRepositoryUsers.getusers(iduser);
    }

    public User getuser(long userid) {
        return mRepositoryUsers.getusuario(userid);
    }

    public LiveData<List<User>> getallusers() {
        return mRepositoryUsers.getallusers();
    }

    public LiveData<List<Follower>> getallfollower(String nombre) {
        return mRepositoryUsers.getallfollower(nombre);
    }

    public LiveData<List<Follower>> getallfollowed(String nombre) {
        return mRepositoryUsers.getallfollowed(nombre);
    }


}