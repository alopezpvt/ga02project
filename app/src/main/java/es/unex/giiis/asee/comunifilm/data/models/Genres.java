package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.Ignore;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

public class Genres {
    @SerializedName("genres")
    @Expose
    private List<Genre> genres;

    public Genres(List<Genre> genres) {
        this.genres = genres;
    }

    @Ignore
    public Genres() {
        genres = new ArrayList<Genre>();
    }

    public List<Genre> getListGenres() {
        return genres;
    }

    public void setListGenres(List<Genre> listGenres) {
        this.genres = listGenres;
    }

    @Ignore
    public void clear() {
        genres.clear();
    }
}
