package es.unex.giiis.asee.comunifilm.fragments.factory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.ListAllFilmsRecomendFragmentViewModel;


public class ListAllFilmsRecomendViewModelFactory extends ViewModelProvider.NewInstanceFactory {


    private final RepositoryFilms mRepositoryFilms;

    public ListAllFilmsRecomendViewModelFactory(RepositoryFilms mRepositoryFilms) {

        this.mRepositoryFilms = mRepositoryFilms;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ListAllFilmsRecomendFragmentViewModel(mRepositoryFilms);
    }
}
