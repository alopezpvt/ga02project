package es.unex.giiis.asee.comunifilm.data.models;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

@Entity(tableName = "COMMENTS")
public class Comment implements Serializable {

    @SerializedName("id")
    @Expose
    @PrimaryKey(autoGenerate = true)
    private long id;

    @SerializedName("idUser")
    @Expose
    private long idUser;

    @SerializedName("autorComment")
    @Expose
    private String autorComment;

    @SerializedName("filmName")
    @Expose
    private String filmName;

    @SerializedName("filmID")
    @Expose
    private long filmID;

    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("posterPath")
    @Expose
    private String posterPath;

    @SerializedName("likes")
    @Expose
    private int likes;

    public Comment(long id, long idUser, String autorComment, String filmName, long filmID, String text, int likes, String posterPath) {
        this.id = id;
        this.idUser = idUser;
        this.autorComment = autorComment;
        this.filmName = filmName;
        this.filmID = filmID;
        this.text = text;
        this.likes = likes;
        this.posterPath = posterPath;
    }


    @Ignore
    public Comment(long idUser, String autorComment, String filmName, long filmID, String text, int likes, String posterPath) {
        this.idUser = idUser;
        this.autorComment = autorComment;
        this.filmName = filmName;
        this.filmID = filmID;
        this.text = text;
        this.likes = likes;
        this.posterPath = posterPath;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public long getFilmID() {
        return filmID;
    }

    public void setFilmID(long filmID) {
        this.filmID = filmID;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getAutorComment() {
        return autorComment;
    }

    public void setAutorComment(String autorComment) {
        this.autorComment = autorComment;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", idUser=" + idUser +
                ", autorComment='" + autorComment + '\'' +
                ", filmName='" + filmName + '\'' +
                ", text='" + text + '\'' +
                ", posterPath='" + posterPath + '\'' +
                ", likes=" + likes +
                '}';
    }
}

