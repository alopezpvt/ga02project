package es.unex.giiis.asee.comunifilm.fragments.factory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryUsers;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.LogginUserFragmentViewModel;


public class LogginUserViewModelFactory extends ViewModelProvider.NewInstanceFactory {


    private final RepositoryUsers mrepositoryUsers;

    public LogginUserViewModelFactory(RepositoryUsers mrepositoryUsers) {

        this.mrepositoryUsers = mrepositoryUsers;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new LogginUserFragmentViewModel(mrepositoryUsers);
    }
}
