package es.unex.giiis.asee.comunifilm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Comment;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.WriteCommentFragmentViewModel;


public class WriteCommentFragment extends DialogFragment {


    private Film film;
    private User user;
    private WriteCommentFragmentViewModel mViewModel;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            film = (Film) getArguments().getSerializable("film");
            user = (User) getArguments().getSerializable("user");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = requireActivity().getLayoutInflater();

        View v = inflater.inflate(R.layout.fragment_write_comment, null);
        EditText eTComment = v.findViewById(R.id.eTComment);
        Log.i("Bundle", "Recibido datos: " + film.getTitle());

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factorywritecomment).get(WriteCommentFragmentViewModel.class);

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(v)
                // Add action buttons
                .setPositiveButton(R.string.addComment, (dialog, id) -> {

                    Comment comment = new Comment(user.getId(), user.getName(), film.getTitle(), film.getId(), eTComment.getText().toString(), 0, film.getPosterPath());
                    AppExecutors.getInstance().diskIO().execute(() -> {
                        mViewModel.insertarcomentario(comment);


                        Intent intent = new Intent();
                        intent.putExtra("comment", comment);
                        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);

                    });

                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    WriteCommentFragment.this.getDialog().cancel();
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_CANCELED, getActivity().getIntent());
                });
        return builder.create();
    }

}
