package es.unex.giiis.asee.comunifilm.data.network;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.ResultSearchFilm;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TopFilmsNetworkLoaderRuneable implements Runnable {

    private final OnFilmsLoadedListener onFilmsLoadedListener;
    private static String BASE_URL = "https://api.themoviedb.org";
    private static String API_KEY = "5e401542b701d60f6992434b292a6b7c";
    private String languaje = "en-ES";
    private Context context;
    private ArrayList<Film> listFilm;

    public TopFilmsNetworkLoaderRuneable(OnFilmsLoadedListener onFilmsLoadedListener, Context context) {
        this.onFilmsLoadedListener = onFilmsLoadedListener;
        this.context = context;
        listFilm = new ArrayList<Film>();
    }


    @Override
    public void run() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        try {
            ResultSearchFilm resultSearchFilm = apiInterface.topFilms(API_KEY, languaje).execute().body();

            for (int i = 0; i < resultSearchFilm.getResults().size(); i++) {

                String img = resultSearchFilm.getResults().get(i).getPosterPath();

                Film film = resultSearchFilm.getResults().get(i);

                if (img != null)
                    film.setPosterPath("https://image.tmdb.org/t/p/original//" + img);

                listFilm.add(film);

                Log.i("Top: ", "film: " + film.getTitle());
                Log.i("Top: ", "path poster: " + film.getPosterPath());
            }


            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {
                    onFilmsLoadedListener.OnFilmsLoaded(listFilm);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}