package es.unex.giiis.asee.comunifilm.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.Follower;


import static androidx.room.OnConflictStrategy.REPLACE;


@Dao
public interface FollowerDAO {
    @Insert(onConflict = REPLACE)
    public long insert(Follower follower);

    @Query("SELECT * FROM FOLLOWER WHERE idFollower IN(:idFollower) AND idFollowed IN (:idFollowed)")
    public boolean exist(long idFollower, long idFollowed);

    @Query("DELETE FROM FOLLOWER WHERE idFollower IN (:idFollower) AND idFollowed IN (:idFollowed)")
    public void deleteFollow(long idFollower, long idFollowed);

    @Query("SELECT * FROM FOLLOWER WHERE idFollower IN (:idFollower)")
    public List<Follower> getAllFollowing(long idFollower);

    @Query("SELECT * FROM FOLLOWER WHERE idFollowed IN(:idFollowed)")
    public List<Follower> getAllFollowed(long idFollowed);

    @Query("SELECT * FROM FOLLOWER WHERE nameFollower IN (:Follower)")
    List<Follower> getAllFollowing(String Follower);

    @Query("SELECT COUNT(*) FROM FOLLOWER WHERE nameFollower IN (:Follower)")
    int getAllFollowingnumero(String Follower);

    @Query("SELECT * FROM FOLLOWER WHERE nameFollowed IN (:Followed)")
    List<Follower> getAllFollowed(String Followed);


    @Query("SELECT * FROM FOLLOWER ")
    LiveData<List<Follower>> getAllFollowedlive();
}
