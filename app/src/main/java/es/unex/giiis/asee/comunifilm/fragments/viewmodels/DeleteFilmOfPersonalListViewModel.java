package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.ViewModel;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;


public class DeleteFilmOfPersonalListViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;

    public DeleteFilmOfPersonalListViewModel(RepositoryFilms mRepositoryFilms) {
        this.mRepositoryFilms = mRepositoryFilms;
    }

    public void deletefilmofpersonallist(long idList, long id) {
        mRepositoryFilms.deletefilmofpersonallist(idList, id);
    }
}