package es.unex.giiis.asee.comunifilm.data.network;

import es.unex.giiis.asee.comunifilm.data.models.Genres;

public interface OnGenresLoadedListener {
    public void OnFilmsLoaded(Genres genres);
}
