package es.unex.giiis.asee.comunifilm.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.bumptech.glide.Glide;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.NavDrawActivity;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.ConfirmReleaseDateFilmViewModel;

import static android.content.Context.MODE_PRIVATE;


public class ConfirmReleaseDateFragment extends Fragment implements DatePickerDialog.OnDateSetListener {


    private Film film;
    private String ReleaseDate;
    private TextView eTReleaseDate;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            film = (Film) getArguments().getSerializable("film");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_confirm_releasedate, container, false);


        Button bYesRelease = v.findViewById(R.id.bYesReleaseDate);
        Button bNoRelease = v.findViewById(R.id.bNoReleaseDate);
        Button btPickDate = v.findViewById(R.id.btPickDate);
        eTReleaseDate = v.findViewById(R.id.eTReleaseDate);
        eTReleaseDate.setText(film.getReleaseDate());

        ImageView imgPosterFilm = v.findViewById(R.id.imgReleaseDatePoster);
        TextView title = v.findViewById(R.id.tVTitleReleaseDate);
        TextView releasedate = v.findViewById(R.id.tVReleaseDate);
        title.setText(film.getTitle());
        releasedate.setText(film.getReleaseDate());


        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        ConfirmReleaseDateFilmViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryconfirmdate).get(ConfirmReleaseDateFilmViewModel.class);

        if (film.getPosterPath() != null)
            Glide.with(getContext()).load(film.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(imgPosterFilm);

        btPickDate.setOnClickListener(v1 -> {
            Date filmDate = null;
            try {
                filmDate = new SimpleDateFormat("yyyy-MM-dd").parse(film.getReleaseDate());

            } catch (ParseException e) {
                e.printStackTrace();
                filmDate = new Date();
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(filmDate);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), ConfirmReleaseDateFragment.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

            datePickerDialog.show();

        });

        bNoRelease.setOnClickListener(v12 -> {

            Intent intent = new Intent(getContext(), NavDrawActivity.class);
            startActivity(intent);
        });

        bYesRelease.setOnClickListener(v13 -> {

            if (ReleaseDate != null) {
                Log.i("updatefilm", "updatefilm: " + ReleaseDate);
                SharedPreferences settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
                String userName = settings.getString("userName", "none");

                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {

                        film.setReleaseDate(ReleaseDate);
                        mViewModel.insertarfilm(film);
                        Log.i("updatefilm", "updatefilm: " + film.toString());

                        Intent intent = new Intent(getContext(), NavDrawActivity.class);
                        startActivity(intent);
                    }
                });

            } else {
                Toast.makeText(getContext(), "Debes elegir una fecha", Toast.LENGTH_SHORT).show();
            }
        });


        return v;
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        ReleaseDate = year + "-" + month + "-" + dayOfMonth;

        eTReleaseDate.setText(ReleaseDate);
    }
}