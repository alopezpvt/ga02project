package es.unex.giiis.asee.comunifilm.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.HideKeyboard;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterSearchFilms;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.PersonalListOfFilmsViewModel;


public class PersonalListOfFilmsFragment extends Fragment {

    private ListOfList listOfList;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            listOfList = (ListOfList) getArguments().getSerializable("listOfList");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_personal_list_of_films, container, false);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerSearchFilm);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        ArrayList<Film> listFilm = new ArrayList<Film>();
        AdapterSearchFilms adapterSearchFilms = new AdapterSearchFilms(listFilm);
        recyclerView.setAdapter(adapterSearchFilms);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        PersonalListOfFilmsViewModel mViewModel = new ViewModelProvider(this, appContainer.factorypersonallist).get(PersonalListOfFilmsViewModel.class);

        mViewModel.setidlista(listOfList.getIdList());

        mViewModel.getbusquedalista().observe(getActivity(), listAndFilmsArrayList -> {

            Log.i("Personal: ", "id de la lista: " + listOfList.getIdList());
            Log.i("Personal: ", "lista de ids de lista y película: " + listAndFilmsArrayList.toString());
            Log.i("Personal: ", "id de la película: " + listOfList.getIdList());

            AppExecutors.getInstance().diskIO().execute(() -> {
                listFilm.clear();
                for (int i = 0; i < listAndFilmsArrayList.size(); i++) {
                    Film film = mViewModel.getfilm(listAndFilmsArrayList.get(i).getIdFilm());
                    Log.i("Personal: ", "lista de películas: " + film.getTitle());

                    listFilm.add(film);
                }

                AppExecutors.getInstance().mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        adapterSearchFilms.swap(listFilm);

                        adapterSearchFilms.setOnItemClickListener(pos -> {

                            Log.i("Click: ", "clicking: " + listFilm.get(pos).getTitle());
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("film", listFilm.get(pos));
                            bundle.putSerializable("listOfList", listOfList);

                            Navigation.findNavController(v).navigate(R.id.action_personalListOfFilmsFragment_to_deleteFilmOfPersonalListFragment, bundle);


                        });
                        recyclerView.setAdapter(adapterSearchFilms);
                    }

                });
            });


        });

        EditText etFilmToSearch = v.findViewById(R.id.etFilmToSearch);
        Button bSearchFilmInList = v.findViewById(R.id.bSearchFilmInList);

        bSearchFilmInList.setOnClickListener(v1 -> {
            String filmToSearch = etFilmToSearch.getText().toString().toLowerCase();

            mViewModel.getbusquedalista().observe(getActivity(), listAndFilmsArrayList -> AppExecutors.getInstance().diskIO().execute(() -> {
                listFilm.clear();
                for (int i = 0; i < listAndFilmsArrayList.size(); i++) {
                    Film film = mViewModel.getfilm(listAndFilmsArrayList.get(i).getIdFilm());
                    if (film.getTitle().toLowerCase().contains(filmToSearch)) {
                        Log.i("Personal: ", "lista de películas: " + film.getTitle());

                        listFilm.add(film);
                    }
                }

                AppExecutors.getInstance().mainThread().execute(() -> {
                    adapterSearchFilms.swap(listFilm);

                    adapterSearchFilms.setOnItemClickListener(pos -> {

                        Log.i("Click: ", "clicking: " + listFilm.get(pos).getTitle());
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("film", listFilm.get(pos));
                        bundle.putSerializable("listOfList", listOfList);
                        Navigation.findNavController(v1).navigate(R.id.action_personalListOfFilmsFragment_to_deleteFilmOfPersonalListFragment, bundle);

                    });
                    recyclerView.setAdapter(adapterSearchFilms);
                });

            }));

            etFilmToSearch.setText("");
            HideKeyboard.hideKeyboard(getActivity());

        });


        FloatingActionButton fABInsertFilmInList = v.findViewById(R.id.fABInsertFilmInList);

        fABInsertFilmInList.setOnClickListener(v12 -> {
            Bundle bundle = new Bundle();
            bundle.putSerializable("listOfList", listOfList);
            bundle.putString("CommentOrRecommend", "addList");
            Navigation.findNavController(v12).navigate(R.id.action_personalListOfFilmsFragment_to_searchFilmFragment, bundle);
        });


        return v;
    }
}