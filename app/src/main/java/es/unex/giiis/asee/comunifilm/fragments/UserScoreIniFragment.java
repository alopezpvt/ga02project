package es.unex.giiis.asee.comunifilm.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.HideKeyboard;
import es.unex.giiis.asee.comunifilm.R;

import static android.content.Context.MODE_PRIVATE;


public class UserScoreIniFragment extends Fragment{





    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_userscoreini, container, false);
        HideKeyboard.hideKeyboard(getActivity());

        SharedPreferences settings = getContext().getSharedPreferences("user", MODE_PRIVATE);
        if (settings.getString("userName", "none").equals("none")) {
            getActivity().onBackPressed();
            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(), "You aren't register", Toast.LENGTH_SHORT).show();
                }
            });

        } else {

            SearchUserFragment searchUserFragment = new SearchUserFragment();

            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.searchUserFragment, searchUserFragment).commit();
        }


        return v;
    }
}
