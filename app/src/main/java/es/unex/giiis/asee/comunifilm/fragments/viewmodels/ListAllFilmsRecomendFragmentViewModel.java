package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.Recomend;

public class ListAllFilmsRecomendFragmentViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;
    private LiveData<List<Recomend>> mAllrecomend;


    public ListAllFilmsRecomendFragmentViewModel(RepositoryFilms mRepositoryFilms) {

        this.mRepositoryFilms = mRepositoryFilms;
        mAllrecomend = mRepositoryFilms.getallfilmsrecomend();
    }

    public LiveData<List<Recomend>> getallfilmsrecomend() {
        return mRepositoryFilms.getallfilmsrecomend();
    }
}