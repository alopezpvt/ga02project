package es.unex.giiis.asee.comunifilm.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.NavDrawActivity;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.data.models.UserScore;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.UserScoreFragmentViewModel;

import static android.content.Context.MODE_PRIVATE;

public class UserScoreFragment extends Fragment {


    private User user;
    private UserScoreFragmentViewModel mViewModel;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            user = (User) getArguments().getSerializable("user");
            Log.i("Usuario Recibido", "Recibido datos: " + user.toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_score_user, container, false);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        mViewModel = new ViewModelProvider(this, appContainer.factoryuserscore).get(UserScoreFragmentViewModel.class);
        Log.i("Bundle", "Recibido datos: " + user.getName());

        ImageButton b1pScore = v.findViewById(R.id.b1pScore);
        ImageButton b2pScore = v.findViewById(R.id.b2pScore);
        ImageButton b3pScore = v.findViewById(R.id.b3pScore);
        ImageButton b4pScore = v.findViewById(R.id.b4pScore);
        ImageButton b5pScore = v.findViewById(R.id.b5pScore);

        scoreButton(b1pScore, 1);
        scoreButton(b2pScore, 2);
        scoreButton(b3pScore, 3);
        scoreButton(b4pScore, 4);
        scoreButton(b5pScore, 5);

        return v;
    }

    private void scoreButton(ImageButton puntuar, int point) {
        SharedPreferences settings = getContext().getSharedPreferences("user", MODE_PRIVATE);

        puntuar.setOnClickListener(v -> {
            SharedPreferences settings1 = getActivity().getSharedPreferences("user", MODE_PRIVATE);
            String userName = settings1.getString("userName", "none");

            int puntuacion = point;

            if (puntuacion < 6 && puntuacion > 0) {

                UserScore uScore = new UserScore(userName, user.getName(), puntuacion);

                AppExecutors.getInstance().diskIO().execute(() -> {
                    if (user.isVotado() == false) {
                        user.setVotado(true);
                        mViewModel.insertaruserscore(uScore);
                        mViewModel.update(user);
                        Log.i("create score", "insertando puntuacion" + uScore.toString());
                        Intent intent = new Intent(getContext(), NavDrawActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    } else {
                        AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Ya has puntuado a este usuario", Toast.LENGTH_LONG).show());
                    }
                });
            } else {
                AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Points need be from 1 to 5", Toast.LENGTH_SHORT).show());
            }
        });
    }
}
