package es.unex.giiis.asee.comunifilm.fragments.viewmodels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;

public class ListofListViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;
    private LiveData<List<ListOfList>> mallListOfList;

    public ListofListViewModel(RepositoryFilms mRepositoryFilms) {
        this.mRepositoryFilms = mRepositoryFilms;
        mallListOfList = mRepositoryFilms.getListOfListAllfilter();
    }

    public LiveData<List<ListOfList>> getListOfListAll() {
        return mallListOfList;
    }

    public void insertarlistofList(ListOfList listOfList) {
        mRepositoryFilms.insertarListOfList(listOfList);
    }

    public void setuser(String user) {
        mRepositoryFilms.setuser(user);
    }
}