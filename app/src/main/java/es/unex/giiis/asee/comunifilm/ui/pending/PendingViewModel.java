package es.unex.giiis.asee.comunifilm.ui.pending;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.data.models.Wish;

public class PendingViewModel extends ViewModel {


    private final RepositoryFilms mRepositoryFilms;
    private final LiveData<List<Wish>> mAllWish;


    public PendingViewModel(RepositoryFilms mRepositoryFilms) {

        this.mRepositoryFilms = mRepositoryFilms;
        mAllWish = mRepositoryFilms.getCurrentdeseadas();
    }

    public LiveData<List<Wish>> getdeseadas() {
        return mAllWish;
    }


}