package es.unex.giiis.asee.comunifilm.ui.home;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.fragments.SearchFilmFragment;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_scoresni, container, false);

        SharedPreferences settings = getContext().getSharedPreferences("user", MODE_PRIVATE);
        if (settings.getString("userName", "none").equals("none")) {
            //  getActivity().onBackPressed();
            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(), "Si no estas registrado tus acciones estan limitadas", Toast.LENGTH_SHORT).show();
                }
            });

        }

        Bundle bundle = new Bundle();
        bundle.putString("CommentOrRecommend", "see");
        SearchFilmFragment searchFilmFragment = new SearchFilmFragment();
        searchFilmFragment.setArguments(bundle);

        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.searchFilmFragment, searchFilmFragment).commit();


        return v;
    }
}