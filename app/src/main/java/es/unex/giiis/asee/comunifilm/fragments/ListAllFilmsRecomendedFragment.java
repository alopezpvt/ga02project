package es.unex.giiis.asee.comunifilm.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.adapters.AdapterRecomend;
import es.unex.giiis.asee.comunifilm.data.models.Recomend;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.ListAllFilmsRecomendFragmentViewModel;


public class ListAllFilmsRecomendedFragment extends Fragment {

    private ArrayList<Recomend> listRecomend;
    private RecyclerView recycler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_list_all_films_recomended, container, false);

        recycler = v.findViewById(R.id.recyclerRecomend);
        recycler.setLayoutManager(new LinearLayoutManager(getContext()));
        listRecomend = new ArrayList<Recomend>();

        AdapterRecomend adapterRecomend = new AdapterRecomend(listRecomend);
        recycler.setAdapter(adapterRecomend);


        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        ListAllFilmsRecomendFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryliastallfilmsrecomend).get(ListAllFilmsRecomendFragmentViewModel.class);

        mViewModel.getallfilmsrecomend().observe(getActivity(), listarecomendadas -> {
            Log.i("Listarecomenda: ", "numero: " + listarecomendadas.size());
            AdapterRecomend adapterRecomend1 = new AdapterRecomend((ArrayList<Recomend>) listarecomendadas);
            recycler.setAdapter(adapterRecomend1);

        });

        return v;
    }
}