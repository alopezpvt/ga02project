package es.unex.giiis.asee.comunifilm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;

public class AdapterListOfList extends RecyclerView.Adapter<AdapterListOfList.ViewHolderListOfList> {



    public void swap(ArrayList<ListOfList> listNames) {
        listOfList = listNames;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener {
        void onItemClick(int pos);
    }

    private ArrayList<ListOfList> listOfList;
    private OnItemClickListener listener;

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public AdapterListOfList(ArrayList<ListOfList> listOfList) {
        this.listOfList = listOfList;
    }


    @NonNull
    @Override
    public ViewHolderListOfList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_of_list_list, parent, false);
        return new AdapterListOfList.ViewHolderListOfList(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderListOfList holder, int position) {
        holder.setList(listOfList.get(position));
    }

    @Override
    public int getItemCount() {
        return listOfList.size();
    }

    public class ViewHolderListOfList extends RecyclerView.ViewHolder {


        TextView tVNameOfList;
        ImageView imgFolder;

        public ViewHolderListOfList(View itemView, OnItemClickListener listener) {
            super(itemView);
            tVNameOfList = itemView.findViewById(R.id.tVNameOfList);
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    int pos = getAdapterPosition();
                    if (pos != RecyclerView.NO_POSITION) {
                        listener.onItemClick(pos);
                    }
                }
            });
        }

        public void setList(ListOfList listOfList) {
            tVNameOfList.setText(listOfList.getNameList());
        }
    }
}
