package es.unex.giiis.asee.comunifilm.fragments.factory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.SearchFilmFragmentViewModel;

public class SearchFilmViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private final RepositoryFilms mRepositoryFilms;

    public SearchFilmViewModelFactory(RepositoryFilms repositoryFilms) {
        this.mRepositoryFilms = repositoryFilms;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new SearchFilmFragmentViewModel(mRepositoryFilms);
    }
}
