package es.unex.giiis.asee.comunifilm.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.ListAndFilms;


import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ListAndFilmsDAO {

    @Insert(onConflict = REPLACE)
    public long insert(ListAndFilms listIdAutoGenerated);

    @Query("SELECT * FROM LISTANDFILMS WHERE idList IN (:id)")
    public List<ListAndFilms> getAllFilmsPerList(long id);

    @Query("SELECT * FROM LISTANDFILMS WHERE idList IN (:id)")
    LiveData<List<ListAndFilms>> getAllFilmsPerListlive(long id);

    @Query("DELETE FROM LISTANDFILMS WHERE idList IN (:idL) AND idFilm IN (:idF)")
    public void deleteFilmOfList(long idL, long idF);

    @Query("DELETE FROM LISTANDFILMS WHERE idFilm IN (:idF)")
    public void deleteFilmOfListbyidfilm(long idF);

    @Query("SELECT * FROM LISTANDFILMS WHERE idList IN(:idList) AND nameFilm IN (:title)")
    public boolean existFilm(long idList, String title);

    @Query("SELECT * FROM LISTANDFILMS")
    LiveData<List<ListAndFilms>> getListAndFilmslive();
}
