package es.unex.giiis.asee.comunifilm.data;

import android.util.Log;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import java.util.ArrayList;
import java.util.List;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.data.models.Follower;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.data.models.UserScore;
import es.unex.giiis.asee.comunifilm.data.roomdb.FollowerDAO;
import es.unex.giiis.asee.comunifilm.data.roomdb.UserDAO;
import es.unex.giiis.asee.comunifilm.data.roomdb.UserScoreDAO;

public class RepositoryUsers {
    private static final String LOG_TAG = RepositoryUsers.class.getSimpleName();

    // For Singleton instantiation
    private static RepositoryUsers sInstance;
    private final UserDAO mUserDAO;
    private MutableLiveData<List<User>> mAllUser = new MutableLiveData<List<User>>();
    private final UserScoreDAO mUserScoreDAO;
    private MutableLiveData<List<UserScore>> mAllUserScore = new MutableLiveData<List<UserScore>>();
    private MutableLiveData<UserScore> mUserScore = new MutableLiveData<UserScore>();
    private final FollowerDAO mFollowerDAO;
    private MutableLiveData<List<Follower>> mAllFollower = new MutableLiveData<List<Follower>>();
    private MutableLiveData<List<Follower>> mAllFollowed = new MutableLiveData<List<Follower>>();
    private final AppExecutors mExecutors = AppExecutors.getInstance();
    private final MutableLiveData<String> userFilterLiveData = new MutableLiveData<>();
    private MutableLiveData<List<User>> usuariopedido = new MutableLiveData<List<User>>();
    private LiveData<Integer> idFilterLiveData;


    private RepositoryUsers(UserDAO mUserDAO, UserScoreDAO mUserScoreDAO, FollowerDAO mFollowerDAO) {
        this.mUserDAO = mUserDAO;
        this.mUserScoreDAO = mUserScoreDAO;
        this.mFollowerDAO = mFollowerDAO;


        AppExecutors.getInstance().diskIO().execute(() -> {
            ArrayList<UserScore> listapuntuaciones = (ArrayList<UserScore>) mUserScoreDAO.getAllScore();
            mAllUserScore.postValue(listapuntuaciones);
        });


    }

    public synchronized static RepositoryUsers getInstance(UserDAO mUserDAO, UserScoreDAO mUserScoreDAO, FollowerDAO mFollowerDAO) {
        Log.d(LOG_TAG, "Getting the repository");
        if (sInstance == null) {
            sInstance = new RepositoryUsers(mUserDAO, mUserScoreDAO, mFollowerDAO);
            Log.d(LOG_TAG, "Made new repository");
        }
        return sInstance;
    }

    public void setuser(final String user) {
        AppExecutors.getInstance().mainThread().execute(() -> {
            userFilterLiveData.setValue(user);
            actualizarusuarios();
        });
    }

    public void registrarusuario(User user) {
        mUserDAO.insert(user);
        AppExecutors.getInstance().mainThread().execute(() -> {
            userFilterLiveData.setValue(user.getName());
            actualizarusuarios();
        });
    }

    public void reinicio() {
        AppExecutors.getInstance().mainThread().execute(() -> {
            userFilterLiveData.setValue("");
        });
    }

    /**
     * Database related operations
     **/

    public void actualizarusuarios() {
        AppExecutors.getInstance().diskIO().execute(() -> {
            List<User> todos = mUserDAO.getAllUser();
            for (User user : todos) {
                UserScore aux = null;
                aux = mUserScoreDAO.getScoreByAutorAndScored(userFilterLiveData.getValue(), user.getName());
                if (aux != null) {
                    user.setVotado(true);
                    mUserDAO.update(user);
                } else {
                    user.setVotado(false);
                    mUserDAO.update(user);
                }
            }
        });
    }

    public void insertarusuario(User user) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUserDAO.insert(user);
            }
        });
    }

    public void eleminarusuario(User user) {
        mUserDAO.deleteUser(user.getName(), user.getEmail());
    }

    public void insertaruserscore(UserScore userScore) {
        AppExecutors.getInstance().diskIO().execute(new Runnable() {
            @Override
            public void run() {
                mUserScoreDAO.insert(userScore);
            }
        });
    }

    public void insertarfollower(Follower follower) {
        mFollowerDAO.insert(follower);
    }

    public void eleminarfollower(long idFollower, long id) {
        mFollowerDAO.deleteFollow(idFollower, id);
    }

    public User getusuario(long userid) {
        return mUserDAO.getUser(userid);
    }

    public void update(User user) {
        mUserDAO.update(user);
    }

    public MutableLiveData<String> getnombreusuario() {
        return userFilterLiveData;
    }


    public LiveData<List<User>> getusers(long iduser) {
        AppExecutors.getInstance().diskIO().execute(() -> {
            ArrayList<User> listausuarios = (ArrayList<User>) mUserDAO.getUsers(iduser);
            usuariopedido.postValue(listausuarios);
        });
        return usuariopedido;
    }

    public LiveData<List<User>> getallusers() {

        AppExecutors.getInstance().diskIO().execute(() -> {
            ArrayList<User> listausuarios = (ArrayList<User>) mUserDAO.getAllUser();
            mAllUser.postValue(listausuarios);
        });

        return mAllUser;
    }

    public LiveData<List<UserScore>> getalluserscore() {

        AppExecutors.getInstance().diskIO().execute(() -> {
            ArrayList<UserScore> listapuntuaciones = (ArrayList<UserScore>) mUserScoreDAO.getAllScore();
            mAllUserScore.postValue(listapuntuaciones);
        });
        return mAllUserScore;
    }

    public void updateUserScore(UserScore userScore) {

        AppExecutors.getInstance().diskIO().execute(() -> {
            mUserScoreDAO.update(userScore);
        });
    }

    public LiveData<UserScore> getScoreByAutorAndScore(String autor, String user) {

        AppExecutors.getInstance().diskIO().execute((Runnable) () -> {
            UserScore userScore = (UserScore) mUserScoreDAO.getScoreByAutorAndScored(autor, user);
            if (userScore != null)
                Log.i("Score", "la ecncuentra " + userScore.toString());
            else
                Log.i("Score", "no la ecncuentra ");

            mUserScore.postValue(userScore);
        });
        return mUserScore;
    }


    public LiveData<List<Follower>> getallfollower(String nombre) {
        AppExecutors.getInstance().diskIO().execute(() -> {
            ArrayList<Follower> listausuarios = (ArrayList<Follower>) mFollowerDAO.getAllFollowing(nombre);
            mAllFollower.postValue(listausuarios);
        });
        return mAllFollower;
    }

    public LiveData<List<Follower>> getallfollowed(String nombre) {

        AppExecutors.getInstance().diskIO().execute(() -> {
            ArrayList<Follower> listausuarios = (ArrayList<Follower>) mFollowerDAO.getAllFollowed(nombre);
            mAllFollowed.postValue(listausuarios);
        });

        return mAllFollowed;
    }


    public LiveData<Integer> getidusuario() {

        return Transformations.switchMap(userFilterLiveData,
                nombreusuario -> mUserDAO.getidbyname(nombreusuario));
    }
}