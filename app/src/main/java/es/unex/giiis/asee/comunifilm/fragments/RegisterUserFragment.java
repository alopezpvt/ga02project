package es.unex.giiis.asee.comunifilm.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import java.util.List;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MainActivity;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.User;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.RegisterUserFragmentViewModel;


import static android.content.Context.MODE_PRIVATE;


public class RegisterUserFragment extends Fragment {

    private User user;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_register_user, container, false);

        EditText eTUserName = v.findViewById(R.id.eTUserName);
        EditText eTPassword1 = v.findViewById(R.id.eTPassword1);
        EditText eTPassword2 = v.findViewById(R.id.eTPassword2);
        EditText eTEmail = v.findViewById(R.id.eTEmail);

        Button bFinishRegistration = v.findViewById(R.id.bFinishRegistration);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        RegisterUserFragmentViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryregisteruser).get(RegisterUserFragmentViewModel.class);

        bFinishRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String userName = eTUserName.getText().toString();
                String password1 = eTPassword1.getText().toString();
                String password2 = eTPassword2.getText().toString();
                String email = eTEmail.getText().toString();
                if (userName != null && !userName.trim().isEmpty() && password1 != null && !password1.trim().isEmpty() && password2 != null
                        && !password2.trim().isEmpty() && email != null && !email.trim().isEmpty()) {


                    if (password1.equals(password2)) {

                        if (password1.length() >= 8) {

                            AppExecutors.getInstance().diskIO().execute(() -> {

                                Boolean existe = false;
                                List<User> todos = mViewModel.getallusers().getValue();
                                for (User usuariobd : todos) {
                                    if (usuariobd.getName().equals(userName)) {
                                        existe = true;
                                    }
                                }

                                if (existe == false) {
                                    user = new User(userName, password1, email);
                                    mViewModel.registrarusuario(user);

                                    AppExecutors.getInstance().mainThread().execute(() -> mViewModel.obteneridusuario().observe(getActivity(), new Observer<Integer>() {
                                        @Override
                                        public void onChanged(Integer idusuario) {

                                            //Set preferences
                                            SharedPreferences settings = getActivity().getSharedPreferences("user", MODE_PRIVATE);
                                            SharedPreferences.Editor editor = settings.edit();

                                            editor.putLong("userID", idusuario);
                                            editor.putString("userName", user.getName());
                                            editor.putString("password", user.getPassword());
                                            editor.putString("email", user.getEmail());
                                            editor.commit();

                                            Intent intent = new Intent(getActivity(), MainActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }
                                    }));
                                } else {
                                    AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "This user already exist!", Toast.LENGTH_SHORT).show());
                                }
                            });

                        } else {
                            AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Please, password need at least eight characters", Toast.LENGTH_SHORT).show());
                        }
                    } else {
                        AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "it is necessary a same password", Toast.LENGTH_SHORT).show());
                    }
                } else {
                    AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Please, complete all fields", Toast.LENGTH_SHORT).show());
                }

            }
        });


        return v;
    }
}