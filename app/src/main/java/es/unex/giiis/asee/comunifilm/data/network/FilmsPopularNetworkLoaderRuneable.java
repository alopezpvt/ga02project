package es.unex.giiis.asee.comunifilm.data.network;

import android.content.Context;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.ResultSearchFilm;
import es.unex.giiis.asee.comunifilm.data.roomdb.ComufilmDatabaseServer;
import es.unex.giiis.asee.comunifilm.data.roomdb.ComufilmDatabaseUser;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FilmsPopularNetworkLoaderRuneable implements Runnable {

    private final OnFilmsLoadedListener onFilmsLoadedListener;
    private static String BASE_URL = "https://api.themoviedb.org";
    private static String API_KEY = "5e401542b701d60f6992434b292a6b7c";
    private String languaje = "en-ES";
    private Context context;
    private ArrayList<Film> listFilm;
    String userName;

    public FilmsPopularNetworkLoaderRuneable(OnFilmsLoadedListener onFilmsLoadedListener, Context context) {
        this.onFilmsLoadedListener = onFilmsLoadedListener;
        this.context = context;
        listFilm = new ArrayList<Film>();
    }

    public FilmsPopularNetworkLoaderRuneable(OnFilmsLoadedListener onFilmsLoadedListener, String userName) {
        this.onFilmsLoadedListener = onFilmsLoadedListener;
        this.userName = userName;
        listFilm = new ArrayList<Film>();
    }

    @Override
    public void run() {
        listFilm.clear();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface apiInterface = retrofit.create(ApiInterface.class);

        ComufilmDatabaseServer dbServer = ComufilmDatabaseServer.getInstance(context);
        ComufilmDatabaseUser userServer = ComufilmDatabaseUser.getInstance(context);

        try {
            ResultSearchFilm resultSearchFilm = apiInterface.popularFilm(API_KEY, languaje).execute().body();

            for (int i = 0; i < resultSearchFilm.getResults().size(); i++) {

                String img = resultSearchFilm.getResults().get(i).getPosterPath();

                Film film = resultSearchFilm.getResults().get(i);
                film.setGenreIds(resultSearchFilm.getResults().get(i).getGenreIds());

                if (dbServer.getRecomendDAO().isRecomendbyautor(userName, film.getTitle()) == null) {
                    film.setRecomendada(false);
                } else {
                    film.setRecomendada(true);
                }
                if (userServer.getFavDAO().isFavbyautor(userName, film.getTitle()) == null) {
                    film.setFavoritos(false);
                } else {
                    film.setFavoritos(true);
                }
                if (userServer.getWishDAO().isWishbyautor(userName, film.getTitle()) == null) {
                    film.setDeseadas(false);
                } else {
                    film.setDeseadas(true);
                }
                if (dbServer.getScoreDAO().isScorebyautor(userName, film.getTitle()) == null) {
                    film.setPuntuacion(0);
                } else {
                    int g = dbServer.getScoreDAO().isScorebyautor(userName, film.getTitle()).getPoints();
                    film.setPuntuacion(g);
                }

                if (img != null)
                    film.setPosterPath("https://image.tmdb.org/t/p/original//" + img);
                film.setType("popular");
                listFilm.add(film);


                Log.i("FilmPopular: ", "film: " + film.toString());
                Log.i("FilmPopular: ", "path poster: " + film.getPosterPath());
            }


            AppExecutors.getInstance().mainThread().execute(new Runnable() {
                @Override
                public void run() {
                    onFilmsLoadedListener.OnFilmsLoaded(listFilm);
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
