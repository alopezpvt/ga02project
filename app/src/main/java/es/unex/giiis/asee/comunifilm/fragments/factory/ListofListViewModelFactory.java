package es.unex.giiis.asee.comunifilm.fragments.factory;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import es.unex.giiis.asee.comunifilm.data.RepositoryFilms;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.ListofListViewModel;

public class ListofListViewModelFactory extends ViewModelProvider.NewInstanceFactory {


    private final RepositoryFilms mRepositoryFilms;

    public ListofListViewModelFactory(RepositoryFilms mRepositoryFilms) {

        this.mRepositoryFilms = mRepositoryFilms;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        //noinspection unchecked
        return (T) new ListofListViewModel(mRepositoryFilms);
    }
}
