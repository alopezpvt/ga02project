package es.unex.giiis.asee.comunifilm.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.Fav;

import static androidx.room.OnConflictStrategy.REPLACE;


@Dao
public interface FavDAO {

    @Insert(onConflict = REPLACE)
    public long insert(Fav fav);

    @Query("SELECT * FROM FAV WHERE id IN (:id)")
    public Fav getFav(long id);

    @Query("SELECT * FROM FAV")
    public List<Fav> getAllFav();

    @Query("SELECT * FROM FAV")
    LiveData<List<Fav>> getAllFavlive();

    @Query("SELECT * FROM FAV WHERE autorFav IN (:autorFav) ")
    LiveData<List<Fav>> getAllFavbyautor(String autorFav);

    @Query("SELECT * FROM FAV WHERE autorFav IN (:autorFav) AND filmName IN (:title) ")
    public Fav isFavbyautor(String autorFav, String title);

    @Update
    public int update(Fav fav);

    @Query("DELETE FROM FAV WHERE autorFav IN (:autorFav) AND filmName IN (:title)")
    abstract void deletebyquery(String autorFav, String title);

    @Query("DELETE FROM FAV WHERE filmName IN (:title)")
    abstract void deletebyfilmname(String title);

    @Delete
    public void delete(Fav fav);
}
