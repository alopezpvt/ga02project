package es.unex.giiis.asee.comunifilm.data.network;

import java.util.ArrayList;
import es.unex.giiis.asee.comunifilm.data.models.Film;

public interface OnFilmsLoadedListener {
    public void OnFilmsLoaded(ArrayList<Film> listFilms);
}

