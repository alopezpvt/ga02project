package es.unex.giiis.asee.comunifilm.fragments;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.Glide;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.roomdb.ComufilmDatabaseUser;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.data.models.ListAndFilms;
import es.unex.giiis.asee.comunifilm.data.models.ListOfList;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.AddPersonalListViewModel;


public class AddPersonalListFragment extends Fragment {

    private Film film;
    private ListOfList listOfList;
    private ComufilmDatabaseUser dbUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            film = (Film) getArguments().getSerializable("film");
            listOfList = (ListOfList) getArguments().getSerializable("listOfList");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_personal_list, container, false);

        ImageView imgPoster = v.findViewById(R.id.imgPoster);
        TextView tvTitle = v.findViewById(R.id.tvTitle);
        TextView tvDescription = v.findViewById(R.id.tvDescription);
        Button noAdd = v.findViewById(R.id.bBack);
        Button siAdd = v.findViewById(R.id.bsiDelete);

        if (film.getPosterPath() != null)
            Glide.with(getContext()).load(film.getPosterPath()).centerCrop().placeholder(R.drawable.ic_launcher_background).into(imgPoster);

        tvTitle.setText(film.getTitle());
        tvDescription.setText(film.getOverview());
        Bundle bundle = new Bundle();
        bundle.putSerializable("listOfList", listOfList);
        dbUser = ComufilmDatabaseUser.getInstance(getContext());

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        AddPersonalListViewModel mViewModel = new ViewModelProvider(this, appContainer.factoryaddpersonal).get(AddPersonalListViewModel.class);


        siAdd.setOnClickListener(v1 -> AppExecutors.getInstance().networkIO().execute(() -> {

            ListAndFilms listAndFilm = new ListAndFilms(listOfList.getIdList(), film.getId(), film.getTitle());

            mViewModel.insertarlistAndFilm(listAndFilm);

            Log.i("addFilm: ", "id de la lista: " + listOfList.getIdList());

            AppExecutors.getInstance().mainThread().execute(() -> {

                Navigation.findNavController(v1).navigate(R.id.action_addPersonalListFragment_to_personalListOfFilmsFragment, bundle);
                Toast.makeText(getContext(), "Película insertada!", Toast.LENGTH_SHORT).show();


            });

        }));

        noAdd.setOnClickListener(v12 -> {
            bundle.putString("CommentOrRecommend", "addList");
            Navigation.findNavController(v12).navigate(R.id.action_addPersonalListFragment_to_searchFilmFragment, bundle);
        });

        return v;
    }
}