package es.unex.giiis.asee.comunifilm.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import java.util.Calendar;
import java.util.Date;
import es.unex.giiis.asee.comunifilm.AppContainer;
import es.unex.giiis.asee.comunifilm.AppExecutors;
import es.unex.giiis.asee.comunifilm.MyApplication;
import es.unex.giiis.asee.comunifilm.NavDrawActivity;
import es.unex.giiis.asee.comunifilm.R;
import es.unex.giiis.asee.comunifilm.data.models.Film;
import es.unex.giiis.asee.comunifilm.fragments.viewmodels.CreateFilmViewModel;
import static android.content.Context.MODE_PRIVATE;



public class CreateFilmFragment extends Fragment implements DatePickerDialog.OnDateSetListener {


    private String date;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_create_film, container, false);
        EditText eTCreateFilmTitle = v.findViewById(R.id.eTCreateFilmTitle);
        EditText eTCreateFilmPosterPath = v.findViewById(R.id.eTCreateFilmPosterPath);
        EditText eTCreateFilmOverview = v.findViewById(R.id.eTCreateFilmOverview);

        Button bCreateFilm = v.findViewById(R.id.bCreateFilm);
        Button bChangeDate = v.findViewById(R.id.bChangeDate);

        AppContainer appContainer = ((MyApplication) getActivity().getApplication()).appContainer;
        CreateFilmViewModel mViewModel = new ViewModelProvider(this, appContainer.factorycreatefilm).get(CreateFilmViewModel.class);


        bChangeDate.setOnClickListener(v1 -> {
            Date filmDate = new Date();

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(filmDate);

            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), CreateFilmFragment.this, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

            datePickerDialog.show();

        });

        bCreateFilm.setOnClickListener(v12 -> {

            String title = eTCreateFilmTitle.getText().toString();
            String overview = eTCreateFilmOverview.getText().toString();

            AppExecutors.getInstance().diskIO().execute(() -> {

                if (title != null && !title.trim().isEmpty() && overview != null && !overview.trim().isEmpty() && date.matches("\\d{4}-\\d{2}-\\d{2}")) {

                    SharedPreferences settings = getContext().getSharedPreferences("user", MODE_PRIVATE);
                    long iduser = settings.getLong("userID", 0);


                    Film film = new Film(title, eTCreateFilmPosterPath.getText().toString(), 0.0, 0, date, overview, iduser);

                    mViewModel.insertarfilm(film);
                    Log.i("insert", "insertando película " + film.toString());

                    AppExecutors.getInstance().mainThread().execute(() -> {
                        Toast.makeText(getContext(), "Film created!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getContext(), NavDrawActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    });
                } else {
                    AppExecutors.getInstance().mainThread().execute(() -> Toast.makeText(getContext(), "Review if the title, overview or date are in correct format.", Toast.LENGTH_SHORT).show());


                }


            });

        });


        return v;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        date = year + "-" + month + "-" + dayOfMonth;

    }

}