package es.unex.giiis.asee.comunifilm.data.roomdb;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;
import es.unex.giiis.asee.comunifilm.data.models.Recomend;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface RecomendDAO {

    @Insert(onConflict = REPLACE)
    public long insert(Recomend recomend);

    @Query("SELECT * FROM RECOMEND WHERE id IN (:id)")
    public Recomend getRecomend(long id);

    @Query("SELECT * FROM RECOMEND")
    public List<Recomend> getAllRecomend();

    @Query("SELECT * FROM RECOMEND")
    LiveData<List<Recomend>> getAllRecomendlive();

    @Query("SELECT * FROM RECOMEND WHERE filmName IN (:filmName) ")
    public List<Recomend> getAllRecomendbyfilmname(String filmName);

    @Query("SELECT * FROM RECOMEND WHERE filmName IN (:autorRecomend) ")
    public List<Recomend> getAllRecomendbyautor(String autorRecomend);

    @Query("SELECT * FROM RECOMEND WHERE autorRecomend IN (:autorRecomend) AND filmName IN (:title) ")
    public Recomend isRecomendbyautor(String autorRecomend, String title);

    @Query("SELECT * FROM RECOMEND WHERE autorRecomend IN (:autorRecomend) AND filmName IN (:title) ")
    LiveData<List<Recomend>> isRecomendbyautorlive(String autorRecomend, String title);

    @Query("DELETE FROM RECOMEND WHERE autorRecomend IN (:autorRecomend) AND filmName IN (:title)")
    abstract void deletebyquery(String autorRecomend, String title);

    @Query("DELETE FROM RECOMEND WHERE filmName IN (:title)")
    abstract void deletebyfilmname(String title);

    @Update
    public int update(Recomend recomend);

}
